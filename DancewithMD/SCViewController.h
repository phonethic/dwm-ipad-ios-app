//
//  SCViewController.h
//  MadhuriDixitHD
//
//  Created by Kirti Nikam on 22/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <AVFoundation/AVFoundation.h>
#import "DMDLoginViewController.h"

@class DMDMyVideosViewController;
@interface SCViewController : UIViewController  <FBUserSettingsDelegate,UITextViewDelegate,DMDViewControllerDelegate> {
    int status;
    NSMutableData *responseAsyncData;
    NSURLConnection *connection1;
    NSURLConnection *connection2;
    DMDLoginViewController *loginController;
    UINavigationController *loginnavController;
    DMDMyVideosViewController *myvideosController;
    UINavigationController *offlinenavController;
    UIPopoverController *popoverController;
    NSURLConnection *connectionLogin;
    NSURLConnection *connectionUserDetail;
    NSURLConnection *connectionVersionCheck;
}

@property (nonatomic, copy) NSString *FBtitle;
@property (nonatomic, copy) NSString *FBtLink;
@property (nonatomic, copy) NSString *FBtCaption;
@property (nonatomic, copy) NSString *FBtPic;
@property (nonatomic, readwrite) int showorhide;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *appIcon;
@property (strong, nonatomic) NSMutableDictionary *postParams;
@property (strong, nonatomic) IBOutlet UITextView *message;
@property (strong, nonatomic) IBOutlet UIView *mainFBView;
@property (strong, nonatomic) IBOutlet UIButton *facebookLoginBtn;

/////////* DANCE FLOOR */////////////
@property (strong, nonatomic) IBOutlet UILabel *messageLabel;
@property (strong, nonatomic) IBOutlet UIButton *offlineModeBtn;


- (IBAction)announce:(id)sender;
- (IBAction)loginFB:(id)sender;
- (IBAction)offlineModeBtnPressed:(id)sender;

@end
