//
//  DMDSongObject.h
//  DancewithMD
//
//  Created by Rishi on 03/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DMDSongObject : NSObject {
    
}

@property (nonatomic, copy) NSString *songId;
@property (nonatomic, copy) NSString *songTitle;
@property (nonatomic, copy) NSString *songMovie;
@property (nonatomic, copy) NSString *songChoreographer;
@property (nonatomic, copy) NSString *songPic;
@property (nonatomic, copy) NSString *songLock;
@property (nonatomic, copy) NSString *songCategory;
@property (nonatomic, copy) NSString *songCategoryName;
@end
