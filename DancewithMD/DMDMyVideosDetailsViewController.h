//
//  DMDMyVideosDetailsViewController.h
//  DancewithMD
//
//  Created by Rishi on 17/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DMDMyVideosDetailsViewController : UIViewController {
    
}
@property (strong, nonatomic) IBOutlet UITableView *downloadedvideoTableView;
@property (strong, nonatomic) NSMutableArray *videoArray;
@property (readwrite, nonatomic) NSIndexPath *tempIndex;
@property (nonatomic, copy) NSString *songID;
@end
