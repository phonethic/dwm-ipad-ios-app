//
//  DMDAppDelegate.h
//  DancewithMD
//
//  Created by Rishi on 06/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "constants.h"
#import "DMDSplashVideoViewController.h"
//#import "Crittercism.h"

#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define DMDDelegate (DMDAppDelegate*)[[UIApplication sharedApplication] delegate]

static const CGFloat kDefaultReflectionFraction = 0.50;
static const CGFloat kDefaultReflectionOpacity = 0.40;
extern NSString *const SCSessionStateChangedNotification;

@class DMDViewController;

@interface DMDAppDelegate : UIResponder <UIApplicationDelegate>
{
    Reachability* internetReach;
    Boolean networkavailable;
}
@property(nonatomic) Boolean networkavailable;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) DMDViewController *viewController;
@property (strong, nonatomic) DMDSplashVideoViewController *splashviewController;

- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;
-(void)animationStop;
- (UIImage *)reflectedImage:(UIImageView *)fromImage withHeight:(NSUInteger)height;
-(NSString*)URLEncode:(NSString*) string;
- (NSString *) md5:(NSString *) input;
@end
