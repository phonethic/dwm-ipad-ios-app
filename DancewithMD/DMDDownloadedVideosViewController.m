//
//  DMDDownloadedVideosViewController.m
//  DancewithMD
//
//  Created by Rishi on 15/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDDownloadedVideosViewController.h"
#import "DMDAppDelegate.h"
#import "constants.h"

@interface DMDDownloadedVideosViewController ()

@end

@implementation DMDDownloadedVideosViewController
@synthesize carousel;
@synthesize songID;
@synthesize selectedIndex;
@synthesize videoArray;
@synthesize moviePlayer;
@synthesize deleteVideoBtn;
@synthesize backgroundImageView,lightspotImageView;
@synthesize videoView,screenshotView,videoboxImageView,titlelbl,namelbl,reflectionView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //NSLog(@"viewWillDisappear called");
    if(self.moviePlayer != nil && !fullscreen) {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    fullscreen = 0;
    carousel.type = iCarouselTypeCoverFlow;
     titlelbl.text = @"";
    namelbl.text = @"";
    deleteVideoBtn.titleLabel.font = KABEL_FONT(20);
    titlelbl.font = KABEL_FONT(30);
    namelbl.font = KABEL_FONT(18);
    titlelbl.textColor = GRAY_TEXT_COLOR;
    namelbl.textColor = GRAY_TEXT_COLOR;
    [videoView setHidden:TRUE];
    small = 0;
    videoArray = [[NSMutableArray alloc] init];
    
    [self getVideoFiles];
}

-(void)getVideoFiles
{
    [videoArray removeAllObjects];
    
    NSArray *dirPaths;
    NSString *fullPath;
    NSString *docsDir;
    NSArray *filelist;
    int count;
    int i;
    NSFileManager *filemgr;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    
    fullPath = [docsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"Videos/%@",self.songID]];
    
    //NSLog(@"%@",fullPath);
    
    filemgr =[NSFileManager defaultManager];
    
    filelist = [filemgr contentsOfDirectoryAtPath:fullPath error:NULL];
    count = [filelist count];
    //NSLog(@"count = %d",count);
    
    for (i = 0; i < count; i++)
    {
        //NSLog(@"%@", [NSString stringWithFormat:@"%@/%@",fullPath,[filelist objectAtIndex: i]]);
        if(![[filelist objectAtIndex: i] isEqualToString:@".DS_Store"])
            [videoArray addObject:[NSString stringWithFormat:@"%@/%@",fullPath,[filelist objectAtIndex: i]]];
    }
    [carousel reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [carousel reloadData];
    [self.view bringSubviewToFront:lightspotImageView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setBackgroundImageView:nil];
    [self setCarousel:nil];
    [self setLightspotImageView:nil];
    [self setVideoView:nil];
    [self setScreenshotView:nil];
    [self setVideoboxImageView:nil];
    [self setTitlelbl:nil];
    [self setNamelbl:nil];
    [self setReflectionView:nil];
    [self setDeleteVideoBtn:nil];
    [super viewDidUnload];
}

-(void) loadLocalDocument:(NSString *)filePath {
    if(self.moviePlayer != nil) {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
    NSURL* videoURL = [NSURL fileURLWithPath: filePath];
    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:videoURL];
    self.moviePlayer.view.frame = CGRectMake(239, 246, 548, 298);
    //self.moviePlayer.view.clipsToBounds = TRUE;
    //self.moviePlayer.view.layer.cornerRadius = 5;
    //self.moviePlayer.view.layer.borderColor = [UIColor darkGrayColor].CGColor;
    //self.moviePlayer.view.layer.borderWidth = 4;
    [moviePlayer setControlStyle:MPMovieControlStyleDefault];
    self.moviePlayer.scalingMode = MPMovieScalingModeFill;
    self.moviePlayer.shouldAutoplay = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinishLocal:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayEnterFullScreen:)
                                                 name:MPMoviePlayerWillEnterFullscreenNotification
                                               object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayExitFullScreen:)
                                                 name:MPMoviePlayerDidExitFullscreenNotification
                                               object:self.moviePlayer];
    [self.view addSubview:moviePlayer.view];
    [moviePlayer prepareToPlay];
    [moviePlayer play];
    //close = 0;
}

-(void)showVideoView
{
    backgroundImageView.image = [UIImage imageNamed:@"BG_zoomed_out.jpg"];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.8f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    
    [backgroundImageView.layer addAnimation:transition forKey:nil];
    
    [videoView setHidden:TRUE];
    [videoView  setAlpha:0.0];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [lightspotImageView setAlpha:0.0];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.4];
    [videoView setAlpha:1.0];
    [UIView commitAnimations];
    
    CABasicAnimation *shrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    shrink.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    shrink.fromValue = [NSNumber numberWithDouble:1.5];
    shrink.toValue = [NSNumber numberWithDouble:1.0];
    shrink.duration = 0.8;
    shrink.fillMode=kCAFillModeForwards;
    shrink.removedOnCompletion=NO;
    //unshrink.delegate = self;
    [videoView.layer addAnimation:shrink forKey:@"shrink"];
}

-(void)hideVideoView {
    backgroundImageView.image = [UIImage imageNamed:@"BG_default.jpg"];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.8f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [backgroundImageView.layer addAnimation:transition forKey:nil];
    
    CABasicAnimation *unshrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    unshrink.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    unshrink.fromValue = [NSNumber numberWithDouble:1.0];
    unshrink.toValue = [NSNumber numberWithDouble:1.5];
    unshrink.duration = 0.8;
    unshrink.fillMode = kCAFillModeForwards;
    unshrink.removedOnCompletion = NO;
    [videoView.layer addAnimation:unshrink forKey:@"shrink"];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.6];
    [videoView setAlpha:0.0];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [lightspotImageView setAlpha:1.0];
    [UIView commitAnimations];
    
    small = 2;
    [carousel reloadData];
    //[videoView setHidden:TRUE];
}

-(void)fadeOut:(UIView*)viewToDissolve withDuration:(NSTimeInterval)duration
{
    [UIView beginAnimations: @"Fade Out" context:nil];
    [UIView setAnimationDelay:0];
    [UIView setAnimationDuration:duration];
    viewToDissolve.alpha = 0.0;
    [UIView commitAnimations];
}

-(void)fadeIn:(UIView*)viewToFadeIn withDuration:(NSTimeInterval)duration
{
    [UIView beginAnimations: @"Fade In" context:nil];
    [UIView setAnimationDelay:0];
    [UIView setAnimationDuration:duration];
    viewToFadeIn.alpha = 1;
    [UIView commitAnimations];
    
}

- (void) moviePlayBackDidFinishLocal:(NSNotification*)notification {
    [moviePlayer setFullscreen:FALSE animated:TRUE];
    NSNumber* reason = [[notification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    switch ([reason intValue]) {
        case MPMovieFinishReasonPlaybackEnded:
            //NSLog(@"Playback Ended");
            break;
        case MPMovieFinishReasonPlaybackError:
        {
            //NSLog(@"Playback Error");
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Sorry!! This video is corrupted.\n Please delete this video and try to download again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlert show];
        }
            
            break;
        case MPMovieFinishReasonUserExited:
            //NSLog(@"User Exited");
            break;
        default:
            break;
    }
    MPMoviePlayerController *player = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerWillEnterFullscreenNotification object:player];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerDidExitFullscreenNotification object:player];
    if ([player respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [player.view removeFromSuperview];
    }
    moviePlayer = nil;
    if ([reason intValue] != MPMovieFinishReasonPlaybackError) {
        [self performSelector:@selector(hideVideoView) withObject:nil afterDelay:0.5];
    }
}

- (void) moviePlayEnterFullScreen:(NSNotification*)notification {
    //NSLog(@"moviePlayEnterFullScreen");
    fullscreen = 1;
    self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
    
}

- (void) moviePlayExitFullScreen:(NSNotification*)notification {
    //NSLog(@"moviePlayExitFullScreen");
    fullscreen = 0;
    self.moviePlayer.scalingMode = MPMovieScalingModeFill;
}


#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [videoArray count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        FXImageView *imageView = [[FXImageView alloc] initWithFrame:CGRectMake(0, 0, 300.0f, 300.0f)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        ((UIImageView *)view).image = [UIImage imageNamed:@"carousal_lock.png"];
        imageView.asynchronous = YES;
        imageView.reflectionScale = 0.5f;
        imageView.reflectionAlpha = 0.25f;
        imageView.reflectionGap = 10.0f;
        imageView.shadowOffset = CGSizeMake(0.0f, 2.0f);
        imageView.shadowBlur = 5.0f;
        imageView.cornerRadius = 10.0f;
        view = imageView;
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(50, 168, 200, 50)];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = UITextAlignmentCenter;
        label.textColor = GRAY_TEXT_COLOR;
        label.font = KABEL_FONT(22);
        label.tag = 1;
        [view addSubview:label];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
    }
    ////NSLog(@"%f %f",((UIImageView *)view).frame.size.width,((UIImageView *)view).frame.size.height);
    
    ////NSLog(@"irow=%d icount=%d",index,[self.seenCount intValue]);

    ((UIImageView *)view).image = [UIImage imageNamed:@"carousal_unlock.png"];

    
    if(small==1)
    {
        CABasicAnimation *shrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        shrink.fromValue = [NSNumber numberWithDouble:1.0];
        shrink.toValue = [NSNumber numberWithDouble:0.5];
        shrink.duration = 0.6;
        shrink.fillMode=kCAFillModeForwards;
        shrink.removedOnCompletion=NO;
        //shrink.delegate = self;
        [((UIImageView *)view).layer addAnimation:shrink forKey:@"shrink"];
        self.carousel.userInteractionEnabled = FALSE;
    } else if(small==2) {
        CABasicAnimation *unshrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        unshrink.fromValue = [NSNumber numberWithDouble:0.5];
        unshrink.toValue = [NSNumber numberWithDouble:1.0];
        unshrink.duration = 0.6;
        unshrink.fillMode=kCAFillModeForwards;
        unshrink.removedOnCompletion=NO;
        //unshrink.delegate = self;
        [((UIImageView *)view).layer addAnimation:unshrink forKey:@"shrink"];
        self.carousel.userInteractionEnabled = TRUE;
    }
    
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    
    NSString *vdeoName  = [videoArray objectAtIndex:index];
    NSArray *arr = [vdeoName componentsSeparatedByString:@"/"];
    ////NSLog(@"array = %@", arr);
    NSArray *arr1 = [[arr objectAtIndex:[arr count] -1] componentsSeparatedByString:@"#"];
    ////NSLog(@"array = %@", arr1);
    NSArray *arr2 = [[arr1 objectAtIndex:[arr1 count] -1] componentsSeparatedByString:@"_"];
    ////NSLog(@"array = %@", arr2);
    label.text = [NSString stringWithFormat:@"Lesson %@   Part %@",[arr2 objectAtIndex:[arr2 count]-3],[[arr2 objectAtIndex:[arr2 count]-1] stringByReplacingOccurrencesOfString:@".mp4" withString:@""]];
    return view;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    selectedIndex = index;
    NSString *vdeoName  = [videoArray objectAtIndex:index];
    titlelbl.text = [NSString stringWithFormat:@"%@",self.songID];
    NSArray *arr = [vdeoName componentsSeparatedByString:@"#"];
    ////NSLog(@"array = %@", arr);
    NSArray *arr1 = [[arr objectAtIndex:[arr count] -1] componentsSeparatedByString:@"_"];
    ////NSLog(@"array = %@", arr1);
    namelbl.text = [NSString stringWithFormat:@"Lesson %@     Part %@",[arr1 objectAtIndex:[arr1 count]-3],[[arr1 objectAtIndex:[arr1 count]-1] stringByReplacingOccurrencesOfString:@".mp4" withString:@""]];
    NSString *filepath = [videoArray objectAtIndex:index];
    [self showVideoView];
    if (small==0 || small==2) {
        small = 1;
    } else if(small==1){
        small = 2;
    }
    self.reflectionView.image = [UIImage imageNamed:@"video_box_transparent.png"];
    [videoView setHidden:FALSE];
    screenshotView.hidden = FALSE;
    
    UIGraphicsBeginImageContext(videoboxImageView.frame.size);
    [videoView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    screenshotView.image = viewImage;
    UIGraphicsEndImageContext();
    //UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
    
    
    // determine the size of the reflection to create
    NSUInteger reflectionHeight = self.videoboxImageView.bounds.size.height * kDefaultReflectionFraction;
    
    // create the reflection image and assign it to the UIImageView
    self.reflectionView.image = [DMDDelegate reflectedImage:screenshotView withHeight:reflectionHeight];
    //self.reflectionView.image = [self reflectedImage:screenshotView withHeight:reflectionHeight];
    self.reflectionView.alpha = kDefaultReflectionOpacity;
    
    screenshotView.image = [UIImage imageNamed:@"video_box_transparent.png"];
    screenshotView.hidden = TRUE;
    
    [self.carousel reloadData];
    [self performSelector:@selector(loadLocalDocument:) withObject:filepath afterDelay:0.5];

}

- (CGFloat)carousel:(iCarousel *)_carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionFadeMin:
        {
            return -0.2;
        }
        case iCarouselOptionFadeMax:
        {
            return 0.2;
        }
        case iCarouselOptionFadeRange:
        {
            return 3.0;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return 0.599131;
        }
        case iCarouselOptionTilt:
        {
            return 0.495158;
        }
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return NO;
        }
        default:
        {
            return value;
        }
    }
}

- (IBAction)closevideoBtnPressed:(id)sender {
    if(self.moviePlayer != nil) {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
    [self hideVideoView];
}

- (IBAction)deletebtnPressed:(id)sender {
    
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:ALERT_TITLE
                              message:@"Are you sure you want to delete this video ?"
                              delegate:self
                              cancelButtonTitle:nil
                              otherButtonTitles:@"No",@"Yes",nil];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
	if([title isEqualToString:@"Yes"])
	{
		//NSLog(@"Button YES was selected.");
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *vdeoPath  = [videoArray objectAtIndex:selectedIndex];
        //NSLog(@"%@",vdeoPath);
        BOOL result = [fileManager removeItemAtPath:vdeoPath error:NULL];
        if(result) {
            [videoArray removeObjectAtIndex:selectedIndex];
            [self closevideoBtnPressed:nil];
            [self getVideoFiles];
        } else {
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:ALERT_TITLE
                                      message:@"Failed to delete the video."
                                      delegate:self
                                      cancelButtonTitle:nil
                                      otherButtonTitles:@"Ok",nil];
            [alertView show];
        }
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}
@end
