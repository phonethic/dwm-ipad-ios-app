//
//  danceViewController.h
//  dancefloor1
//
//  Created by Sagar Mody on 27/04/13.
//  Copyright (c) 2013 phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface danceViewController : UIViewController <AVAudioPlayerDelegate >
{
    
    
}
@property (strong, nonatomic) NSMutableArray *pathArray;
@property (strong, nonatomic) NSMutableArray *videoPlayerArray;

@property (nonatomic, strong) NSTimer *glowTimer;

@property (strong, nonatomic) IBOutlet UIImageView *padOneImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padTwoImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padThreeImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padFourImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padFiveImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padSixImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padSevenImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padEightImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padNineImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padTenImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padElevenImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padTwelveImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padThirteenImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padFourteenImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padFifttenImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padSixteenImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padSeventeenImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padEighteenImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padNineteenImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padTwentyImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padTwentyOneImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padTwentyTwoImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padTwentyThreeImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padTwentyFourImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padTwentyFiveImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padTwentysixImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padTwentySevenImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padTwentyEightImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padTwentyNineImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padThirtyImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padThirtyOneImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padThirtyTwoView;
@property (strong, nonatomic) IBOutlet UIImageView *padThirtyThreeImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padThirtyFourImgView;
@property (strong, nonatomic) IBOutlet UIImageView *padThirtyFiveImgView;


- (void)randomOnOffBtnPressed:(id)sender;

@end
