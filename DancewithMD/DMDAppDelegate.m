//
//  DMDAppDelegate.m
//  DancewithMD
//
//  Created by Rishi on 06/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDAppDelegate.h"

#import "DMDViewController.h"
#import "MFSideMenu.h"
#import "SideMenuViewController.h"
#import "DMDHomeViewController.h"
#import "DMDSplashVideoViewController.h"
#import <CommonCrypto/CommonDigest.h>
#include <sys/xattr.h>

NSString *const SCSessionStateChangedNotification = @"com.facebook.MadhuriDMD:MSessionStateChangedNotification";

@implementation DMDAppDelegate
@synthesize window = _window;
@synthesize splashviewController;
@synthesize networkavailable;

- (DMDHomeViewController *)dmdController {
    return [[DMDHomeViewController alloc] initWithNibName:@"DMDHomeViewController" bundle:nil];
}

- (UINavigationController *)navigationController {
    return [[UINavigationController alloc]
            initWithRootViewController:[self dmdController]];
}

- (MFSideMenu *)sideMenu
{
    SideMenuViewController *rightSideMenuController = [[SideMenuViewController alloc] init];
    UINavigationController *navigationController = [self navigationController];
    
    MFSideMenu *sideMenu = [MFSideMenu menuWithNavigationController:navigationController
                                             leftSideMenuController:nil
                                            rightSideMenuController:rightSideMenuController];
    
    rightSideMenuController.sideMenu = sideMenu;
    
    return sideMenu;
}

- (void) setupNavigationControllerApp {
    self.window.rootViewController = [self sideMenu].navigationController;
    [self.window makeKeyAndVisible];
}

-(void)animationStop
{
    [splashviewController.view removeFromSuperview];
    splashviewController = nil;
    [self setupNavigationControllerApp];
//    [UIView  transitionWithView:self.window duration:0.8  options:UIViewAnimationOptionTransitionFlipFromLeft
//                     animations:^(void) {
//                         BOOL oldState = [UIView areAnimationsEnabled];
//                         [UIView setAnimationsEnabled:NO];
//                         [self setupNavigationControllerApp];
//                         [UIView setAnimationsEnabled:oldState];
//                     }
//                     completion:nil];   
}

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    NSUInteger orientations = UIInterfaceOrientationMaskAll;
    
    if (self.window.rootViewController) {
        UIViewController* presented = [[(UINavigationController *)self.window.rootViewController viewControllers] lastObject];
        orientations = [presented supportedInterfaceOrientations];
    }
    return orientations;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    //Add SlideMenu
    //[self setupNavigationControllerApp];
    
    //Add Flurry
    [Flurry startSession:FLURRY_APPID];
    //[Flurry logEvent:@"Dance With Madhuri App Started"];
    
    //Add Appirater
    //[self setAppRateAlert];
    //[Appirater appLaunched:YES];
    
    //Add Parse
    [Parse setApplicationId:PARSE_APPID clientKey:PARSE_CLIENTKEY];
    
    //ADD Crittercism
    //[Crittercism enableWithAppID:CRITTERCISM_APPID];
    
    [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound];
    
    [self startCheckNetwork];
    
    splashviewController = [[DMDSplashVideoViewController alloc] initWithNibName:@"DMDSplashVideoViewController" bundle:nil];
    [self.window addSubview:splashviewController.view];
    [self.window makeKeyAndVisible];
    
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                UITextAttributeTextColor: [UIColor whiteColor],
                          UITextAttributeTextShadowColor: [UIColor blackColor],
                         UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)],
                                     UITextAttributeFont: KABEL_FONT(25)
     }];
    [[UIBarButtonItem appearance] setTitleTextAttributes: @{
                                UITextAttributeTextColor: [UIColor whiteColor],
//                          UITextAttributeTextShadowColor: [UIColor blackColor],
//                         UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)],
                                     UITextAttributeFont: KABEL_FONT(18.0)
     } forState:UIControlStateNormal];
    
    [[UIBarButtonItem appearance] setTitleTextAttributes: @{
                                UITextAttributeTextColor: [UIColor whiteColor],
//                          UITextAttributeTextShadowColor: [UIColor blackColor],
//                         UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)],
                                     UITextAttributeFont: KABEL_FONT(18.0)
     } forState:UIControlStateHighlighted];
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -5.0) forBarMetrics:UIBarMetricsDefault];
    NSArray *dirPaths;
    NSString *fullPath;
    NSString *docsDir;
    NSFileManager *filemgr;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    fullPath = [docsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"Videos"]];
    
    //NSLog(@"%@",fullPath);
    
    filemgr =[NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath:fullPath])
    {
        //NSLog(@"Directory exists");
    } else {
        //NSLog(@"Directory not Found.");
        if ([filemgr createDirectoryAtPath:fullPath withIntermediateDirectories:YES
                                attributes:nil error: NULL] == NO)
        {
            //NSLog(@"Failed to create directory");
            
        } else {
            //NSLog(@"Directory Created");
            [self addSkipBackupAttributeToItemAtURL:fullPath];
        }
    }
//    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
//    
//    NSLog(@"bundleIdentifier=%@",bundleIdentifier);
    
    return YES;
}

- (void)addSkipBackupAttributeToItemAtURL:(NSString*)path
{
    //assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"5.1")) {
        int result = getxattr([path fileSystemRepresentation], "com.apple.MobileBackup", NULL, sizeof(u_int8_t), 0, 0);
        if (result != -1) {
            // The attribute exists, we need to remove it
            int removeResult = removexattr([path fileSystemRepresentation], "com.apple.MobileBackup", 0);
            if (removeResult == 0) {
                //NSLog(@"Removed extended attribute on file %@", [NSURL fileURLWithPath:path]);
            }
        }
        NSError *error = nil;
        BOOL success = [[NSURL fileURLWithPath:path] setResourceValue:[NSNumber numberWithBool: YES]
                                                               forKey: NSURLIsExcludedFromBackupKey error: &error];
        if(!success){
            NSLog(@"Error excluding %@ from backup %@", [[NSURL fileURLWithPath:path] lastPathComponent], error);
        }
        //NSLog(@"result==%d",success);
    } else {
        u_int8_t b = 1;
        int result = setxattr([path fileSystemRepresentation], "com.apple.MobileBackup", &b, 1, 0, 0);
        //NSLog(@"result==%d",result);
    }
}

#pragma Appirater Methods
-(void) setAppRateAlert
{
    [Appirater setAppId:APPIRATER_APPID];
    [Appirater setDaysUntilPrompt:2];
    [Appirater setUsesUntilPrompt:2];
    [Appirater setSignificantEventsUntilPrompt:-1];
    [Appirater setTimeBeforeReminding:3];
    //[Appirater setDebug:YES];
}

-(NSString*)URLEncode:(NSString*) string
{
    ////NSLog(@"==BEFORE ENCODE=%@",string);
    NSMutableString *escaped = [[string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] mutableCopy];
    [escaped replaceOccurrencesOfString:@"=" withString:@"%3D" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@" " withString:@"%20" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@"\"" withString:@"%22" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@"\n" withString:@"%0A" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    ////NSLog(@"==AFTER ENCODE=%@",escaped);
    return escaped;
}

- (NSString *) md5:(NSString *) input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
    
}


#pragma Parse Push Notification Methods
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    DebugLog(@"didRegisterForRemoteNotificationsWithDeviceToken : devicetoken %@",deviceToken);
    [PFPush storeDeviceToken:deviceToken];
    [PFPush subscribeToChannelInBackground:@"" block:^(BOOL succeeded, NSError *error) {
        if (succeeded)
            DebugLog(@"Successfully subscribed to broadcast channel!");
        else
            DebugLog(@"Failed to subscribe to broadcast channel; Error: %@",error);
    }];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    DebugLog(@"didFailToRegisterForRemoteNotificationsWithError");
    if (error.code == 3010) {
        DebugLog(@"Push notifications are not supported in the iOS Simulator.");
    } else {
        // show some alert or otherwise handle the failure to register.
        DebugLog(@"application:didFailToRegisterForRemoteNotificationsWithError: %@", error);
	}
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    DebugLog(@"didReceiveRemoteNotification  : userInfo %@",userInfo);
    [PFPush handlePush:userInfo];
     UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateInactive) {
        DebugLog(@"UIApplicationStateInactive");
    } else if (state == UIApplicationStateBackground){
        DebugLog(@"UIApplicationStateBackground");
    } else if (state == UIApplicationStateActive){
        DebugLog(@"UIApplicationStateActive");
    } 
}

#pragma Reachability Methods
-(void) startCheckNetwork
{
    
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
	
    internetReach = [Reachability reachabilityForInternetConnection];
	[internetReach startNotifer];
    [self updateReachabitlityFlag:internetReach];
    
}

-(void) stopCheckNetwork
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
	internetReach = nil;
}

- (void)reachabilityChanged:(NSNotification *)note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	[self updateReachabilityStatus: curReach];
}

- (void) updateReachabilityStatus: (Reachability*) curReach
{
	if(curReach == internetReach)
	{
		[self updateReachabitlityFlag: curReach];
    }
}

- (void) updateReachabitlityFlag: (Reachability*) curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    BOOL connectionRequired=[curReach connectionRequired];
    
    switch (netStatus)
    {
        case NotReachable:
        {
            networkavailable =  NO;
            break;
        }
            
        case ReachableViaWWAN:
        {
			if(connectionRequired==NO)
			{
                networkavailable = YES;
			}
			else
			{
				//this is for invoking internet library
				NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:@"maps.google.com"] cachePolicy:NO timeoutInterval:15.0] ;
				
				NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
				if (theConnection) {
					connectionRequired	= NO;
				}
			} 
			break;
        }
        case ReachableViaWiFi:
        {
            if(connectionRequired==NO)
            {
                networkavailable = YES;
            }
            break;
		}
    }
    if(connectionRequired)
    {
		networkavailable = NO;
    }
    DebugLog(@"network status = %d",networkavailable);
    [self sendNetworkNotification];
}
#pragma Wifi Notification CallBack
-(void)sendNetworkNotification
{
    if (networkavailable) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NETWORK_NOTIFICATION object:nil];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    DebugLog(@"applicationWillEnterForeground");
    //[Appirater appEnteredForeground:YES];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [FBSession.activeSession close];
}


#pragma mark Facebook methods
- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState)state
                      error:(NSError *)error
{
    // FBSample logic
    // Any time the session is closed, we want to display the login controller (the user
    // cannot use the application unless they are logged in to Facebook). When the session
    // is opened successfully, hide the login controller and show the main UI.
    switch (state) {
        case FBSessionStateOpen: {
            //DebugLog(@"success");
        }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            // FBSample logic
            // Once the user has logged in, we want them to be looking at the root view.
            //[self.navController popToRootViewControllerAnimated:NO];
            
            [FBSession.activeSession closeAndClearTokenInformation];
            
            //[self showLoginView];
            break;
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SCSessionStateChangedNotification
                                                        object:session];
    
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:error.localizedDescription
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
    NSArray *permissions = [NSArray arrayWithObjects:@"email",nil];

    return [FBSession openActiveSessionWithPermissions:permissions
                                          allowLoginUI:allowLoginUI
                                     completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                         [self sessionStateChanged:session state:state error:error];
                                     }];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // FBSample logic
    // We need to handle URLs by passing them to FBSession in order for SSO authentication
    // to work.
    return [FBSession.activeSession handleOpenURL:url];
}

#pragma mark - Image Reflection

CGImageRef CreateGradientImage(int pixelsWide, int pixelsHigh)
{
	CGImageRef theCGImage = NULL;
    
	// gradient is always black-white and the mask must be in the gray colorspace
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
	
	// create the bitmap context
	CGContextRef gradientBitmapContext = CGBitmapContextCreate(NULL, pixelsWide, pixelsHigh,
															   8, 0, colorSpace, kCGImageAlphaNone);
	
	// define the start and end grayscale values (with the alpha, even though
	// our bitmap context doesn't support alpha the gradient requires it)
	CGFloat colors[] = {0.0, 1.0, 1.0, 1.0};
	
	// create the CGGradient and then release the gray color space
	CGGradientRef grayScaleGradient = CGGradientCreateWithColorComponents(colorSpace, colors, NULL, 2);
	CGColorSpaceRelease(colorSpace);
	
	// create the start and end points for the gradient vector (straight down)
	CGPoint gradientStartPoint = CGPointZero;
	CGPoint gradientEndPoint = CGPointMake(0, pixelsHigh);
	
	// draw the gradient into the gray bitmap context
	CGContextDrawLinearGradient(gradientBitmapContext, grayScaleGradient, gradientStartPoint,
								gradientEndPoint, kCGGradientDrawsAfterEndLocation);
	CGGradientRelease(grayScaleGradient);
	
	// convert the context into a CGImageRef and release the context
	theCGImage = CGBitmapContextCreateImage(gradientBitmapContext);
	CGContextRelease(gradientBitmapContext);
	
	// return the imageref containing the gradient
    return theCGImage;
}

CGContextRef MyCreateBitmapContext(int pixelsWide, int pixelsHigh)
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	
	// create the bitmap context
	CGContextRef bitmapContext = CGBitmapContextCreate (NULL, pixelsWide, pixelsHigh, 8,
														0, colorSpace,
														// this will give us an optimal BGRA format for the device:
														(kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst));
	CGColorSpaceRelease(colorSpace);
    
    return bitmapContext;
}

- (UIImage *)reflectedImage:(UIImageView *)fromImage withHeight:(NSUInteger)height
{
    if(height == 0)
		return nil;
    
	// create a bitmap graphics context the size of the image
	CGContextRef mainViewContentContext = MyCreateBitmapContext(fromImage.bounds.size.width, height);
	
	// create a 2 bit CGImage containing a gradient that will be used for masking the
	// main view content to create the 'fade' of the reflection.  The CGImageCreateWithMask
	// function will stretch the bitmap image as required, so we can create a 1 pixel wide gradient
	CGImageRef gradientMaskImage = CreateGradientImage(1, height);
	
	// create an image by masking the bitmap of the mainView content with the gradient view
	// then release the  pre-masked content bitmap and the gradient bitmap
	CGContextClipToMask(mainViewContentContext, CGRectMake(0.0, 0.0, fromImage.bounds.size.width, height), gradientMaskImage);
	CGImageRelease(gradientMaskImage);
	
	// In order to grab the part of the image that we want to render, we move the context origin to the
	// height of the image that we want to capture, then we flip the context so that the image draws upside down.
	CGContextTranslateCTM(mainViewContentContext, 0.0, height);
	CGContextScaleCTM(mainViewContentContext, 1.0, -1.0);
	
	// draw the image into the bitmap context
	CGContextDrawImage(mainViewContentContext, fromImage.bounds, fromImage.image.CGImage);
	
	// create CGImageRef of the main view bitmap content, and then release that bitmap context
	CGImageRef reflectionImage = CGBitmapContextCreateImage(mainViewContentContext);
	CGContextRelease(mainViewContentContext);
	
	// convert the finished reflection image to a UIImage
	UIImage *theImage = [UIImage imageWithCGImage:reflectionImage];
	
	// image is retained by the property setting above, so we can release the original
	CGImageRelease(reflectionImage);
	
	return theImage;
}

@end
