//
//  DMDMyVideosViewController.m
//  DancewithMD
//
//  Created by Rishi on 17/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDMyVideosViewController.h"
#import "DMDMyVideosDetailsViewController.h"
#import "DMDDownloadedVideosViewController.h"
#import "constants.h"

@interface DMDMyVideosViewController ()

@end

@implementation DMDMyVideosViewController
@synthesize myvideoTableView;
@synthesize folderArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    folderArray = [[NSMutableArray alloc] init];
    
    NSArray *dirPaths;
    NSString *fullPath;
    NSString *docsDir;
    NSArray *filelist;
    int count;
    int i;
    NSFileManager *filemgr;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    
    fullPath = [docsDir stringByAppendingPathComponent:@"Videos"];
    
    //NSLog(@"%@",fullPath);
    
    filemgr =[NSFileManager defaultManager];
    
    filelist = [filemgr contentsOfDirectoryAtPath:fullPath error:NULL];
    count = [filelist count];
    //NSLog(@"count = %d",count);
    
    for (i = 0; i < count; i++)
    {
        if(![[filelist objectAtIndex: i] isEqualToString:@".DS_Store"])
            [folderArray addObject:[filelist objectAtIndex: i]];
        //NSLog(@"%@", [filelist objectAtIndex: i]);
    }
     [myvideoTableView reloadData];
//    NSArray *theFiles =  [filemgr contentsOfDirectoryAtURL:[NSURL fileURLWithPath:fullPath]
//                                    includingPropertiesForKeys:[NSArray arrayWithObject:NSURLIsDirectoryKey]
//                                                       options:NSDirectoryEnumerationSkipsHiddenFiles
//                                                         error:nil];
//    
//    //NSLog(@"%@",theFiles);

}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [folderArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.userInteractionEnabled = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.font = KABEL_FONT(30);
        //cell.textLabel.text = @"This is test text.";
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:0.5];
        cell.selectedBackgroundView = bgColorView;
    }
     cell.textLabel.text = [folderArray objectAtIndex: indexPath.row];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    DMDMyVideosDetailsViewController *videodetailController = [[DMDMyVideosDetailsViewController alloc] initWithNibName:@"DMDMyVideosDetailsViewController" bundle:nil];
//    videodetailController.title = @"Downloaded Videos";
//    videodetailController.songID = [folderArray objectAtIndex:indexPath.row];
//    [self.navigationController pushViewController:videodetailController animated:YES];
//    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
    
    
    DMDDownloadedVideosViewController *videodetailController = [[DMDDownloadedVideosViewController alloc] initWithNibName:@"DMDDownloadedVideosViewController" bundle:nil];
    videodetailController.title = @"Downloaded Videos";
    videodetailController.songID = [folderArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:videodetailController animated:YES];
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setMyvideoTableView:nil];
    [super viewDidUnload];
}
@end
