//
//  SCViewController.m
//  MadhuriDixitHD
//
//  Created by Kirti Nikam on 22/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "SCViewController.h"
#import "DMDAppDelegate.h"
#import "DMDLoginViewController.h"
#import "constants.h"
#import "DMDMyVideosViewController.h"

//#define USER_REGISTRATION_LINK(USERID) [NSString stringWithFormat:@"%@/api/v1/retrieve/user_profile/%@/0",LIVE_DWM_SERVER,USERID]
//#define USER_ADD_LINK [NSString stringWithFormat:@"%@/api/v1/retrieve/user_profile",LIVE_DWM_SERVER]
//#define LOGIN_LINK [NSString stringWithFormat:@"%@/api/v1/retrieve/login",LIVE_DWM_SERVER]
//#define USER_DETAIL_LINK(USER_ID,TOKEN) [NSString stringWithFormat:@"%@/api/v1/retrieve/user_profile/%@/%@",LIVE_DWM_SERVER,USER_ID,TOKEN]
//#define VERSION_CHECK_LINK [NSString stringWithFormat:@"%@/api/v1/retrieve/validapi",LIVE_DWM_SERVER]

#define USER_REGISTRATION_LINK(USERID) [NSString stringWithFormat:@"%@/api/retrieve/user_profile/%@/0",LIVE_DWM_SERVER,USERID]
#define USER_ADD_LINK [NSString stringWithFormat:@"%@/api/retrieve/user_profile",LIVE_DWM_SERVER]
#define LOGIN_LINK [NSString stringWithFormat:@"%@/api/retrieve/login",LIVE_DWM_SERVER]
#define USER_DETAIL_LINK(USER_ID,TOKEN) [NSString stringWithFormat:@"%@/api/retrieve/user_profile/%@/%@",LIVE_DWM_SERVER,USER_ID,TOKEN]
#define VERSION_CHECK_LINK [NSString stringWithFormat:@"%@/api/retrieve/validapi",LIVE_DWM_SERVER]

NSString *const kPlaceholderPostMessage = @"Say something about this...";

@interface SCViewController ()<UINavigationControllerDelegate>

@property (strong, nonatomic) FBUserSettingsViewController *settingsViewController;
@property (strong, nonatomic) IBOutlet FBProfilePictureView *userProfileImage;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UIButton *announceButton;
@property (strong, nonatomic) IBOutlet UIButton *loginFBBtn;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
- (void)populateUserDetails;
- (void)centerAndShowActivityIndicator;
@end

@implementation SCViewController
@synthesize userNameLabel = _userNameLabel;
@synthesize userProfileImage = _userProfileImage;
@synthesize announceButton = _announceButton;
@synthesize activityIndicator = _activityIndicator;
@synthesize cancelButton = _cancelButton;
@synthesize settingsViewController = _settingsViewController;
@synthesize postParams = _postParams;
@synthesize appIcon = _appIcon;
@synthesize message = _message;
@synthesize loginFBBtn = _loginFBBtn;
@synthesize FBtitle = _FBtitle;
@synthesize FBtLink = _FBtLink;
@synthesize FBtPic = _FBtPic;
@synthesize showorhide = _showorhide;
@synthesize facebookLoginBtn = _facebookLoginBtn;
@synthesize FBtCaption = _FBtCaption;
@synthesize messageLabel = _messageLabel;

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)resetPostMessage
{
    self.message.text = @"";
    self.message.textColor = [UIColor blackColor];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    // Clear the message text when the user starts editing
    if ([textView.text isEqualToString:kPlaceholderPostMessage]) {
        [self resetPostMessage];
    }
    textView.textColor = [UIColor blackColor];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    // Reset to placeholder text if the user is done
    // editing and no message has been entered.
    if ([textView.text isEqualToString:@""]) {
        self.message.text = kPlaceholderPostMessage;
        textView.textColor = [UIColor blackColor];
        
    }
}
#pragma mark open graph


// FBSample logic
// Handles the user clicking the Announce button, by either creating an Open Graph Action
// or first uploading a photo and then creating the action.
- (IBAction)announce:(id)sender {
    
    // Add user message parameter if user filled it in
    if (![self.message.text isEqualToString:kPlaceholderPostMessage] &&
        ![self.message.text isEqualToString:@""]) {
        [self.postParams setObject:self.message.text forKey:@"message"];
    }
    
    [self.postParams setObject:self.FBtitle forKey:@"name"];
    [self.postParams setObject:self.FBtCaption forKey:@"caption"];
    [self.postParams setObject:self.FBtPic forKey:@"picture"];

    
    [FBRequestConnection
     startWithGraphPath:@"me/feed"
     parameters:self.postParams
     HTTPMethod:@"POST"
     completionHandler:^(FBRequestConnection *connection,
                         id result,
                         NSError *error) {
         NSString *alertText;
         if (error) {
             alertText = [NSString stringWithFormat:
                          @"error: domain = %@, code = %d",
                          error.domain, error.code];
         } else {
             //             alertText = [NSString stringWithFormat:
             //                          @"Posted action, id: %@",
             //                          [result objectForKey:@"id"]];
             alertText = @"Your message has been successfully posted on your facebook wall.";
         }
         // Show the result in an alert
         [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                     message:alertText
                                    delegate:nil
                           cancelButtonTitle:@"OK!"
                           otherButtonTitles:nil]
          show];
     }];
    
    [self dismissModalViewControllerAnimated:YES];
    
}


//- (void) alertView:(UIAlertView *)alertView
//didDismissWithButtonIndex:(NSInteger)buttonIndex
//{
//    [self resetPostMessage];
//
//}

- (void)centerAndShowActivityIndicator {
    CGRect frame = self.view.frame;
    CGPoint center = CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame));
    self.activityIndicator.center = center;
    [self.activityIndicator startAnimating];
    
}
// FBSample logic
// Displays the user's name and profile picture so they are aware of the Facebook
// identity they are logged in as.
- (void)populateUserDetails {
    if (FBSession.activeSession.isOpen) {
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
             if (!error) {
                 //NSLog(@"user ID %@",user.id);
//                 NSLog(@"user name %@",user.first_name);
//                 NSLog(@"user middle name %@",user.middle_name);
//                 NSLog(@"user last name %@",user.last_name);
//                 NSLog(@"user link %@",user.link);
//                 NSLog(@"user name %@",user.username);
//                 NSLog(@"user email %@",[user objectForKey:@"email"]);
//                 NSLog(@"user gender %@",[user objectForKey:@"gender"]);
                 if(self.showorhide==1)
                 {
                     [Flurry logEvent:@"Login via facebook"];
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     [defaults setObject:FACEBOOKSERVER forKey:LOGGED_BY];
                     [defaults setObject:user.id forKey:LOGIN_USERID];
                     [defaults setObject:[FBSession.activeSession accessToken] forKey:LOGIN_TOKEN];
                     [defaults setObject:[NSString stringWithFormat:@"%@ %@", [user first_name],[user last_name]] forKey:LOGIN_USERNAME];
                     [defaults setObject:@"" forKey:LOGIN_PASSWORD];
                     [defaults setObject:[user objectForKey:@"email"] forKey:LOGIN_EMAIL];
                     [defaults setObject:[user objectForKey:@"gender"] forKey:LOGIN_GENDER];
                     [defaults synchronize];
                     if(connection1==nil) {
                         //NSLog(@"New Connection");
                         [self checkRegistraitionAsynchronousCall];
                     } else {
                         //NSLog(@"Already a  Connection");
                     }
                     [self cancelModalView:nil];
                     return ;
                 }
                 //DebugLog(@"%@",user.name);
                 self.facebookLoginBtn.hidden = TRUE;
                 self.userNameLabel.text = user.name;
                 self.userProfileImage.profileID = [user objectForKey:@"id"];
                 _announceButton.enabled =  TRUE;
                 _announceButton.hidden = FALSE;
                 self.mainFBView.hidden = FALSE;
                 self.facebookLoginBtn.hidden = TRUE;
                 //[_loginFBBtn setTitle:@"Logout" forState:UIControlStateNormal];
                 _loginFBBtn.hidden = TRUE;
             } else {
                 _announceButton.enabled =  FALSE;
                 _announceButton.hidden = TRUE;
                 _loginFBBtn.hidden = FALSE;
                 self.facebookLoginBtn.hidden = FALSE;
                 self.mainFBView.hidden = TRUE;
                 //[_loginFBBtn setTitle:@"Login" forState:UIControlStateNormal];
             }
         }];
    }
}




- (IBAction)loginFB:(id)sender {
    if([DMDDelegate networkavailable]) {
        UIButton * button = (UIButton*) sender;
        if([button.titleLabel.text isEqualToString:@"Login"]) {
            //[self didShowModalViewController];
            [self showLoginPopOverController];
        } else {
            [FBSession.activeSession closeAndClearTokenInformation];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:@"" forKey:@"userId"];
            [defaults setObject:@"" forKey:@"accessToken"];
            [defaults synchronize];
            //[self resetPostMessage];
            _announceButton.enabled =  FALSE;
            //[_loginFBBtn setTitle:@"Login" forState:UIControlStateNormal];
        }
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
    }
}

- (void)cancelModalView:(id)sender {
    [self.view.layer removeAllAnimations];
    if (![[self modalViewController] isBeingDismissed])
        [self dismissModalViewControllerAnimated:YES];
    [self dismissPopOverController];

}

-(void)checkApiVersionAsynchronousCall
{
	/****************Asynchronous Request**********************/
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:VERSION_CHECK_LINK] cachePolicy:NO timeoutInterval:10.0];
    //NSLog(@"%@",urlRequest.URL);
    connectionVersionCheck = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)checkRegistraitionAsynchronousCall
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:@"userId"];
	/****************Asynchronous Request**********************/
    NSString *requestString = [NSString stringWithString:USER_REGISTRATION_LINK(userID)];
    //NSLog(@"%@",requestString);
	/****************Asynchronous Request**********************/
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:requestString] cachePolicy:NO timeoutInterval:10.0];
    
    connection1 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

//[self insertUserInDB:[user id] name:[user first_name] email:[user objectForKey:@"email"] gender:[user objectForKey:@"gender"] picture:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture", user.id]];
-(void)insertUserInDB
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	NSString *userId = [userDefaults stringForKey:@"userId"];
    NSString *userName = [userDefaults stringForKey:@"userName"];
    NSString *userEmail = [userDefaults stringForKey:@"userEmail"];
    NSString *userGender = [userDefaults stringForKey:@"gender"];
    NSString *userPic = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture", [userDefaults stringForKey:@"userId"]];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:USER_ADD_LINK] cachePolicy:NO timeoutInterval:30.0];
    NSMutableDictionary *postReq = [[NSMutableDictionary alloc] init];
    [postReq setObject:userId forKey:@"User_ID"];
    [postReq setObject:userName forKey:@"User_Name"];
    if(userEmail!=nil)
        [postReq setObject:userEmail forKey:@"Email"];
    else
        [postReq setObject:@"" forKey:@"Email"];
    [postReq setObject:userGender forKey:@"Gender"];
    [postReq setObject:userPic forKey:@"Picture"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReq
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (!jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //NSLog(@"---%@---",jsonString);
    }
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody: jsonData];
    connection2 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)loginAsynchronousCall:(NSString *)userEmail pass:(NSString *)password
{
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LOGIN_LINK] cachePolicy:NO timeoutInterval:30.0];
    NSString *md5Pass = [DMDDelegate md5:password];
    NSMutableDictionary *postReq = [[NSMutableDictionary alloc] init];
    [postReq setObject:userEmail forKey:@"uname"];
    [postReq setObject:md5Pass forKey:@"pass"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReq
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (!jsonData) {
        //NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //NSLog(@"---%@---",jsonString);
    }
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody: jsonData];
    connectionLogin = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)userDetailsAsynchronousCall:(NSString *)userId token:(NSString *)userToken
{
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:USER_DETAIL_LINK(userId,userToken)] cachePolicy:NO timeoutInterval:5.0];
    //NSLog(@"link=%@", urlRequest.URL);
    connectionUserDetail = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        status = [httpResponse statusCode];
        //NSLog(@"res code=%d",status);
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"%@",error.description);
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //NSLog(@"DONE. Received Bytes: %d", [responseAsyncData length]);

     NSError* error;
    if (connection==connection1) {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSArray *dataArray = [json objectForKey:@"data"];
                   // NSLog(@"dataArray %@",dataArray);

                    NSDictionary* attributeDict = [dataArray objectAtIndex:0];
                    if([attributeDict valueForKey:@"ID"] != nil) {
                        //NSLog(@"User exists");
                    }
                    else {
                        //NSLog(@"User NOT exists");
                        [self insertUserInDB];
                    }
                }else{
                    NSString *message = [json objectForKey:@"message"];
                    //NSLog(@"status false :( message %@",message);
                }
            }
        }
        connection1 = nil;
        responseAsyncData = nil;
    } else if (connection==connection2) {
        NSString *result = [[NSString alloc] initWithBytes:[responseAsyncData mutableBytes]
                                                    length:[responseAsyncData length] encoding:NSASCIIStringEncoding];
        //NSLog(@"res string=%@", result);
        connection2 = nil;
        responseAsyncData = nil;
    }
    else if (connection==connectionLogin)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSDictionary *dataDict = [json objectForKey:@"data"];
                        if([dataDict valueForKey:@"token"] != nil) {
                            NSString * result = [dataDict objectForKey:@"token"];
                            //NSLog(@"from scview: token==%@",result);
                            //[self showAlert:[NSString stringWithFormat:@"Login Success. Token = %@",result]];
                            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                            [defaults setObject:result forKey:LOGIN_TOKEN];
                            NSDate *myDate = [NSDate date];
                            [defaults setObject:myDate forKey:LOGIN_EXPIREDDATE];
                            [defaults synchronize];
                            [self performSelector:@selector(cancelModalView:) withObject:nil afterDelay:2.0];
                            //[self userDetailsAsynchronousCall:loginusernametextfield.text token:[attributeDict objectForKey:@"token"]];
                            //                        if(self.delegate != nil)
                            //                            [self.delegate cancelModalView:nil];
                        } else {
                            NSString * result = [dataDict objectForKey:@"data"];
                            //NSLog(@"%@",result);
                            if(result != nil)
                            {
                                [self showAlert:result];
                            }
                    }
                } else {
                    NSString * result = [json objectForKey:@"message"];
                    [self showAlert:result];
                }
            }
        }
        responseAsyncData = nil;
        connectionLogin = nil;
    }
    else if (connection==connectionUserDetail)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* attributeDict in dataArray)
                    {
                        if([attributeDict valueForKey:@"User_Name"] != nil) {
                            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                            [defaults setObject:[attributeDict valueForKey:@"ID"] forKey:LOGIN_USERID];
                            [defaults setObject:[attributeDict valueForKey:@"User_Name"] forKey:LOGIN_USERNAME];
                            [defaults setObject:[attributeDict valueForKey:@"Email"] forKey:LOGIN_EMAIL];
                            [defaults setObject:[attributeDict valueForKey:@"Gender"] forKey:LOGIN_GENDER];
                            [defaults synchronize];
                            //NSLog(@"%@--%@--%@--%@",[defaults objectForKey:LOGIN_USERID],[defaults objectForKey:LOGIN_USERNAME],[defaults objectForKey:LOGIN_EMAIL],[defaults objectForKey:LOGIN_GENDER]);
                            //if(self.delegate != nil)
                                //[self.delegate cancelModalView:nil];
                        } else {
                            //NSString * result = [attributeDict objectForKey:@"data"];
                            //NSLog(@"%@",result);
    //                        if(result != nil && [result isEqualToString:@"0"])
    //                            [self showAlert:@"Failed to login.Please try after sometime."];
                        }
                    }
                }
            }
        }
        responseAsyncData = nil;
        connectionUserDetail = nil;
    } else if (connection==connectionVersionCheck)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            if(json != nil) {
                if([json valueForKey:@"success"] != nil && ([[json valueForKey:@"success"] boolValue] == 0 || [[json valueForKey:@"success"] hasPrefix:@"false"])) {
                    //NSLog(@"success false");
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                            message:@"Please update the app to proceed."
                                                                           delegate:nil
                                                                  cancelButtonTitle:@"OK"
                                                                  otherButtonTitles:nil];
                        [alertView show];
                    }
                }
        }
        connectionVersionCheck = nil;
        responseAsyncData = nil;
    }
}

-(void)showAlert:(NSString*) message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //NSLog(@"--viewDidLoad--");
    self.postParams =
    [[NSMutableDictionary alloc] initWithObjectsAndKeys:
     @"http://www.dancewithmadhuri.com", @"link",
     @"http://madhuridixit-nene.com/uploads/Madhuri_App_Icon.png", @"picture",
     @"Title of Post", @"name",
     @"", @"caption",
     @"A revolutionized way of learning dance with the Bollywood Dance Diva herself. Come Dance with Madhuri.", @"description",
     nil];
    UIImage *cancelButtonImage;
    cancelButtonImage = [[UIImage imageNamed:@"DEFacebookSendButtonPortrait"] stretchableImageWithLeftCapWidth:4 topCapHeight:0];
    [self.cancelButton setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
    [self.announceButton setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
    [self.loginFBBtn setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
    //_cancelButton.frame = CGRectMake(8, 7, 63, 30);
    //    self.postParams =
    //    [[NSMutableDictionary alloc] initWithObjectsAndKeys:
    //     @"https://developers.facebook.com/ios", @"link",
    //     @"https://developers.facebook.com/attachment/iossdk_logo.png", @"picture",
    //     @"Facebook SDK for iOS", @"name",
    //     @"Build great social apps and get more installs.", @"caption",
    //     @"The Facebook SDK for iOS makes it easier and faster to develop Facebook integrated iOS apps.", @"description",
    //     nil];
    
    // Set up the post information, hard-coded for this sample
    //self.name.text = [self.postParams objectForKey:@"name"];
    //self.caption.text = [self.postParams objectForKey:@"caption"];
    //[self.caption sizeToFit];
    //self.description.text = [self.postParams objectForKey:@"description"];
    //[self.description sizeToFit];
    
    //self.message.text = [NSString stringWithFormat:@"%@\n", self.FBtitle];
    self.message.delegate =  self;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithTitle:@"Settings"
                                              style:UIBarButtonItemStyleBordered
                                              target:self
                                              action:@selector(settingsButtonWasPressed:)];
  
  
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityIndicator.hidesWhenStopped = YES;
    [self.view addSubview:self.activityIndicator];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sessionStateChanged:)
                                                 name:SCSessionStateChangedNotification
                                               object:nil];
    self.mainFBView.layer.cornerRadius = 10;
    self.mainFBView.layer.masksToBounds = YES;
    
    _announceButton.hidden = TRUE;
    _loginFBBtn.hidden = FALSE;
    
    [DMDDelegate openSessionWithAllowLoginUI:NO];
    
     [self.view setMultipleTouchEnabled:YES];

    _messageLabel.font = KABEL_FONT(20);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:LOGIN_USERID];
    NSString *userPass = [defaults objectForKey:LOGIN_PASSWORD];
    if(userID != nil && userPass != nil && [userID length] > 0 && [userPass length] > 0)
    {
        //NSLog(@"Login called from SCViewController %@ %@",userID,userPass);
        if([DMDDelegate networkavailable]) {
            [self loginAsynchronousCall:userID pass:userPass];
        } else {
            UIAlertView *errorView = [[UIAlertView alloc]
                                      initWithTitle:@"No Network Connection"
                                      message:@"Please check your internet connection and try again."
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [errorView show];
        }
    }
    if([DMDDelegate networkavailable]) {
        [self checkApiVersionAsynchronousCall];
    }
}

/*
 * A simple way to dismiss the message text view:
 * whenever the user clicks outside the view.
 */
//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *) event
//{
//    UITouch *touch = [[event allTouches] anyObject];
//    if ([self.message isFirstResponder] &&
//        (self.message != touch.view))
//    {
//        [self.message resignFirstResponder];
//    }
//}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (FBSession.activeSession.isOpen) {
        [self populateUserDetails];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [_message becomeFirstResponder];
}

-(void)showLoginPopOverController
{
    loginController = [[DMDLoginViewController alloc] initWithNibName:@"DMDLoginViewController" bundle:nil];
    loginController.delegate = self;
    popoverController = [[UIPopoverController alloc] initWithContentViewController:loginController];
    CGRect popRect = CGRectMake(self.facebookLoginBtn.frame.origin.x,
                                self.facebookLoginBtn.frame.origin.y,
                                self.facebookLoginBtn.frame.size.width,
                                self.facebookLoginBtn.frame.size.height);
    [popoverController presentPopoverFromRect:popRect
                                       inView:self.view
                     permittedArrowDirections:UIPopoverArrowDirectionAny
                                     animated:YES];
}

-(void)dismissPopOverController
{
    if ([popoverController isPopoverVisible]) {
        [popoverController dismissPopoverAnimated:NO];
    }
}

- (IBAction)offlineModeBtnPressed:(id)sender {
    myvideosController = [[DMDMyVideosViewController alloc] initWithNibName:@"DMDMyVideosViewController" bundle:nil];
    offlinenavController = [[UINavigationController alloc] initWithRootViewController:myvideosController];
    offlinenavController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    offlinenavController.modalPresentationStyle = UIModalPresentationFullScreen;
    offlinenavController.navigationBar.tintColor = BLACK_COLOR;
    myvideosController.navigationItem.title = @"My Videos";

    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                       target:self
                                                                                       action:@selector(didDismissOfflineViewController)];
    myvideosController.navigationItem.rightBarButtonItem = doneBarButton;
    [self presentViewController:offlinenavController animated:YES completion:NULL];
}

- (void)didDismissOfflineViewController
{
    [offlinenavController dismissViewControllerAnimated:YES completion:NULL];
    myvideosController = nil;
    offlinenavController = nil;
}

-(void)didShowModalViewController
{
    loginController = [[DMDLoginViewController alloc] initWithNibName:@"DMDLoginViewController" bundle:nil];
    loginnavController = [[UINavigationController alloc] initWithRootViewController:loginController];
    loginnavController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    loginnavController.modalPresentationStyle = UIModalPresentationFormSheet;
    loginnavController.navigationBar.tintColor = BLACK_COLOR;
    loginController.navigationItem.title = @"Login";
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                   target:self
                                                                                   action:@selector(didDismissPresentedViewController)];
    loginController.navigationItem.rightBarButtonItem = doneBarButton;
    loginController.delegate = self;
    [self presentViewController:loginnavController animated:YES completion:NULL];
}

- (void)didDismissPresentedViewController
{
    [loginnavController dismissViewControllerAnimated:YES completion:NULL];
    loginController.delegate = nil;
    loginController = nil;
    loginnavController = nil;
}

-(void)settingsButtonWasPressed:(id)sender {
    if (self.settingsViewController == nil) {
        self.settingsViewController = [[FBUserSettingsViewController alloc] init];
    }
    [self.navigationController pushViewController:self.settingsViewController animated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setCancelButton:nil];
    [self setAnnounceButton:nil];
    [self setUserNameLabel:nil];
    [self setMessage:nil];
    [self setLoginFBBtn:nil];
    [self setMainFBView:nil];
    [self setFacebookLoginBtn:nil];
    [self setMessageLabel:nil];
    [self setOfflineModeBtn:nil];
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}

- (void)sessionStateChanged:(NSNotification*)notification {
    // A more complex app might check the state to see what the appropriate course of
    // action is, but our needs are simple, so just make sure our idea of the session is
    // up to date and repopulate the user's name and picture (which will fail if the session
    // has become invalid).
    [self populateUserDetails];
}

@end
