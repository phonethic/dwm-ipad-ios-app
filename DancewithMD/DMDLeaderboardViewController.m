//
//  DMDLeaderboardViewController.m
//  DancewithMD
//
//  Created by Rishi on 22/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDLeaderboardViewController.h"
#import "DMDLeaderboardObj.h"
#import "MFSideMenu.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "constants.h"
#import "DMDAppDelegate.h"

//#define USER_RANK_LINK(USERID) [NSString stringWithFormat:@"%@/api/v1/retrieve/rank/%@",LIVE_DWM_SERVER,USERID]
//#define USER_PIC(USERID) [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture",USERID]
//#define BOARD_LIST_LINK [NSString stringWithFormat:@"%@/api/v1/retrieve/leaderboard",LIVE_DWM_SERVER]
//#define COUNTER_LINK [NSString stringWithFormat:@"%@/api/v1/retrieve/counter",LIVE_DWM_SERVER]

#define USER_RANK_LINK(USERID) [NSString stringWithFormat:@"%@/api/retrieve/rank/%@",LIVE_DWM_SERVER,USERID]
#define USER_PIC(USERID) [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture",USERID]
#define BOARD_LIST_LINK [NSString stringWithFormat:@"%@/api/retrieve/leaderboard",LIVE_DWM_SERVER]
#define COUNTER_LINK [NSString stringWithFormat:@"%@/api/retrieve/counter",LIVE_DWM_SERVER]

@interface DMDLeaderboardViewController ()

@end

@implementation DMDLeaderboardViewController
@synthesize leaderboardArray;
@synthesize leaderboardTable;
@synthesize myranklbl;
@synthesize pointslbl;
@synthesize namelbl;
@synthesize userPicImgView;
@synthesize dancerslbl;
@synthesize spinnerIndicatior;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Flurry logEvent:@"Tab_LeaderBoard"];

    leaderboardArray = [[NSMutableArray alloc] init];
    if([DMDDelegate networkavailable])
    {
        [self leaderboardListAsynchronousCall];
        [self myRankAsynchronousCall];
        [self dancersAsynchronousCall];
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        [spinnerIndicatior stopAnimating];
        
    }
    
    //[self setupMenuBarButtonItems];
    
    namelbl.font = KABEL_FONT(30);
    myranklbl.font = KABEL_FONT(30);
    pointslbl.font = KABEL_FONT(30);
    dancerslbl.font = KABEL_FONT(30);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:@"userId"];
    userPicImgView.layer.cornerRadius = 25;
    userPicImgView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    userPicImgView.layer.borderWidth = 4;
    userPicImgView.layer.masksToBounds = TRUE;
    [userPicImgView setImageWithURL:[NSURL URLWithString:USER_PIC(userID)]
                   placeholderImage:[UIImage imageNamed:@"favicon.png"]
                            success:^(UIImage *image) {
                                //DebugLog(@"success");
                            }
                            failure:^(NSError *error) {
                                //DebugLog(@"write error %@", error);
                            }];

}

- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)leaderboardListAsynchronousCall
{
    [spinnerIndicatior startAnimating];
	/****************Asynchronous Request**********************/
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:BOARD_LIST_LINK] cachePolicy:NO timeoutInterval:15.0];
    
    //Get list of all songs
    connection1 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)myRankAsynchronousCall
{
	/****************Asynchronous Request**********************/
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:@"userId"];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:USER_RANK_LINK(userID)] cachePolicy:NO timeoutInterval:5.0];
    
    //Get list of all songs
    connection2 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)dancersAsynchronousCall
{
	/****************Asynchronous Request**********************/
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:COUNTER_LINK] cachePolicy:NO timeoutInterval:15.0];
    
    //Get list of all songs
    connection3 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
        //NSLog(@"URL-%@-",connection.currentRequest.URL);
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //NSLog(@"Error: %@", [error localizedDescription]);
    [spinnerIndicatior stopAnimating];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [spinnerIndicatior stopAnimating];
    NSError* error;
    if(connection==connection1)
    {
        //NSLog(@"con 1 length = %d",[responseAsyncData length]);
        if (responseAsyncData != nil && [responseAsyncData length] > 0) {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            if(json != nil) {
                    NSString *statusValue = [json objectForKey:@"success"];
                    if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                        [leaderboardArray removeAllObjects];
                        NSArray *dataArray = [json objectForKey:@"data"];
                        for (NSDictionary* songattributeDict in dataArray)
                        {
                            boardObj = [[DMDLeaderboardObj alloc] init];
                            boardObj.userId = [songattributeDict objectForKey:@"User_ID"];
                            boardObj.userRank = [songattributeDict objectForKey:@"rank"];
                            boardObj.userName = [songattributeDict objectForKey:@"User_Name"];
                            boardObj.userPoints = [songattributeDict objectForKey:@"points"];
                            ////NSLog(@"\n songid: %@ , name: %@  , clipid: %@  , sequence: %@  , shareid: %@ \n", songDetailObj.songId, songDetailObj.songName, songDetailObj.songClip_Part_ID, songDetailObj.songSequence, songDetailObj.songShare_ID);
                            [leaderboardArray addObject:boardObj];
                            boardObj = nil;
                        }
                    ////NSLog(@"detail array count: %d", [songDetailArray count]);
                    [leaderboardTable reloadData];
                }
            }
        }
        responseAsyncData = nil;
        connection1 = nil;
    } else if (connection==connection2) {
        //NSLog(@"con 2 length = %d",[responseAsyncData length]);
        if (responseAsyncData != nil && [responseAsyncData length] > 0) {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            if (json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* attributeDict in dataArray)
                    {
                        namelbl.text = [attributeDict objectForKey:@"User_Name"];
                        myranklbl.text = [attributeDict objectForKey:@"rank"];
                        pointslbl.text = [attributeDict objectForKey:@"points"];
                    }
                }
            }
        }
        responseAsyncData = nil;
    } else if(connection==connection3) {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            if (json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSDictionary *datadict = [json objectForKey:@"data"];
                    dancerslbl.text = [datadict objectForKey:@"count"];
                }
            }
        }
        responseAsyncData = nil;
    }
}

#pragma mark Table view methods
/*
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section   // custom view for header. will be adjusted to default or specified header height
{
    UILabel *lblRank;
    UILabel *lblName;
    UILabel *lblPoints;
    if(headerView == nil) {
        //allocate the view if it doesn't exist yet
        headerView  = [[UIView alloc] initWithFrame:CGRectMake(0.0f,0.0f,800.0f,50)];
        headerView.backgroundColor = [UIColor grayColor];
        
        //Initialize Label with tag 2.(Title Label)
        lblRank = [[UILabel alloc] initWithFrame:CGRectMake(80.0+80,0.0,100.0,50.0)];
        lblRank.tag = 1;
        lblRank.text = @"Rank";
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblTitle.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblRank.font = [UIFont fontWithName:@"Cochin-Bold" size:30];
        lblRank.textAlignment = UITextAlignmentLeft;
        lblRank.textColor = [UIColor whiteColor];
        lblRank.backgroundColor =  [UIColor grayColor];
        
        
        //Initialize Label with tag 2.(Title Label)
        lblName = [[UILabel alloc] initWithFrame:CGRectMake(300.0+80,0.0,100.0,50.0)];
        lblName.tag = 2;
        lblName.text = @"Name";
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblMovie.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblName.font = [UIFont fontWithName:@"Cochin-Bold" size:30];
        lblName.textAlignment = UITextAlignmentLeft;
        lblName.textColor = [UIColor whiteColor];
        lblName.backgroundColor =  [UIColor grayColor];
        
        //Initialize Label with tag 2.(Title Label)
        lblPoints = [[UILabel alloc] initWithFrame:CGRectMake(600.0+80,0.0,100.0,50.0)];
        lblPoints.tag = 3;
        lblPoints.text = @"Points";
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblMovie.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblPoints.font = [UIFont fontWithName:@"Cochin-Bold" size:30];
        lblPoints.textAlignment = UITextAlignmentLeft;
        lblPoints.textColor = [UIColor whiteColor];
        lblPoints.backgroundColor =  [UIColor grayColor];
        
        
        //add the button to the view
        [headerView insertSubview:lblRank atIndex:1];
        [headerView insertSubview:lblName atIndex:2];
        [headerView insertSubview:lblPoints atIndex:3];
        
    }
    
    //return the view for the footer
    return headerView;
} */

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [leaderboardArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UILabel *lblRank;
    UILabel *lblName;
    UILabel *lblPoints;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.userInteractionEnabled = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //        cell.textLabel.textColor = [UIColor whiteColor];
        //        cell.textLabel.font = [UIFont systemFontOfSize:24.0];
        //        cell.detailTextLabel.textColor = [UIColor lightGrayColor];
        //        cell.detailTextLabel.font = [UIFont systemFontOfSize:20.0];
        
        //Initialize Label with tag 2.(Title Label)
        lblRank = [[UILabel alloc] initWithFrame:CGRectMake(60.0,20.0,100.0,50.0)];
        lblRank.tag = 1;
        lblRank.text = @"Rank";
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblTitle.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblRank.font = KABEL_FONT(30);
        lblRank.textAlignment = UITextAlignmentCenter;
        lblRank.textColor = [UIColor whiteColor];
        lblRank.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblRank];
        
        //Initialize Label with tag 2.(Title Label)
        lblName = [[UILabel alloc] initWithFrame:CGRectMake(200.0+70,20.0,300.0,50.0)];
        lblName.tag = 2;
        lblName.text = @"Name";
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblMovie.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblName.font = KABEL_FONT(30);
        lblName.textAlignment = UITextAlignmentCenter;
        lblName.textColor = [UIColor whiteColor];
        lblName.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblName];
        
        
        //Initialize Label with tag 2.(Title Label)
        lblPoints = [[UILabel alloc] initWithFrame:CGRectMake(740.0,20.0,100.0,50.0)];
        lblPoints.tag = 3;
        lblPoints.text = @"Points";
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblMovie.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblPoints.font = KABEL_FONT(30);
        lblPoints.textAlignment = UITextAlignmentLeft;
        lblPoints.textColor = [UIColor whiteColor];
        lblPoints.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblPoints];
        
        UIImageView *seperatorImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 80, 988, 8)];
        seperatorImg.image = [UIImage imageNamed:@"leaderboard_table_line.png"];
        seperatorImg.contentMode = UIViewContentModeScaleAspectFit;
        [cell.contentView addSubview:seperatorImg];
    }
    
    cell.userInteractionEnabled = NO;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    DMDLeaderboardObj *tempboardObj  = [leaderboardArray objectAtIndex:indexPath.row];
    lblRank = (UILabel *)[cell viewWithTag:1];
    lblName = (UILabel *)[cell viewWithTag:2];
    lblPoints = (UILabel *)[cell viewWithTag:3];
    lblRank.text = tempboardObj.userRank;
    lblName.text = tempboardObj.userName;
    lblPoints.text = tempboardObj.userPoints;
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}

- (void)viewDidUnload {
    [self setLeaderboardTable:nil];
    [self setMyranklbl:nil];
    [self setNamelbl:nil];
    [self setPointslbl:nil];
    [self setUserPicImgView:nil];
    [self setDancerslbl:nil];
    [self setSpinnerIndicatior:nil];
    [super viewDidUnload];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
