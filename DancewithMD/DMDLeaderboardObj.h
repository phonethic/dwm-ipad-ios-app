//
//  DMDLeaderboardObj.h
//  DancewithMD
//
//  Created by Rishi on 20/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DMDLeaderboardObj : NSObject {
    
}

@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *userRank;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *userPoints;

@end
