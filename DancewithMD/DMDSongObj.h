//
//  DMDSongObj.h
//  DancewithMD
//
//  Created by Rishi on 12/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DMDSongObj : NSObject {
    
}

@property (nonatomic, copy) NSString *songId;
@property (nonatomic, copy) NSString *songTitle;
@property (nonatomic, copy) NSString *songMovie;
@property (nonatomic, copy) NSString *songChoreographer;
@property (nonatomic, copy) NSString *songPic;
@property (nonatomic, copy) NSString *songLock;
@property (nonatomic, copy) NSString *songCategory;
@property (nonatomic, copy) NSString *songCategoryName;
@property (nonatomic, copy) NSString *isVideo;
@property (nonatomic, copy) NSString *videoLink;
@end
