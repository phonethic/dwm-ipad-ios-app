//
//  DMDUploadVideoViewController.h
//  DancewithMD
//
//  Created by Rishi on 25/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>

@interface DMDUploadVideoViewController : UIViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UIPopoverControllerDelegate,NSURLConnectionDataDelegate,NSURLConnectionDelegate>
{
    UIPopoverController *popover;
    int status;
    NSMutableData *responseAsyncData;
    NSURLConnection *connection1;
}

@property (strong, nonatomic) UIProgressView *progressBar;

- (IBAction)submissionBtnPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *submitProgressBar;

@end
