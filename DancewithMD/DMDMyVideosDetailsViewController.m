//
//  DMDMyVideosDetailsViewController.m
//  DancewithMD
//
//  Created by Rishi on 17/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDMyVideosDetailsViewController.h"
#import "constants.h"

@interface DMDMyVideosDetailsViewController ()

@end

@implementation DMDMyVideosDetailsViewController
@synthesize downloadedvideoTableView;
@synthesize songID;
@synthesize tempIndex;
@synthesize videoArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    videoArray = [[NSMutableArray alloc] init];
    
    NSArray *dirPaths;
    NSString *fullPath;
    NSString *docsDir;
    NSArray *filelist;
    int count;
    int i;
    NSFileManager *filemgr;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    
    fullPath = [docsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"Videos/%@",self.songID]];
    
    //NSLog(@"%@",fullPath);
    
    filemgr =[NSFileManager defaultManager];
    
    filelist = [filemgr contentsOfDirectoryAtPath:fullPath error:NULL];
    count = [filelist count];
    //NSLog(@"count = %d",count);
    
    for (i = 0; i < count; i++)
    {
        [videoArray addObject:[filelist objectAtIndex: i]];
        //NSLog(@"%@", [filelist objectAtIndex: i]);
    }
    
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [videoArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.userInteractionEnabled = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.font = [UIFont systemFontOfSize:28.0];
        //cell.textLabel.text = @"This is test text.";
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(800.0,25.0,150.0,57.0);
		button.tag = 1;
        [button setImage:[UIImage imageNamed:@"delete_button.png"] forState:UIControlStateNormal];
        button.imageView.tag = 10;
        // set the button's target to this table view controller so we can interpret touch events and map that to a NSIndexSet
        [button addTarget:self action:@selector(deleteVideoButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        cell.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:button];
        
    }
    cell.textLabel.text = [videoArray objectAtIndex:indexPath.row];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
}


- (void)deleteVideoButtonTapped:(id)sender event:(id)event
{
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:downloadedvideoTableView];
	NSIndexPath *indexPath = [downloadedvideoTableView indexPathForRowAtPoint: currentTouchPosition];
	if (indexPath != nil)
	{
        tempIndex = indexPath;
        
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                          message:@"Are you sure you want to delete this video ?"
                                                         delegate:self
                                                cancelButtonTitle:@"NO"
                                                otherButtonTitles:@"YES",nil];
        [message show];
        
        //NSLog(@"%d",indexPath.row);
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"NO"])
    {
        //NSLog(@"NO was selected.");
    }
    else if([title isEqualToString:@"YES"])
    {
        //NSLog(@"YES was selected.");
        [self deleteVideo:[videoArray objectAtIndex:tempIndex.row] index:tempIndex.row];
        tempIndex = nil;
    }
}


-(void)deleteVideo:(NSString *)fileName index:(NSInteger)arrayIndex
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    
    NSArray *dirPaths;
    NSString *fullPath;
    NSString *docsDir;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    
    fullPath = [docsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"Videos/%@/%@",self.songID,fileName]];
    
    //NSLog(@"%@",fullPath);
    
    BOOL result = [fileManager removeItemAtPath:fullPath error:NULL];
    if(result) {
        [videoArray removeObjectAtIndex:arrayIndex];
        [downloadedvideoTableView reloadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setDownloadedvideoTableView:nil];
    [super viewDidUnload];
}
@end
