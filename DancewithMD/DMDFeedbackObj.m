//
//  DMDFeedbackObj.m
//  DancewithMD
//
//  Created by Rishi on 25/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDFeedbackObj.h"

@implementation DMDFeedbackObj

@synthesize userId;
@synthesize userName;
@synthesize picture;
@synthesize feedbackText;
@synthesize name;

#pragma mark NSCoding Protocol
- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:self.userId forKey:@"userId"];
    [coder encodeObject:self.userName forKey:@"userName"];
    [coder encodeObject:self.picture forKey:@"picture"];
    [coder encodeObject:self.feedbackText forKey:@"feedbackText"];
    [coder encodeObject:self.name forKey:@"name"];
}

- (id)initWithCoder:(NSCoder *)coder {
	self = [super init];
	if (self != nil) {
        self.userId         = [coder decodeObjectForKey:@"userId"];
        self.userName       = [coder decodeObjectForKey:@"userName"];
        self.picture        = [coder decodeObjectForKey:@"picture"];
        self.feedbackText   = [coder decodeObjectForKey:@"feedbackText"];
        self.name           = [coder decodeObjectForKey:@"name"];
	}
	return self;
}
@end
