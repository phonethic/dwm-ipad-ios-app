//
//  DMDViewController.m
//  DancewithMD
//
//  Created by Rishi on 06/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "MFSideMenu.h"
#import "DMDSongObj.h"
#import "DMDSongDetailViewController.h"
#import "SCViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DMDAppDelegate.h"

//#define SONG_CATEGORY_DETAIL_LIST_LINK(ID) [NSString stringWithFormat:@"%@/api/v1/retrieve/songinfo/%@",LIVE_DWM_SERVER,ID]

#define VIDEO_IMAGE_LINK(IMG_NAME) [NSString stringWithFormat:@"%@/%@",LIVE_DWM_SERVER,IMG_NAME]

#define SONG_CATEGORY_DETAIL_LIST_LINK(ID) [NSString stringWithFormat:@"%@/api/retrieve/songinfo/%@",LIVE_DWM_SERVER,ID]


@interface DMDViewController ()

@end

@implementation DMDViewController
@synthesize moviePlayer;
@synthesize songArray;
@synthesize songTableView;
@synthesize spinnerIndicatior;
@synthesize selectedStyleID;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
//    [self setupMenuBarButtonItems];
    
    songArray = [[NSMutableArray alloc] init];
    
    if([DMDDelegate networkavailable])
    {
        [self songsListAsynchronousCall];
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        [spinnerIndicatior stopAnimating];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //NSLog(@"viewWillDisappear called");
    if(self.moviePlayer != nil && !fullscreen) {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
}

-(void)playThumnailVideo:(UITableViewCell *)cell index:(NSIndexPath *)lindexPath
{
    DMDSongObj *tempsongObj  = [songArray objectAtIndex:lindexPath.row];
    //NSLog(@"VIDEO URL=%@",tempsongObj.videoLink);
    
    if(self.moviePlayer != nil) {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:[DMDDelegate URLEncode:tempsongObj.videoLink]]];
    self.moviePlayer.view.frame = CGRectMake(64, 2, 320, 218);
    [moviePlayer setControlStyle:MPMovieControlStyleDefault];
    self.moviePlayer.scalingMode = MPMovieScalingModeFill;
    self.moviePlayer.shouldAutoplay = YES;
    self.moviePlayer.controlStyle = MPMovieControlStyleEmbedded;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayEnterFullScreen:)
                                                 name:MPMoviePlayerWillEnterFullscreenNotification
                                               object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayExitFullScreen:)
                                                 name:MPMoviePlayerDidExitFullscreenNotification
                                               object:self.moviePlayer];
    //working
    moviePlayer.view.tag = 420;
    [cell.contentView addSubview:moviePlayer.view];
    [cell bringSubviewToFront:moviePlayer.view];
    [moviePlayer prepareToPlay];
    [moviePlayer play];
}

-(void)stopVideo
{
    if(self.moviePlayer != nil) {
        close = 1;
        [moviePlayer stop];
        //NSLog(@"close=%d",close);
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }

}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    //NSLog(@"moviePlayBackDidFinish close=%d",close);
    [moviePlayer setFullscreen:FALSE animated:TRUE];  
    MPMoviePlayerController *player = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerWillEnterFullscreenNotification object:player];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerDidExitFullscreenNotification object:player];

    if ([player respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [player.view removeFromSuperview];
    }
    moviePlayer = nil;
}

- (void) moviePlayEnterFullScreen:(NSNotification*)notification {
    //NSLog(@"moviePlayEnterFullScreen");
    fullscreen = 1;
    self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
    
}

- (void) moviePlayExitFullScreen:(NSNotification*)notification {
    //NSLog(@"moviePlayExitFullScreen");
    fullscreen = 0;
    [moviePlayer play];
    self.moviePlayer.scalingMode = MPMovieScalingModeFill;
}

-(void)songsListAsynchronousCall
{
    [spinnerIndicatior startAnimating];
	/****************Asynchronous Request**********************/
    
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:SONG_CATEGORY_DETAIL_LIST_LINK(selectedStyleID)] cachePolicy:NO timeoutInterval:15.0];
	listConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
	//NSLog(@"%@",urlRequest.URL);
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //NSLog(@"Error: %@", [error localizedDescription]);
    [spinnerIndicatior stopAnimating];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [spinnerIndicatior stopAnimating];
	if(responseAsyncData != nil)
    {
		//NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
		////NSLog(@"\n result:%@\n\n", result);
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
        //NSLog(@"json: %@", json);
        if(json != nil) {
            NSString *statusValue = [json objectForKey:@"success"];
            if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                NSArray *dataArray = [json objectForKey:@"data"];
                [songArray removeAllObjects];
                for (NSDictionary* songattributeDict in dataArray) {
                    songObj = [[DMDSongObj alloc] init];
                    songObj.songId = [songattributeDict objectForKey:@"Song_ID"];
                    songObj.songTitle = [songattributeDict objectForKey:@"Title"];
                    songObj.songMovie = [songattributeDict objectForKey:@"Movie"];
                    songObj.songChoreographer = [songattributeDict objectForKey:@"Choreographer"];
                    songObj.songPic = [songattributeDict objectForKey:@"Thumbnail"];
                    songObj.songLock = [songattributeDict objectForKey:@"Lock_Type"];
                    songObj.songCategory = [songattributeDict objectForKey:@"Category"];
                    songObj.songCategoryName = [songattributeDict objectForKey:@"Category_Name"];
                    songObj.isVideo = [songattributeDict objectForKey:@"Is_Video"];
                    songObj.videoLink = [songattributeDict objectForKey:@"Video_Link"];
    //                NSLog(@"\nsongid: %@ , title: %@  , movie: %@  , choreographer: %@ , picture: %@\n", songObj.songId, songObj.songTitle, songObj.songMovie, songObj.songChoreographer, songObj.songPic);
                    [songArray addObject:songObj];
                    songObj = nil;
                }
            }
        }
	}
    listConnection = nil;
    responseAsyncData = nil;
//    NSLog(@"array count: %d", [songArray count]);
    [songTableView reloadData];
}


#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 226;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [songArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UILabel *lblSongTitle;
    UILabel *lblSong;
    UILabel *lblMovieTitle;
    UILabel *lblMovie;
    UILabel *lblChoreoTitle;
    UILabel *lblChoreographer;
    UILabel *lblCredits;
    UIView *swipeView;
    UIButton *button;
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        //Initialize Image View with tag 1.(Thumbnail Image)
        UIImageView *thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(64, 2, 320, 218)];
        thumbImg.tag = 1;
        thumbImg.userInteractionEnabled = TRUE;
        thumbImg.multipleTouchEnabled = TRUE;
        thumbImg.contentMode = UIViewContentModeScaleToFill;
        thumbImg.layer.masksToBounds = YES;
        [cell.contentView addSubview:thumbImg];
        
        
        UIButton *videobutton = [UIButton buttonWithType:UIButtonTypeCustom];
        [videobutton setFrame:CGRectMake(64, 2, 320, 218)];
        [videobutton setBackgroundColor:[UIColor clearColor]];
        videobutton.tag = 100;
        [videobutton addTarget:self action:@selector(songIntroBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:videobutton];
        
        lblSongTitle = [[UILabel alloc] initWithFrame:CGRectMake(570.0,60.0,100.0,50.0)];
        lblSongTitle.text = @"song";
        lblSongTitle.font = KABEL_FONT(20);
        lblSongTitle.textAlignment = UITextAlignmentLeft;
        lblSongTitle.textColor = [UIColor whiteColor];
        lblSongTitle.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblSongTitle];
        
        //Initialize Label with tag 2.(Title Label)
        lblSong = [[UILabel alloc] initWithFrame:CGRectMake(570.0,65.0,300.0,100.0)];
        lblSong.tag = 2;
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblTitle.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblSong.font = KABEL_FONT(40);
        lblSong.textAlignment = UITextAlignmentLeft;
        lblSong.textColor = [UIColor whiteColor];
        lblSong.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblSong];
        
        
        // Create the swipe view
        swipeView = [[UIView alloc] initWithFrame:CGRectMake(730+250, 1, 0, 219)];
        swipeView.tag = 50;
        swipeView.clipsToBounds = TRUE;
        //swipeView.alpha = 0.3;
        swipeView.userInteractionEnabled = TRUE;
        swipeView.backgroundColor = [UIColor clearColor];
        //[swipeView setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"song_credits_bg.png"]]];
        //[swipeView setBackgroundColor:[UIColor colorWithRed:0.30 green:0.27 blue:0.23 alpha:0.7]];
        
        UIButton *bgbutton = [UIButton buttonWithType:UIButtonTypeCustom];
        [bgbutton setFrame:CGRectMake(0, 0, 250, 219)];
        [bgbutton setAlpha:0.8];
        [bgbutton setTitle:@"" forState:UIControlStateNormal];
        [bgbutton setTitle:@"" forState:UIControlStateHighlighted];
        [bgbutton setBackgroundImage:[UIImage imageNamed:@"song_credits_bg.png"] forState:UIControlStateNormal];
        [bgbutton setBackgroundImage:[UIImage imageNamed:@"song_credits_bg.png"] forState:UIControlStateHighlighted];
        [bgbutton addTarget:self action:@selector(songIntroBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        [swipeView addSubview:bgbutton];
        
        lblMovieTitle = [[UILabel alloc] initWithFrame:CGRectMake(20.0,20.0,100.0,50.0)];
        lblMovieTitle.text = @"Movie";
        lblMovieTitle.tag = 222;
        lblMovieTitle.font = KABEL_FONT(18);
        lblMovieTitle.textAlignment = UITextAlignmentLeft;
        lblMovieTitle.textColor = GRAY_TEXT_COLOR;
        lblMovieTitle.backgroundColor =  [UIColor clearColor];
        [swipeView addSubview:lblMovieTitle];
        
        //Initialize Label with tag 2.(Title Label)
        lblMovie = [[UILabel alloc] initWithFrame:CGRectMake(20.0,40.0,230.0,50.0)];
        lblMovie.tag = 3;
        lblMovie.minimumFontSize = 20;
        lblMovie.adjustsFontSizeToFitWidth = TRUE;
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblMovie.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblMovie.font = KABEL_FONT(24);
        lblMovie.textAlignment = UITextAlignmentLeft;
        lblMovie.textColor = GRAY_TEXT_COLOR;
        lblMovie.backgroundColor =  [UIColor clearColor];
        //[cell.contentView addSubview:lblMovie];
        [swipeView addSubview:lblMovie];
        
        
        lblChoreoTitle = [[UILabel alloc] initWithFrame:CGRectMake(20.0,75.0,150.0,50.0)];
        lblChoreoTitle.text = @"Choreographer";
        lblChoreoTitle.tag = 333;
        lblChoreoTitle.font = KABEL_FONT(18);
        lblChoreoTitle.textAlignment = UITextAlignmentLeft;
        lblChoreoTitle.textColor = GRAY_TEXT_COLOR;
        lblChoreoTitle.backgroundColor =  [UIColor clearColor];
        [swipeView addSubview:lblChoreoTitle];
        
        //Initialize Label with tag 2.(Title Label)
        lblChoreographer = [[UILabel alloc] initWithFrame:CGRectMake(20.0,95.0,250.0,50.0)];
        lblChoreographer.tag = 4;
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblChoreographer.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblChoreographer.font = KABEL_FONT(24);
        lblChoreographer.textAlignment = UITextAlignmentLeft;
        lblChoreographer.textColor = GRAY_TEXT_COLOR;
        lblChoreographer.backgroundColor =  [UIColor clearColor];
        //[cell.contentView addSubview:lblChoreographer];
        [swipeView addSubview:lblChoreographer];
        
        [cell.contentView addSubview:swipeView];
        
        
        UIImage *image = [UIImage imageNamed:@"song_credits_off.png"];
        UIImage *image1 = [UIImage imageNamed:@"song_credits_on.png"];
        //NSLog(@"%f %f",cell.frame.size.width , image.size.width);
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        CGRect frame = CGRectMake(980.0, 1.0, image.size.width, image.size.height-4);
        button.frame = frame;	// match the button's size with the image size
		button.tag = 5;
        [button setAlpha:0.8];
        [button setBackgroundImage:image forState:UIControlStateNormal];
        [button setBackgroundImage:image1 forState:UIControlStateHighlighted];
        [button addTarget:self action:@selector(songcCreditsBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        cell.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:button];
        
        
        //Initialize Label with tag 2.(Title Label)
        lblCredits = [[UILabel alloc] initWithFrame:CGRectMake(890.0,80.0,219.0,50.0)];
        lblCredits.tag = 6;
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblChoreographer.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblCredits.text = @"Song Credits";
        lblCredits.font = KABEL_FONT(20);
        lblCredits.textAlignment = UITextAlignmentCenter;
        lblCredits.textColor = GRAY_TEXT_COLOR;
        lblCredits.backgroundColor =  [UIColor clearColor];
        lblCredits.transform = CGAffineTransformMakeRotation(M_PI/-2);
        [cell.contentView addSubview:lblCredits];
        
        UIImageView *videoImg = [[UIImageView alloc] init];
        videoImg.image = [UIImage imageNamed:@"playbutton.png"];
        videoImg.contentMode = UIViewContentModeScaleAspectFit;
        videoImg.tag = 7;
        [videoImg setHidden:TRUE];
        //NSLog(@"%f %f",bgbutton.center.x+50,bgbutton.center.y-50);
        videoImg.frame = CGRectMake(175.0,59.5,100.0,100.0);
        [cell.contentView addSubview:videoImg];
        
    }
    DMDSongObj *tempsongObj  = [songArray objectAtIndex:indexPath.row];
    lblSong = (UILabel *)[cell viewWithTag:2];
    lblMovie = (UILabel *)[cell viewWithTag:3];
    lblChoreographer = (UILabel *)[cell viewWithTag:4];
    lblSong.text = [NSString stringWithFormat:@"%@",tempsongObj.songTitle];
    
    
    lblMovie.text = [NSString stringWithFormat:@"%@",tempsongObj.songMovie];
    lblChoreographer.text = [NSString stringWithFormat:@"%@",tempsongObj.songChoreographer];
    
    
    UIImageView *thumbImgview = (UIImageView *)[cell viewWithTag:1];
    [thumbImgview setImageWithURL:[NSURL URLWithString:[DMDDelegate URLEncode:tempsongObj.songPic]]
                 placeholderImage:nil
                          success:^(UIImage *image) {
                              //DebugLog(@"success");
                          }
                          failure:^(NSError *error) {
                              //DebugLog(@"write error %@", error);
                          }];
     UIImageView *playImgview = (UIImageView *)[cell viewWithTag:7];
    if([tempsongObj.isVideo isEqualToString:@"1"])
    {
        [playImgview setHidden:FALSE];
    } else {
        [playImgview setHidden:TRUE];
    }
    
    UIImageView *bg = [[UIImageView alloc] initWithFrame:cell.frame];
    UIImageView *bgon = [[UIImageView alloc] initWithFrame:cell.frame];
    bg.image = [UIImage imageNamed:@"tab_off.png"];
    bgon.image = [UIImage imageNamed:@"tab_on.png"];
    cell.backgroundView = bg;
    cell.selectedBackgroundView = bgon;

    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell  = (UITableViewCell *)[self.songTableView cellForRowAtIndexPath:indexPath];
//    UIView *moviewplayerView = (UIView *)[cell viewWithTag:420];
////    NSLog(@"movieplayerview %@",moviewplayerView);
//    CGRect rectInTableView = [tableView rectForRowAtIndexPath:indexPath];
//    CGRect rectInSuperview = [tableView convertRect:rectInTableView toView:[tableView superview]];
//    NSLog(@"%f,%f,%f,%f",rectInSuperview.origin.x,rectInSuperview.origin.y,rectInSuperview.size.width,rectInSuperview.size.height);
//    return;
    
    UIView *swipeView = (UIView *)[cell viewWithTag:50];
    UIButton *button = (UIButton *)[cell viewWithTag:5];
    UILabel *label = (UILabel *)[cell viewWithTag:6];
    UIImage *image = [UIImage imageNamed:@"song_credits_off.png"];
    [swipeView setFrame:CGRectMake(730+250, 1, 0, 219)];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setBackgroundImage:image forState:UIControlStateHighlighted];
    [label setHidden:FALSE];
    DMDSongObj *tempsongObj  = [songArray objectAtIndex:indexPath.row];
    DMDSongDetailViewController *typedetailController = [[DMDSongDetailViewController alloc] initWithNibName:@"DMDSongDetailViewController" bundle:nil] ;
    typedetailController.title = tempsongObj.songTitle;
    typedetailController.songID = tempsongObj.songId;
    [self.navigationController pushViewController:typedetailController animated:YES];
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:NO];
    [self stopVideo];
}

- (void)songIntroBtnTapped:(id)sender event:(id)event
{
    NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.songTableView];
	NSIndexPath *indexPath = [self.songTableView indexPathForRowAtPoint: currentTouchPosition];
	if (indexPath != nil)
	{
        UITableViewCell *cell  = (UITableViewCell *)[self.songTableView cellForRowAtIndexPath:indexPath];
        //UIButton *button = (UIButton *)[cell viewWithTag:100];
        //NSLog(@"Button Tap = %d",indexPath.row);
        DMDSongObj *tempsongObj  = [songArray objectAtIndex:indexPath.row];
        //NSLog(@"----IS VIDEO--->%@",tempsongObj.isVideo);
        if([tempsongObj.isVideo isEqualToString:@"1"])
        {
            [self playThumnailVideo:cell index:indexPath];
        }
    }
}


- (void)songcCreditsBtnTapped:(id)sender event:(id)event
{
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.songTableView];
	NSIndexPath *indexPath = [self.songTableView indexPathForRowAtPoint: currentTouchPosition];
	if (indexPath != nil)
	{
        UITableViewCell *cell  = (UITableViewCell *)[self.songTableView cellForRowAtIndexPath:indexPath];
        UIView *swipeView = (UIView *)[cell viewWithTag:50];
        UIButton *button = (UIButton *)[cell viewWithTag:5];
        UILabel *label = (UILabel *)[cell viewWithTag:6];
        UIImage *image = [UIImage imageNamed:@"song_credits_off.png"];
        UIImage *image1 = [UIImage imageNamed:@"song_credits_on.png"];
        if(swipeView.frame.origin.x > 750) {
            [button setBackgroundImage:image1 forState:UIControlStateNormal];
            [button setBackgroundImage:image1 forState:UIControlStateHighlighted];
            [label setHidden:TRUE];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
            // Swipe top view left
            [UIView animateWithDuration:1.0 animations:^{
                [swipeView setFrame:CGRectMake(730, 1, 250, 219)]; } completion:^(BOOL finished) {
            }];
        } else {
            [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
            // Swipe top view right
            [UIView animateWithDuration:1.0 animations:^{
                [swipeView setFrame:CGRectMake(730+250, 1, 0, 219)]; } completion:^(BOOL finished) {
                    [UIView  transitionWithView:button duration:0.5
                                        options:UIViewAnimationOptionTransitionCrossDissolve |UIViewAnimationOptionAllowUserInteraction
                                     animations:^(void) {
                                         [button setBackgroundImage:image forState:UIControlStateNormal];
                                         [button setBackgroundImage:image forState:UIControlStateHighlighted];
                                         [UIView  transitionWithView:label duration:1.0
                                                             options:UIViewAnimationOptionTransitionCrossDissolve |UIViewAnimationOptionAllowUserInteraction
                                                          animations:^(void) {
                                                              [label setHidden:FALSE];
                                                          }
                                                          completion:^(BOOL finished) {
                                                          }];
                                     }
                                     completion:^(BOOL finished) {
                                     }];
                }];
        }
    }
}

- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{

    return UIInterfaceOrientationMaskLandscape;

}

- (void)viewDidUnload {
    [self setSongTableView:nil];
    [self setSpinnerIndicatior:nil];
    [super viewDidUnload];
}

@end
