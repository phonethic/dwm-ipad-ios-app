//
//  DMDFBViewController.h
//  DancewithMD
//
//  Created by Rishi on 09/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DMDFBViewController : UIViewController <UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *fbWebView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *barbackBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *barfwdBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *barrefreshBtn;
@property (strong, nonatomic) IBOutlet UIToolbar *webviewtoolBar;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinnerIndicatior;

- (IBAction)webviewbackBtnPressed:(id)sender;
- (IBAction)webviewfwdBtnPressed:(id)sender;
- (IBAction)webviewrefreshBtnPressed:(id)sender;

@end
