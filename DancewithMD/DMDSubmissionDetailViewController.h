//
//  DMDSubmissionDetailViewController.h
//  DancewithMD
//
//  Created by Kirti Nikam on 09/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "iCarousel.h"
#import "FXImageView.h"

@class DMDSubmittedVideosObj;
@interface DMDSubmissionDetailViewController : UIViewController <iCarouselDataSource, iCarouselDelegate>
{
    int status;
    int small;
    int fullscreen;
    NSMutableData *responseAsyncData;
    NSURLConnection *connection1;
    NSURLConnection *connection2;
    DMDSubmittedVideosObj *videosObj;
}
@property (strong, nonatomic) IBOutlet iCarousel *carousel;

@property (nonatomic, copy) NSString *selectedvideoUserId;
@property (nonatomic, copy) NSString *selectedvideoId;
@property (nonatomic, copy) NSString *selectedSongId;

@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;
@property (strong, nonatomic) NSMutableArray *videoArray;
@property (strong, nonatomic) IBOutlet UIButton *voteBtn;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (strong, nonatomic) IBOutlet UIImageView *lightspotImageView;
@property (strong, nonatomic) IBOutlet UIView *videoView;
@property (strong, nonatomic) IBOutlet UIImageView *screenshotView;
@property (strong, nonatomic) IBOutlet UIImageView *videoboxImageView;
@property (strong, nonatomic) IBOutlet UIProgressView *progressBar;
@property (strong, nonatomic) IBOutlet UILabel *titlelbl;
@property (strong, nonatomic) IBOutlet UILabel *namelbl;
@property (strong, nonatomic) IBOutlet UIImageView *reflectionView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinnerIndicatior;



- (IBAction)voteBtnPressed:(id)sender;
- (IBAction)closevideoBtnPressed:(id)sender;

@end
