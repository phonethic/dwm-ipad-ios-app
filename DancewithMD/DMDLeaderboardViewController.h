//
//  DMDLeaderboardViewController.h
//  DancewithMD
//
//  Created by Rishi on 22/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMDLeaderboardObj;
@interface DMDLeaderboardViewController : UIViewController {
    int status;
    NSMutableData *responseAsyncData;
    NSURLConnection *connection1;
    NSURLConnection *connection2;
    NSURLConnection *connection3;
    DMDLeaderboardObj *boardObj;
    UIView *headerView;
}

@property (strong, nonatomic) IBOutlet UILabel *namelbl;
@property (strong, nonatomic) IBOutlet UILabel *pointslbl;
@property (strong, nonatomic) IBOutlet UILabel *dancerslbl;

@property (strong, nonatomic) IBOutlet UILabel *myranklbl;
@property (strong, nonatomic) IBOutlet UITableView *leaderboardTable;
@property (strong, nonatomic) NSMutableArray *leaderboardArray;
@property (strong, nonatomic) IBOutlet UIImageView *userPicImgView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinnerIndicatior;

@end
