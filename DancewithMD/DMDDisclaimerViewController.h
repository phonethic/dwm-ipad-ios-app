//
//  DMDDisclaimerViewController.h
//  DancewithMD
//
//  Created by Rishi on 16/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DMDDisclaimerViewController : UIViewController <UIWebViewDelegate>
{

}

@property (strong, nonatomic) IBOutlet UIWebView *disclaimerWebview;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *disclaimerindicator;

@end
