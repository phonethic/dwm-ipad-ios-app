//
//  DMDMyVideosViewController.h
//  DancewithMD
//
//  Created by Rishi on 17/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DMDMyVideosViewController : UIViewController {
    
}

@property (strong, nonatomic) IBOutlet UITableView *myvideoTableView;
@property (strong, nonatomic) NSMutableArray *folderArray;

@end
