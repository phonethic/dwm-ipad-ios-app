//
//  DMDMyProfileViewController.m
//  DancewithMD
//
//  Created by Rishi on 20/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDMyProfileViewController.h"
#import "MFSideMenu.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DMDMyBadge.h"
#import "DMDMyVideosViewController.h"
#import "constants.h"
#import "DMDAppDelegate.h"
#import "DMDLeaderboardViewController.h"

//#define USER_BADGES(USERID) [NSString stringWithFormat:@"%@/api/v1/retrieve/mybadge/%@",LIVE_DWM_SERVER,USERID]
//#define USER_PIC(USERID) [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture",USERID]
//#define USER_RANK_LINK(USERID) [NSString stringWithFormat:@"%@/api/v1/retrieve/rank/%@",LIVE_DWM_SERVER,USERID]
//#define COUNTER_LINK [NSString stringWithFormat:@"%@/api/v1/retrieve/counter",LIVE_DWM_SERVER]

#define USER_BADGES(USERID) [NSString stringWithFormat:@"%@/api/retrieve/mybadge/%@",LIVE_DWM_SERVER,USERID]
#define USER_PIC(USERID) [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture",USERID]
#define USER_RANK_LINK(USERID) [NSString stringWithFormat:@"%@/api/retrieve/rank/%@",LIVE_DWM_SERVER,USERID]
#define COUNTER_LINK [NSString stringWithFormat:@"%@/api/retrieve/counter",LIVE_DWM_SERVER]

@interface DMDMyProfileViewController ()

@end

@implementation DMDMyProfileViewController
@synthesize mybadgeArray;
@synthesize mybadgeTableView;
@synthesize badgescountlbl;
@synthesize specialbadgescountlbl;
@synthesize userPicImgView;
@synthesize userNamelbl;
@synthesize welcomelbl;
@synthesize downloadedVideoBtn;
@synthesize leaderboardBtn;
@synthesize spinnerIndicatior;
@synthesize ranklbl;
@synthesize pointslbl;
@synthesize dancerslbl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Flurry logEvent:@"Tab_MyProfile"];

    badgescountlbl.text = @"0 / 8";
    specialbadgescountlbl.text = @"0 / 4";
    badgescountlbl.font = KABEL_FONT(30);
    specialbadgescountlbl.font = KABEL_FONT(30);
    userNamelbl.font = KABEL_FONT(30);
    welcomelbl.font = KABEL_FONT(15);
    downloadedVideoBtn.titleLabel.font = KABEL_FONT(30);
    leaderboardBtn.titleLabel.font = KABEL_FONT(30);
    badgescountlbl.font = KABEL_FONT(30);
    specialbadgescountlbl.font = KABEL_FONT(30);
    ranklbl.font = KABEL_FONT(25);
    pointslbl.font = KABEL_FONT(25);
    dancerslbl.font = KABEL_FONT(30);

    mybadgeArray = [[NSMutableArray alloc] init];
    if([DMDDelegate networkavailable])
    {
        [self myBadgesAsynchronousCall];
        [self myRankAsynchronousCall];
        [self dancersAsynchronousCall];
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        [spinnerIndicatior stopAnimating];
        
    }
    [self setupMenuBarButtonItems];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:@"userId"];
    NSString *userName = [defaults objectForKey:@"userName"];
    userNamelbl.text = userName;
    [userPicImgView setImageWithURL:[NSURL URLWithString:USER_PIC(userID)]
                   placeholderImage:[UIImage imageNamed:@"unknown.jpg"]
                            success:^(UIImage *image) {
                                //DebugLog(@"success");
                            }
                            failure:^(NSError *error) {
                                //DebugLog(@"write error %@", error);
                            }];
}

- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)myBadgesAsynchronousCall
{
    [spinnerIndicatior startAnimating];
	/****************Asynchronous Request**********************/
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:@"userId"];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:USER_BADGES(userID)] cachePolicy:NO timeoutInterval:15.0];
    
    //Get list of all songs
    connection1 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)myRankAsynchronousCall
{
	/****************Asynchronous Request**********************/
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:@"userId"];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:USER_RANK_LINK(userID)] cachePolicy:NO timeoutInterval:5.0];
    
    //Get list of all songs
    connection2 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)dancersAsynchronousCall
{
	/****************Asynchronous Request**********************/
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:COUNTER_LINK] cachePolicy:NO timeoutInterval:15.0];
    
    //Get list of all songs
    connection3 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}


#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //NSLog(@"Error: %@", [error localizedDescription]);
    [spinnerIndicatior stopAnimating];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [spinnerIndicatior stopAnimating];
    NSError* error;
    if(connection==connection1)
    {
        //NSLog(@"con 1 length = %d",[responseAsyncData length]);
        if (responseAsyncData != nil && [responseAsyncData length] > 0) {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            NSString *statusValue = [json objectForKey:@"success"];
            if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                [mybadgeArray removeAllObjects];
                NSArray *dataArray = [json objectForKey:@"data"];
                for (NSDictionary* mybadgeattributeDict in dataArray)
                {
                      //NSLog(@"count : %@", [mybadgeattributeDict objectForKey:@"count"]);
                    if([mybadgeattributeDict valueForKey:@"badge_name"] != nil)
                    {
                        mybadgeObj = [[DMDMyBadge alloc] init];
                        mybadgeObj.mybadge_name = [mybadgeattributeDict objectForKey:@"badge_name"];
                        mybadgeObj.mybadge_count = [mybadgeattributeDict objectForKey:@"count"];
                        mybadgeObj.mybadge_songs = [mybadgeattributeDict objectForKey:@"songs"];
                        mybadgeObj.mybadge_icon =  [mybadgeattributeDict objectForKey:@"image"];
                      ////NSLog(@"\n  badge_name: %@  , badge_count: %d  , badge_songs: %@  \n", mybadgeObj.mybadge_name, mybadgeObj.mybadge_count,  mybadgeObj.mybadge_songs);
                        if(![[NSString stringWithFormat:@"%@",mybadgeObj.mybadge_count] isEqualToString:@"0"])
                        {
                            [mybadgeArray addObject:mybadgeObj];
                        }
                        mybadgeObj = nil;
                    } else {
                        if([mybadgeattributeDict valueForKey:@"badge"] != nil)
                        {
                            badgescountlbl.text = [NSString stringWithFormat:@"%@ / 8", [mybadgeattributeDict objectForKey:@"badge"]];
                            specialbadgescountlbl.text = [NSString stringWithFormat:@"%@ / 4", [mybadgeattributeDict objectForKey:@"spbadge"]];
                        }
                    }
                }
            }
            ////NSLog(@"detail array count: %d", [songDetailArray count]);
            [mybadgeTableView reloadData];
        }
        responseAsyncData = nil;
        connection1 = nil;
    } else if (connection==connection2) {
        //NSLog(@"con 2 length = %d",[responseAsyncData length]);
        if (responseAsyncData != nil && [responseAsyncData length] > 0) {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            if (json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* attributeDict in dataArray)
                    {
                        ranklbl.text = [attributeDict objectForKey:@"rank"];
                        pointslbl.text = [attributeDict objectForKey:@"points"];
                    }
                }
            }
        }
        connection2 = nil;
        responseAsyncData = nil;
    } else if(connection==connection3) {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            if (json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSDictionary *datadict = [json objectForKey:@"data"];
                    dancerslbl.text = [datadict objectForKey:@"count"];
                }
            }
        }
        responseAsyncData = nil;
    }
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [mybadgeArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UILabel *lblName;
    UILabel *lblCount;
    UILabel *lblSongs;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.userInteractionEnabled = NO;
        //        cell.textLabel.textColor = [UIColor whiteColor];
        //        cell.textLabel.font = [UIFont systemFontOfSize:24.0];
        //        cell.detailTextLabel.textColor = [UIColor lightGrayColor];
        //        cell.detailTextLabel.font = [UIFont systemFontOfSize:20.0];
        
        
        //Initialize Image View with tag 1.(Thumbnail Image)
        UIImageView *thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(18, 10, 100, 100)];
        thumbImg.tag = 4;
        thumbImg.contentMode = UIViewContentModeScaleToFill;
        [cell.contentView addSubview:thumbImg];
        
        //Initialize Label with tag 2.(Title Label)
        lblName = [[UILabel alloc] initWithFrame:CGRectMake(130.0,35.0,300.0,40.0)];
        lblName.tag = 1;
        lblName.text = @"Badge_Name";
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblTitle.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblName.font = KABEL_FONT(22);
        lblName.textAlignment = UITextAlignmentLeft;
        lblName.textColor = [UIColor whiteColor];
        lblName.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblName];
        
        
        UIImageView *countImg = [[UIImageView alloc] initWithFrame:CGRectMake(360.0,10.0,94.0,94.0)];
        countImg.image = [UIImage imageNamed:@"badges_counter_field.png"];
        countImg.contentMode = UIViewContentModeScaleAspectFit;
        [cell.contentView addSubview:countImg];
        
        //Initialize Label with tag 2.(Title Label)
        lblCount = [[UILabel alloc] initWithFrame:CGRectMake(370.0,25.0,70.0,60.0)];
        lblCount.tag = 2;
        lblCount.text = @"Badge_Count";
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblMovie.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblCount.font = KABEL_FONT(30);
        lblCount.textAlignment = UITextAlignmentCenter;
        lblCount.textColor = [UIColor whiteColor];
        lblCount.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblCount];
        
        
        //Initialize Label with tag 2.(Title Label)
//        lblSongs = [[UILabel alloc] initWithFrame:CGRectMake(120.0,70.0,400.0,40.0)];
//        lblSongs.tag = 3;
//        lblSongs.text = @"Badge_Songs";
//        //lblTitle.shadowColor   = [UIColor blackColor];
//        //lblMovie.shadowOffset  = CGSizeMake(0.0, 2.0);
//        lblSongs.font = [UIFont fontWithName:@"Cochin-Bold" size:20];
//        lblSongs.textAlignment = UITextAlignmentLeft;
//        lblSongs.textColor = [UIColor whiteColor];
//        lblSongs.backgroundColor =  [UIColor clearColor];
//        [cell.contentView addSubview:lblSongs];
        
        UIImageView *seperatorImg = [[UIImageView alloc] initWithFrame:CGRectMake(-4, 110, 480, 17)];
        seperatorImg.image = [UIImage imageNamed:@"badges_table_row_line.png"];
        seperatorImg.tag = 5;
        seperatorImg.contentMode = UIViewContentModeScaleAspectFit;
        [cell.contentView addSubview:seperatorImg];
    }
    
    DMDMyBadge *tempMybadgeObj  = [mybadgeArray objectAtIndex:indexPath.row];
    lblName = (UILabel *)[cell viewWithTag:1];
    lblCount = (UILabel *)[cell viewWithTag:2];
    lblSongs = (UILabel *)[cell viewWithTag:3];
    lblName.text = tempMybadgeObj.mybadge_name;
    lblCount.text = [NSString stringWithFormat:@"%@",tempMybadgeObj.mybadge_count];
    lblSongs.text = tempMybadgeObj.mybadge_songs;
    
    UIImageView *thumbImgview = (UIImageView *)[cell viewWithTag:4];
    [thumbImgview setImageWithURL:[NSURL URLWithString:tempMybadgeObj.mybadge_icon]
                 placeholderImage:[UIImage imageNamed:@"pop-up.png"]
                          success:^(UIImage *image) {
                              //DebugLog(@"success");
                          }
                          failure:^(NSError *error) {
                              //DebugLog(@"write error %@", error);
                          }];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setMybadgeTableView:nil];
    [self setBadgescountlbl:nil];
    [self setSpecialbadgescountlbl:nil];
    [self setUserPicImgView:nil];
    [self setUserNamelbl:nil];
    [self setWelcomelbl:nil];
    [self setDownloadedVideoBtn:nil];
    [self setSpinnerIndicatior:nil];
    [self setRanklbl:nil];
    [self setPointslbl:nil];
    [self setLeaderboardBtn:nil];
    [self setDancerslbl:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}
- (IBAction)downloadedVideoBtnPressed:(id)sender {
    DMDMyVideosViewController *songdetailController = [[DMDMyVideosViewController alloc] initWithNibName:@"DMDMyVideosViewController" bundle:nil];
    songdetailController.title = @"My Videos";
    [self.navigationController pushViewController:songdetailController animated:YES];
}

- (IBAction)leaderboardBtnPressed:(id)sender {
    DMDLeaderboardViewController *profileController = [[DMDLeaderboardViewController alloc] initWithNibName:@"DMDLeaderboardViewController" bundle:nil];
    profileController.title = @"Leaderboard";
    [self.navigationController pushViewController:profileController animated:YES];

}
@end
