//
//  DMDTweetsViewController.h
//  DancewithMD
//
//  Created by Rishi on 25/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMDTweetObj;
@interface DMDTweetsViewController : UIViewController {
    int status;
    NSMutableData *responseAsyncData;
    NSURLConnection *connection1;
    NSURLConnection *connection2;
    DMDTweetObj *tweetObj;
}
@property (nonatomic, copy) NSString *dataFilePath;
@property (nonatomic, copy) NSString *bearerToken;
@property (strong, nonatomic) IBOutlet UITableView *tweetsTableView;
@property (strong, nonatomic) NSMutableArray *tweetsArray;

@property (strong, nonatomic) IBOutlet UIView *detailTweetView;
@property (strong, nonatomic) IBOutlet UITextView *tweetcontentTextView;
@property (strong, nonatomic) IBOutlet UIImageView *tweetuserImageView;
@property (strong, nonatomic) IBOutlet UILabel *tweetusernameLabel;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinnerIndicatior;

@property (strong, nonatomic) IBOutlet UIButton *closeBtn;

- (IBAction)closeBtnClicked:(id)sender;

@end
