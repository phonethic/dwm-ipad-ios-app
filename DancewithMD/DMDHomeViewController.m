//
//  DMDHomeViewController.m
//  DancewithMD
//
//  Created by Rishi on 01/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDHomeViewController.h"
#import "DMDLoginViewController.h"
#import "SCViewController.h"
#import "MFSideMenu.h"
#import "constants.h"
#import "DMDAppDelegate.h"
#import "DMDMyVideosViewController.h"

#define VIDEO_ENGLISH_LINK @"https://s3.amazonaws.com/dance-with-md-new/opening/mp4/360/Site_Intro_English.mp4"
#define VIDEO_HINDI_LINK @"https://s3.amazonaws.com/dance-with-md-new/opening/mp4/360/Site_Intro_Hindi.mp4"

@implementation UITextView (DisableCopyPaste)

- (BOOL)canBecomeFirstResponder
{
    return NO;
}

@end

@interface DMDHomeViewController ()
{
    DMDLoginViewController *controller;
    UIPopoverController *popoverController;
}

@end

@implementation DMDHomeViewController
@synthesize englishBtn;
@synthesize hindiBtn;
@synthesize moviePlayer;
@synthesize tickerLabel;
@synthesize googleBtn;
@synthesize helptextView;
@synthesize expandTextBtn;
@synthesize dmdTitleView;
@synthesize welcomelbl;
@synthesize videoBoximgView;
@synthesize helpMsgBackImgView;
@synthesize helpView,videoView;
@synthesize infoscreen;
@synthesize blackView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    if(![DMDDelegate networkavailable])
//    {
//        [self dmdfbLoginPressed:nil];
//        return;
//    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:LOGIN_USERID];
    NSString *userPass = [defaults objectForKey:LOGIN_PASSWORD];
//    NSDate *oldDate = [defaults objectForKey:LOGIN_EXPIREDDATE];
    
    
    if (FBSession.activeSession.isOpen) //User logged-in via Facebook
    {
        self.navigationController.navigationBarHidden = NO;
        [blackView setHidden:TRUE];
        if(self.moviePlayer==nil) {
            [self performSelector:@selector(showVideo:) withObject:@"welcome" afterDelay:0.0];
        }
    }
    else if ((userID != nil && userPass != nil && [userID length] > 0 && [userPass length] > 0)) //User logged-in via own server
    {
        self.navigationController.navigationBarHidden = NO;
        [blackView setHidden:TRUE];
        NSDate *oldDate = [defaults objectForKey:LOGIN_EXPIREDDATE];
        //NSLog(@"welcome called");
        NSDate *currentDate = [NSDate date];
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                            fromDate:oldDate
                                                              toDate:currentDate
                                                             options:0];
        //NSLog(@"days = %d",[components day]);
        //NSLog(@"-----oldDate %@ --currentDate %@--",oldDate,currentDate);
        
        //Using below check we are authenticating or verifying user on our own server daily.
        if([components day] != 0) // Compare expiration dates. if greater than one then users session timeout occurs.
        { 
            //NSLog(@"Login called");
            [self dmdfbLoginPressed:nil];
        } else {
            if(self.moviePlayer==nil) {
                [self performSelector:@selector(showVideo:) withObject:@"welcome" afterDelay:0.0];
            }
        }
    }
    else //User not logged-in via facebook or own server
    {
        //NSLog(@"activesession called");
        [self dmdfbLoginPressed:nil];
    }

    
//    if((userID != nil && userPass != nil && [userID length] > 0 && [userPass length] > 0) || FBSession.activeSession.isOpen)
//    {
//        NSLog(@"welcome called");
//        NSDate *currentDate = [NSDate date];
//        
//        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//        NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
//                                                            fromDate:oldDate
//                                                              toDate:currentDate
//                                                             options:0];
//        NSLog(@"days = %d",[components day]);
//        NSLog(@"-----oldDate %@ --currentDate %@--",oldDate,currentDate);
//        if([components day] != 0 && (userID != nil && userPass != nil && [userID length] > 0 && [userPass length] > 0))
//        {
//            NSLog(@"Login called");
//            [self dmdfbLoginPressed:nil];
//        } else {
//            [self performSelector:@selector(showVideo:) withObject:@"welcome" afterDelay:2.0];
//        }
//    } else {
//        NSLog(@"activesession called");
//        [self dmdfbLoginPressed:nil];
//    }
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
     //NSLog(@"viewWillDisappear called");
    if(self.moviePlayer != nil && !fullscreen) {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
//    [self.view.layer removeAllAnimations];
//    for (CALayer* layer in [self.view.layer sublayers]) {
//        [layer removeAllAnimations];
//    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    self.navigationController.navigationBarHidden = YES;
    fullscreen = 0; //Not fullscreen
    [Flurry logEvent:@"Tab_Home"];
    self.title = @"Home";
    [blackView setHidden:FALSE];
    [googleBtn setHidden:FALSE];
    [helpView setHidden:TRUE];
    [videoView setHidden:TRUE];
    [welcomelbl setHidden:TRUE];
    [expandTextBtn setHidden:TRUE];
    [englishBtn setSelected:TRUE];
    self.infoscreen.hidden = TRUE;
    welcomelbl.font = KABEL_FONT(30);
    helptextView.font = KABEL_FONT(20);
    controller = [[DMDLoginViewController alloc] initWithNibName:@"DMDLoginViewController" bundle:nil];
    popoverController = [[UIPopoverController alloc] initWithContentViewController:controller];
    //self.contentSizeForViewInPopover = CGSizeMake(150.0, 140.0);
    [self setupMenuBarButtonItems];
    helptextView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    self.navigationController.navigationBar.tintColor = BLACK_COLOR;
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                UITextAttributeTextColor: [UIColor whiteColor],
                          UITextAttributeTextShadowColor: [UIColor blackColor],
                         UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)],
                                     UITextAttributeFont: KABEL_FONT(25)
     }];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL isShown = [[defaults objectForKey:LOGIN_HELPINFOSCREEN] boolValue];
    if (isShown) {
        self.infoscreen.hidden = TRUE;
    }else{
        self.infoscreen.hidden = FALSE;
        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideHelpInfoScreen)];
        gestureRecognizer.cancelsTouchesInView=NO;
        [self.infoscreen addGestureRecognizer:gestureRecognizer];
    }
}

- (void) hideHelpInfoScreen {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithBool:1] forKey:LOGIN_HELPINFOSCREEN];
    [defaults synchronize];
    [UIView animateWithDuration:1.0 delay:0.0
                        options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                     animations:^(void)
                     {
                         self.infoscreen.alpha = 0.0f;
                     }
                     completion:^(BOOL finished)
                     {
                         self.infoscreen.hidden = YES;
                         if (self.moviePlayer != nil && (self.moviePlayer.playbackState == MPMoviePlaybackStatePaused || self.moviePlayer.playbackState == MPMoviePlaybackStateStopped)  && self.isViewLoaded && self.view.window) {
                             //NSLog(@"tap detected, playing video");
                             [self.moviePlayer play];
                         }
                     }];
}

-(void)showVideo:(NSString *)type
{
    
    if ([type isEqualToString:@"welcome"]) {
            [videoView setAlpha:0.0];
            [videoView setHidden:FALSE];
            [UIView  transitionWithView:self.videoBoximgView duration:0.5  options:UIViewAnimationOptionTransitionNone
                              animations:^(void) {
                                  [videoView setAlpha:1.0];
                                  [UIView  transitionWithView:welcomelbl duration:1.0  options:UIViewAnimationOptionTransitionCrossDissolve
                                                   animations:^(void) {
                                                       [welcomelbl setHidden:FALSE];
                                                   }
                                                   completion:^(BOOL finished) {
                                                      
                                                   }];
                                  [UIView  transitionWithView:expandTextBtn duration:1.0  options:UIViewAnimationOptionTransitionCrossDissolve
                                                   animations:^(void) {
                                                       [expandTextBtn setHidden:FALSE];
                                                   }
                                                   completion:^(BOOL finished) {
                                                       
                                                   }];
                              }
                              completion:^(BOOL finished) {
//                                                  if (self.moviePlayer.playbackState == MPMoviePlaybackStatePaused || self.moviePlayer.playbackState == MPMoviePlaybackStateStopped) {
//                                                      [self.moviePlayer play];
//                                                  }
                                  if (self.isViewLoaded && self.view.window) {
                                      //NSLog(@"firsttime video");
                                      NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                      NSString *userID = [defaults objectForKey:LOGIN_USERID];
                                      NSString *userPass = [defaults objectForKey:LOGIN_PASSWORD];
                                      if((userID != nil && userPass != nil && [userID length] > 0 && [userPass length] > 0) || FBSession.activeSession.isOpen)
                                      {
                                          [self playVideo:VIDEO_ENGLISH_LINK];
                                      }
                                  }
                                  [dmdTitleView setHidden:TRUE];
                                  [englishBtn setSelected:TRUE];
                                  [helpView setHidden:FALSE];
                                  [helpView setAlpha:0.0];
                                   //expandTextBtn.enabled = TRUE;
                                   expandTextBtn.userInteractionEnabled = TRUE;
                              }];
    }
    else
    {
        [UIView  transitionWithView:self.helpView duration:0.5  options:UIViewAnimationOptionTransitionNone
                         animations:^(void) {
                             [helpView setAlpha:0.0];
                         }
                         completion:^(BOOL finished) {
                             [UIView  transitionWithView:self.videoView duration:0.5  options:UIViewAnimationOptionTransitionNone
                                              animations:^(void) {
                                                  [videoView setAlpha:1.0];
                                              }
                                              completion:^(BOOL finished) {
                                                  if (self.moviePlayer != nil && (self.moviePlayer.playbackState == MPMoviePlaybackStatePaused || self.moviePlayer.playbackState == MPMoviePlaybackStateStopped)  && self.isViewLoaded && self.view.window && self.infoscreen.hidden) {
                                                      //NSLog(@"secondtime video");
                                                      [self.moviePlayer play];
                                                  }
                                                  //expandTextBtn.enabled = TRUE;
                                                  expandTextBtn.userInteractionEnabled = TRUE;
                                              }];
                         }];
    }
}
-(void)showHelpText
{
    if (self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
        [self.moviePlayer pause];
    }
    [UIView  transitionWithView:self.videoView duration:0.5  options:UIViewAnimationOptionTransitionNone
                     animations:^(void) {
                         [videoView setAlpha:0.0];
                     }
                     completion:^(BOOL finished) {
                         [UIView  transitionWithView:self.helpView duration:0.5  options:UIViewAnimationOptionTransitionNone
                                          animations:^(void) {
                                              [helpView setAlpha:1.0];
                                          }
                                          completion:^(BOOL finished) {
                                              //expandTextBtn.enabled = TRUE;
                                              expandTextBtn.userInteractionEnabled = TRUE;
                                          }];
                     }];
}

- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
//    return [[UIBarButtonItem alloc]
//            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
//            target:self.navigationController.sideMenu
//            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
//    return [[UIBarButtonItem alloc]
//            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
//            target:self.navigationController.sideMenu
//            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)dmdfbLoginPressed:(id)sender {
    SCViewController *facebookViewComposer = [[SCViewController alloc] initWithNibName:@"SCViewController" bundle:nil];
    facebookViewComposer.title = @"FACEBOOK";
    facebookViewComposer.FBtitle = @"DMD FB Login";
    facebookViewComposer.FBtLink = @"";
    facebookViewComposer.modalPresentationStyle = UIModalPresentationCurrentContext;
    facebookViewComposer.showorhide = 1; //Hide after login
    [self.navigationController presentModalViewController:facebookViewComposer animated:YES];
    
}


-(void)playVideo:(NSString *) videoUrl {
    
    if(self.moviePlayer != nil) {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:videoUrl]];
    self.moviePlayer.view.frame = CGRectMake(35, 23, 564, 314);
    [moviePlayer setControlStyle:MPMovieControlStyleDefault];
    self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
    self.moviePlayer.controlStyle = MPMovieControlStyleDefault;
    if (self.infoscreen.hidden) {
        self.moviePlayer.shouldAutoplay = YES;
    }else{
        self.moviePlayer.shouldAutoplay = NO;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.moviePlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieEnterFullScreen:)
                                                 name:MPMoviePlayerWillEnterFullscreenNotification
                                               object:self.moviePlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieExitFullScreen:)
                                                 name:MPMoviePlayerDidExitFullscreenNotification
                                               object:self.moviePlayer];
    [self.videoView addSubview:moviePlayer.view];
    [moviePlayer prepareToPlay];
//    if (!self.infoscreen.hidden) {
//        [moviePlayer pause];
//    }
    //[moviePlayer play];
    
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    [moviePlayer setFullscreen:FALSE animated:TRUE];
    [moviePlayer play];
}
- (void) movieEnterFullScreen:(NSNotification*)notification {
    fullscreen = 1;
}
- (void) movieExitFullScreen:(NSNotification*)notification {
    fullscreen = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)expandBtnPressed:(id)sender {
    //expandTextBtn.enabled = FALSE;
    expandTextBtn.userInteractionEnabled = FALSE;
    if(expandTextBtn.isSelected==FALSE)
    {
        [self showHelpText];
        [expandTextBtn setImage:[UIImage imageNamed:@"minus_button.png"] forState:UIControlStateNormal];
        [expandTextBtn setSelected:TRUE];
    } else {
        [self showVideo:nil];
        [expandTextBtn setImage:[UIImage imageNamed:@"plus_button_off.png"] forState:UIControlStateNormal];
        [expandTextBtn setSelected:FALSE];
    }
}

- (IBAction)englishBtnPressed:(id)sender {
    [englishBtn setSelected:TRUE];
    [hindiBtn setSelected:FALSE];
    [self playVideo:VIDEO_ENGLISH_LINK];
}

- (IBAction)hindiBtnPressed:(id)sender {
    [englishBtn setSelected:FALSE];
    [hindiBtn setSelected:TRUE];
    [self playVideo:VIDEO_HINDI_LINK];
}

- (IBAction)googleLoginBtnPressed:(id)sender {
    if ([popoverController isPopoverVisible]) {
        [popoverController dismissPopoverAnimated:YES];
    } else {
        //the rectangle here is the frame of the object that presents the popover,
        //in this case, the UIButton…
        CGRect popRect = CGRectMake(self.googleBtn.frame.origin.x,
                                    self.googleBtn.frame.origin.y,
                                    self.googleBtn.frame.size.width,
                                    self.googleBtn.frame.size.height);
        [popoverController presentPopoverFromRect:popRect
                                           inView:self.view
                         permittedArrowDirections:UIPopoverArrowDirectionAny
                                         animated:YES];
    }
}

- (IBAction)offlineVideoBtnPressed:(id)sender {
    DMDMyVideosViewController *myvideosController = [[DMDMyVideosViewController alloc] initWithNibName:@"DMDMyVideosViewController" bundle:nil];
    myvideosController.title = @"My Videos";
    [self.navigationController pushViewController:myvideosController animated:YES];
}

- (void)viewDidUnload {
    [self setEnglishBtn:nil];
    [self setHindiBtn:nil];
    [self setTickerLabel:nil];
    [self setGoogleBtn:nil];
    [self setDmdTitleView:nil];
    [self setHelptextView:nil];
    [self setExpandTextBtn:nil];
    [self setHelpMsgBackImgView:nil];
    [self setWelcomelbl:nil];
    [self setHelpView:nil];
    [self setVideoView:nil];
    [self setInfoscreen:nil];
    [self setBlackView:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}

-(void)dealloc {
    //NSLog(@"dealloc called");
    self.navigationController.sideMenu.menuStateEventBlock = nil;
}
@end
