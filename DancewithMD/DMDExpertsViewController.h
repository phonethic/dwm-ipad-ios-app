//
//  DMDExpertsViewController.h
//  DancewithMD
//
//  Created by Rishi on 19/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "FXImageView.h"
#import <MediaPlayer/MediaPlayer.h>

@class DMDExpertsObj;
@interface DMDExpertsViewController : UIViewController <iCarouselDataSource, iCarouselDelegate>{
    int status;
    int small;
    int fullscreen;
    NSMutableData *responseAsyncData;
    NSURLConnection *connection1;
    DMDExpertsObj *expvideosObj;
    UIWebView *videowebView;
}

@property (strong, nonatomic) NSMutableArray *videoDetailArray;
@property (strong, nonatomic) IBOutlet iCarousel *carousel;
@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;
//Video View
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (strong, nonatomic) IBOutlet UIView *videoView;
@property (strong, nonatomic) IBOutlet UIImageView *reflectionView;
@property (strong, nonatomic) IBOutlet UILabel *namelbl;
@property (strong, nonatomic) IBOutlet UIImageView *screenshotView;
@property (strong, nonatomic) IBOutlet UILabel *titlelbl;
@property (strong, nonatomic) IBOutlet UIImageView *videoboxImageView;
@property (strong, nonatomic) IBOutlet UIImageView *lightspotImageView;
@property (strong, nonatomic) IBOutlet UIView *descriptionView;
@property (strong, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (strong, nonatomic) IBOutlet UIImageView *descriptionImageView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinnerIndicatior;


- (IBAction)closevideoBtnPressed:(id)sender;

@end
