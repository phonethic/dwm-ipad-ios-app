//
//  DMDSongDetailObj.m
//  DancewithMD
//
//  Created by Rishi on 12/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDSongDetailObj.h"

@implementation DMDSongDetailObj
@synthesize songId;
@synthesize songName;
@synthesize songClip_Part_ID;
@synthesize songSequence;
@synthesize songShare_ID;
@end
