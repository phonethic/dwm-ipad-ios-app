//
//  DMDSubmissionViewController.h
//  DancewithMD
//
//  Created by Rishi on 21/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>

@class DMDSongObj;

@interface DMDSubmissionViewController : UIViewController  <UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UIPopoverControllerDelegate,NSURLConnectionDataDelegate,NSURLConnectionDelegate>
{
    int status;
    NSMutableData *responseAsyncData;
    DMDSongObj *songObj;
    UIPopoverController *popover;
    NSURLConnection *connection3;
     NSURLConnection *connection4;
    BOOL selectdBtnStatus;
    BOOL uploadBtnStatus;
}
@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;

//@property (strong, nonatomic) UIPickerView *submissionCategorypicker;
//@property (strong, nonatomic) NSMutableArray *categoryArray;

@property (strong, nonatomic) NSMutableArray *songArray;
@property (nonatomic, copy) NSString *selectedvideoId;
@property (nonatomic, copy) NSString *selectedvideoUserId;

@property (strong, nonatomic) IBOutlet UIButton *selectedsongBtn;
@property (strong, nonatomic) IBOutlet UIButton *uploadBtn;
@property (strong, nonatomic) IBOutlet UILabel *submissionInfoLabel;
@property (strong, nonatomic) IBOutlet UILabel *uploadInfoLabel;
@property (strong, nonatomic) IBOutlet UITableView *submissionTableView;
@property (strong, nonatomic) IBOutlet UITableView *uploadTableView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *submitProgressBar;
@property (strong, nonatomic) IBOutlet UILabel *uploadinglbl;

- (IBAction)selectedsongBtnPressed:(id)sender;
- (IBAction)uploadBtnPressed:(id)sender;


@end
