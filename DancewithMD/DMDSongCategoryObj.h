//
//  DMDSongCategoryObj.h
//  DancewithMD
//
//  Created by Kirti Nikam on 12/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DMDSongCategoryObj : NSObject
@property (nonatomic, copy) NSString *styleId;
@property (nonatomic, copy) NSString *styleName;
@property (nonatomic, copy) NSString *styleThumbnail;
@property (nonatomic, copy) NSString *styleVideoLink;
@property (nonatomic, copy) NSString *styleImageLink;
@property (nonatomic, copy) NSString *styleCategory;
@end
