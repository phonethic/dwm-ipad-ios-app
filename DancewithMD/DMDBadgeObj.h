//
//  DMDBadgeObj.h
//  DancewithMD
//
//  Created by Sagar Mody on 24/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DMDBadgeObj : NSObject {
    
}

@property (nonatomic, copy) NSString *songId;
@property (nonatomic, copy) NSString *clip_partId;
@property (nonatomic, copy) NSString *shareId;
@property (nonatomic, copy) NSString *sharePic;
@property (nonatomic, copy) NSString *badgeName;
@property (nonatomic, copy) NSString *shareText;

@end
