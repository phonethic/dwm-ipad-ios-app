//
//  DMDHomeViewController.h
//  DancewithMD
//
//  Created by Rishi on 01/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface DMDHomeViewController : UIViewController <UIPopoverControllerDelegate>{
    int status;
    int fullscreen;
}
@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;
@property (strong, nonatomic) IBOutlet UIImageView *dmdTitleView;
@property (strong, nonatomic) IBOutlet UIButton *englishBtn;
@property (strong, nonatomic) IBOutlet UIButton *hindiBtn;
@property (strong, nonatomic) IBOutlet UILabel *tickerLabel;
@property (strong, nonatomic) IBOutlet UIButton *googleBtn;
@property (strong, nonatomic) IBOutlet UITextView *helptextView;
@property (strong, nonatomic) IBOutlet UIButton *expandTextBtn;
@property (strong, nonatomic) IBOutlet UIImageView *helpMsgBackImgView;
@property (strong, nonatomic) IBOutlet UIImageView *videoBoximgView;
@property (strong, nonatomic) IBOutlet UILabel *welcomelbl;
@property (strong, nonatomic) IBOutlet UIView *helpView;
@property (strong, nonatomic) IBOutlet UIView *videoView;
@property (strong, nonatomic) IBOutlet UIImageView *infoscreen;
@property (strong, nonatomic) IBOutlet UIView *blackView;

- (IBAction)expandBtnPressed:(id)sender;
- (IBAction)englishBtnPressed:(id)sender;
- (IBAction)hindiBtnPressed:(id)sender;
- (IBAction)googleLoginBtnPressed:(id)sender;

- (IBAction)offlineVideoBtnPressed:(id)sender;


@end
