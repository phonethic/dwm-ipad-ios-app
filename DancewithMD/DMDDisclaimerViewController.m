//
//  DMDDisclaimerViewController.m
//  DancewithMD
//
//  Created by Rishi on 16/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDDisclaimerViewController.h"
#import "MFSideMenu.h"
#import "DMDAppDelegate.h"

#define DISCLLAIMER_LINK [NSString stringWithFormat:@"%@/disclaimer-details.php",LIVE_DWM_SERVER]

@interface DMDDisclaimerViewController ()

@end

@implementation DMDDisclaimerViewController
@synthesize disclaimerWebview;
@synthesize disclaimerindicator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Flurry logEvent:@"Tab_Disclaimer"];
    
    [self setupMenuBarButtonItems];
    
    if([DMDDelegate networkavailable])
    {
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:DISCLLAIMER_LINK]];
        disclaimerWebview.scrollView.bounces = FALSE;
        [disclaimerWebview loadRequest:requestObj];
        [disclaimerindicator startAnimating];
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        [disclaimerindicator stopAnimating];
    }
}

-(void)loadWebViewWithUrl:(NSString *)pagaeType
{

    [disclaimerWebview loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:pagaeType ofType:@"html"]isDirectory:NO]]];
}

#pragma mark - Web View
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [disclaimerindicator stopAnimating];
    //[self zoomToFit];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [disclaimerindicator stopAnimating];
}

-(void)zoomToFit
{
    
    if ([disclaimerWebview respondsToSelector:@selector(scrollView)])
    {
        UIScrollView *scroll=[disclaimerWebview scrollView];
        
        float zoom=disclaimerWebview.bounds.size.width/scroll.contentSize.width;
        [scroll setZoomScale:zoom animated:YES];
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {

    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {

    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}


- (void)viewDidUnload {
    [self setDisclaimerWebview:nil];
    [self setDisclaimerindicator:nil];
    [super viewDidUnload];
}
@end
