//
//  DMDJhalakViewController.h
//  DancewithMD
//
//  Created by Rishi on 03/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMDSongObject;
@interface DMDJhalakViewController : UIViewController
{
    NSMutableData *responseData;
    NSError  *connectionError;
    NSURLConnection *conn;
    int status;
    NSMutableData *responseAsyncData;
    DMDSongObject *songObj;
    NSURLConnection *listConnection;
}

@property (strong, nonatomic) NSMutableArray *songArray;
@property (strong, nonatomic) IBOutlet UITableView *jhlakTableView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinnerIndicatior;

@end
