//
//  DMDLoginViewController.m
//  DancewithMD
//
//  Created by Rishi on 04/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDLoginViewController.h"
#import "DMDAppDelegate.h"

//#define REGISTER_LINK [NSString stringWithFormat:@"%@/api/v1/retrieve/registeruser",LIVE_DWM_SERVER]
//#define LOGIN_LINK [NSString stringWithFormat:@"%@/api/v1/retrieve/login",LIVE_DWM_SERVER]
//#define USER_DETAIL_LINK(USER_ID,TOKEN) [NSString stringWithFormat:@"%@/api/v1/retrieve/user_profile/%@/%@",LIVE_DWM_SERVER,USER_ID,TOKEN]
//#define FORGET_PASSWORD_LINK [NSString stringWithFormat:@"%@/api/v1/retrieve/resetpass",LIVE_DWM_SERVER]
//#define LOGOUT_LINK [NSString stringWithFormat:@"%@/api/v1/retrieve/logout",LIVE_DWM_SERVER]
//
//#define JHALAK_CHECK_LINK @"http://dancewithmadhuri.com/api/v1/retrieve/validjhalak"


#define REGISTER_LINK [NSString stringWithFormat:@"%@/api/retrieve/registeruser",LIVE_DWM_SERVER]
#define LOGIN_LINK [NSString stringWithFormat:@"%@/api/retrieve/login",LIVE_DWM_SERVER]
#define USER_DETAIL_LINK(USER_ID,TOKEN) [NSString stringWithFormat:@"%@/api/retrieve/user_profile/%@/%@",LIVE_DWM_SERVER,USER_ID,TOKEN]
#define FORGET_PASSWORD_LINK [NSString stringWithFormat:@"%@/api/retrieve/resetpass",LIVE_DWM_SERVER]
#define LOGOUT_LINK [NSString stringWithFormat:@"%@/api/retrieve/logout",LIVE_DWM_SERVER]

#define JHALAK_CHECK_LINK @"http://dancewithmadhuri.com/api/retrieve/validjhalak"

@interface DMDLoginViewController ()

@end

@implementation DMDLoginViewController
@synthesize maleButton,femaleButton;
@synthesize loginButton,forgetPassButton,logoutButton;
@synthesize loginpasswordtextfield,loginusernametextfield;
@synthesize registeremailtextfield,registernametextfield,registerpasstextfield,registervpasstextfield;
@synthesize loginprogressbar;
@synthesize registerView;
@synthesize loginView;
@synthesize signupButton;
@synthesize delegate;
@synthesize orlabel,newacclabel;
@synthesize cancelButton,registerButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == loginusernametextfield) {
        [loginpasswordtextfield becomeFirstResponder];
	} else if (textField == loginpasswordtextfield) {
        [loginpasswordtextfield resignFirstResponder];
	} else if (textField == registeremailtextfield) {
        [registernametextfield becomeFirstResponder];
	} else if (textField == registernametextfield) {
        [registerpasstextfield becomeFirstResponder];
	} else if (textField == registerpasstextfield) {
        [registervpasstextfield becomeFirstResponder];
	} else if (textField == registervpasstextfield) {
        [registervpasstextfield resignFirstResponder];
    }
   	return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     //NSLog(@"login view viewDidLoad");
    // Do any additional setup after loading the view from its nib.
    orlabel.font = KABEL_FONT(22);
    newacclabel.font = KABEL_FONT(18);
    loginButton.titleLabel.font = KABEL_FONT(20);
    forgetPassButton.titleLabel.font = KABEL_FONT(14);
    logoutButton.titleLabel.font = KABEL_FONT(20);
    signupButton.titleLabel.font = KABEL_FONT(20);
    cancelButton.titleLabel.font = KABEL_FONT(20);
    registerButton.titleLabel.font = KABEL_FONT(20);
    maleButton.titleLabel.font = KABEL_FONT(18);
    femaleButton.titleLabel.font = KABEL_FONT(18);
    self.contentSizeForViewInPopover = CGSizeMake(300.0, 400.0);
    [maleButton setSelected:TRUE];
    [logoutButton setHidden:TRUE];
    [registerView setHidden:TRUE];
    if([DMDDelegate networkavailable]) {
        [self checkJhalakAsynchronousCall];
    }
    
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NetworkNotifyCallBack) name:NETWORK_NOTIFICATION object:nil];
}
-(void)NetworkNotifyCallBack
{
    DebugLog(@"Price List : Got Network Notification");
    [self checkJhalakAsynchronousCall];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //NSLog(@"login view viewDidAppear");
}

- (void)viewDidDisappear:(BOOL)animated 
{
    [super viewDidDisappear:animated];
    //NSLog(@"login view  viewDidDisappear");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)checkJhalakAsynchronousCall
{
	/****************Asynchronous Request**********************/
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:JHALAK_CHECK_LINK] cachePolicy:NO timeoutInterval:10.0];
    //NSLog(@"%@",urlRequest.URL);
    connectionJhalak = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}
-(void)showAlert:(NSString*) message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                      message:message
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [alertView show];
}

-(BOOL)validateEmail: (NSString *) email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL isValid = [emailTest evaluateWithObject:email];
    return isValid;
}

- (IBAction)registerBtnPressed:(id)sender {
    //NSLog(@"%d",[registerpasstextfield.text length]);
    
    BOOL allAreValidate = YES;
    if([registeremailtextfield.text isEqualToString:@""] || [registernametextfield.text isEqualToString:@""] || [registerpasstextfield.text isEqualToString:@""] || [registervpasstextfield.text isEqualToString:@""])
    {
        [self showAlert:@"Please fill all the fields."];
        allAreValidate = NO;
    }
    else if(![self validateEmail:registeremailtextfield.text])
    {
        [self showAlert:@"Invalid email address.\nPlease check your email address and try again."];
        allAreValidate = NO;

    } else if(([registerpasstextfield.text length] < 8) || ([registervpasstextfield.text length] <8))
    {
        [self showAlert:@"Password should have minimum 8 characters."];
        allAreValidate = NO;

    } else if(![registerpasstextfield.text isEqualToString:registervpasstextfield.text])
    {
        [self showAlert:@"Passwords do not match.\nPlease check your passwords and try again"];
        allAreValidate = NO;
    }
    
    if (allAreValidate){
        if([DMDDelegate networkavailable])
        {
            if([maleButton isSelected]){
                [self registerAsynchronousCall:registeremailtextfield.text name:registernametextfield.text gendertype:@"Male" pass:registerpasstextfield.text confirmpass:registervpasstextfield.text];
            } else {
                [self registerAsynchronousCall:registeremailtextfield.text name:registernametextfield.text gendertype:@"Female" pass:registerpasstextfield.text confirmpass:registervpasstextfield.text];
            }
        } else {
            UIAlertView *errorView = [[UIAlertView alloc]
                                      initWithTitle:@"No Network Connection"
                                      message:@"Please check your internet connection and try again."
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [errorView show];
        }
    }
}

- (IBAction)forgotpassBtnPressed:(id)sender {
    if(![loginusernametextfield.text isEqualToString:@""])
    {
        if([DMDDelegate networkavailable])
        {
            [self resetpasswordAsynchronousCall:loginusernametextfield.text];
        } else {
            UIAlertView *errorView = [[UIAlertView alloc]
                                      initWithTitle:@"No Network Connection"
                                      message:@"Please check your internet connection and try again."
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [errorView show];
        }
    } else {
        [self showAlert:@"Please enter valid username."];
    }
}

- (IBAction)loginBtnPressed:(id)sender {
    if(![loginusernametextfield.text isEqualToString:@""] || ![loginpasswordtextfield.text isEqualToString:@""])
    {
        if([DMDDelegate networkavailable])
        {
            [self loginAsynchronousCall:loginusernametextfield.text pass:loginpasswordtextfield.text];
        } else {
            UIAlertView *errorView = [[UIAlertView alloc]
                                      initWithTitle:@"No Network Connection"
                                      message:@"Please check your internet connection and try again."
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [errorView show];            
        }
    } else {
        [self showAlert:@"Please fill all the fields."];
    }
}

- (IBAction)maleBtnPressed:(id)sender {
    [maleButton setSelected:TRUE];
    [femaleButton setSelected:FALSE];
}

- (IBAction)femaleBtnPressed:(id)sender {
    [maleButton setSelected:FALSE];
    [femaleButton setSelected:TRUE];
}

- (IBAction)logoutBtnPressed:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:LOGIN_USERID];
    NSString *tokenID = [defaults objectForKey:LOGIN_TOKEN];
    //NSLog(@"%@%@",userID,tokenID);
    if([DMDDelegate networkavailable])
    {
         [self logoutAsynchronousCall:userID token:tokenID];
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
    }
}

- (IBAction)signupBtnPressed:(id)sender {
    [UIView  transitionWithView:self.view duration:0.8  options:UIViewAnimationOptionTransitionFlipFromLeft
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         [loginView setHidden:TRUE];
                         [registerView setHidden:FALSE];
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];

}

- (IBAction)cancelBtnPressed:(id)sender {
    [UIView  transitionWithView:self.view duration:0.8  options:UIViewAnimationOptionTransitionFlipFromLeft
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         [loginView setHidden:FALSE];
                         [registerView setHidden:TRUE];
                         [self clear];
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];

}

- (IBAction)facebookLoginBtnPressed:(id)sender {
    DMDAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate openSessionWithAllowLoginUI:YES];
}

-(void)resetpasswordAsynchronousCall:(NSString *)userEmail
{
    [loginprogressbar startAnimating];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:FORGET_PASSWORD_LINK] cachePolicy:NO timeoutInterval:30.0];
    NSMutableDictionary *postReq = [[NSMutableDictionary alloc] init];
    [postReq setObject:userEmail forKey:@"uname"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReq
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (!jsonData) {
        //NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //NSLog(@"---%@---",jsonString);
    }
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody: jsonData];
    connectionPassReset = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)loginAsynchronousCall:(NSString *)userEmail pass:(NSString *)password
{
    [loginprogressbar startAnimating];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LOGIN_LINK] cachePolicy:NO timeoutInterval:30.0];
    NSString *md5Pass = [DMDDelegate md5:password];
    NSMutableDictionary *postReq = [[NSMutableDictionary alloc] init];
    [postReq setObject:userEmail forKey:@"uname"];
    [postReq setObject:md5Pass forKey:@"pass"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReq
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (!jsonData) {
        //NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //NSLog(@"---%@---",jsonString);
    }
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody: jsonData];
    connectionLogin = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)userDetailsAsynchronousCall:(NSString *)userId token:(NSString *)userToken
{
    [loginprogressbar startAnimating];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:USER_DETAIL_LINK(userId,userToken)] cachePolicy:NO timeoutInterval:15.0];
    //NSLog(@"link=%@", urlRequest.URL);
    connectionUserDetail = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)logoutAsynchronousCall:(NSString *)userEmail token:(NSString *)userToken
{
    [loginprogressbar startAnimating];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LOGOUT_LINK] cachePolicy:NO timeoutInterval:30.0];
    NSMutableDictionary *postReq = [[NSMutableDictionary alloc] init];
    [postReq setObject:userEmail forKey:@"uname"];
    [postReq setObject:userToken forKey:@"token"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReq
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (!jsonData) {
        //NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //NSLog(@"---%@---",jsonString);
    }
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody: jsonData];
    connectionLogout = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)registerAsynchronousCall:(NSString *)userEmail name:(NSString *)userName gendertype:(NSString *)gender pass:(NSString *)password confirmpass:(NSString *)cpassword
{
    [loginprogressbar startAnimating];
    NSString *md5Pass = [DMDDelegate md5:password];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:REGISTER_LINK] cachePolicy:NO timeoutInterval:30.0];
    NSMutableDictionary *postReq = [[NSMutableDictionary alloc] init];
    [postReq setObject:userEmail forKey:@"uname"];
    [postReq setObject:userName forKey:@"name"];
    [postReq setObject:gender forKey:@"gender"];
    [postReq setObject:md5Pass forKey:@"pass"];
    [postReq setObject:md5Pass forKey:@"confirm_pass"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReq
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (!jsonData) {
        //NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //NSLog(@"---%@---",jsonString);
    }
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody: jsonData];
    connectionRegister = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //NSLog(@"Error: %@", [error localizedDescription]);
    [loginprogressbar stopAnimating];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError* error;
    [loginprogressbar stopAnimating];
    if (connection==connectionRegister)
    {
        if(responseAsyncData != nil)
        {
//            NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
//            //NSLog(@"\n result:%@\n\n", result);
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSString * result = [json objectForKey:@"data"];
                    if([result hasPrefix:@"Successfully registered"])
                    {
                        [self showAlert:@"User registered successfully"];
                        [self cancelBtnPressed:nil];
                    }
                } else {
                    NSString * result = [json objectForKey:@"message"];
                    if ([result hasPrefix:@"Already registered"]) {
                        [self showAlert:@"User already registered."];
                        [self cancelBtnPressed:nil];
                    } else if ([result hasPrefix:@"Password not matching"]) {
                        [self showAlert:@"Passwords are not matching."];
                    }
                }
            }
        }
        responseAsyncData = nil;
    }
    else if (connection==connectionLogin)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSDictionary *dataDict = [json objectForKey:@"data"];
                        if([dataDict valueForKey:@"token"] != nil) {
                            [Flurry logEvent:@"Login via DWM server"];
                            NSString * result = [dataDict objectForKey:@"token"];
                            //NSLog(@"%@",result);
                            //[self showAlert:[NSString stringWithFormat:@"Login Success. Token = %@",result]];
                            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                            [defaults setObject:DWMSERVER forKey:LOGGED_BY];
                            [defaults setObject:loginusernametextfield.text forKey:LOGIN_USERID];
                            [defaults setObject:loginpasswordtextfield.text forKey:LOGIN_PASSWORD];
                            [defaults setObject:result forKey:LOGIN_TOKEN];
                            NSDate *myDate = [NSDate date];
                            [defaults setObject:myDate forKey:LOGIN_EXPIREDDATE];
                            [defaults setObject:[NSNumber numberWithBool:0] forKey:LOGIN_HELPINFOSCREEN];
                            [defaults synchronize];
                            [loginButton setHidden:TRUE];
                            [forgetPassButton setHidden:TRUE];
                            [signupButton setHidden:TRUE];
                            [logoutButton setHidden:FALSE];
                            [newacclabel setHidden:TRUE];
                            [self userDetailsAsynchronousCall:loginusernametextfield.text token:[dataDict objectForKey:@"token"]];
    //                        if(self.delegate != nil)
    //                            [self.delegate cancelModalView:nil];
                        } else {
                            NSString * result = [dataDict objectForKey:@"data"];
                            //NSLog(@"data:%@",result);
                            [self showAlert:result];
                        } 
                } else {
                    NSString * result = [json objectForKey:@"message"];
                    [self showAlert:result];
                }
            }
        }
        responseAsyncData = nil;
    }
    else if (connection==connectionUserDetail)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* attributeDict in dataArray)
                    {
                        if([attributeDict valueForKey:@"User_Name"] != nil) {
                            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                            [defaults setObject:DWMSERVER forKey:LOGGED_BY];
                            [defaults setObject:[attributeDict valueForKey:@"ID"] forKey:LOGIN_USERID];
                            [defaults setObject:[attributeDict valueForKey:@"User_Name"] forKey:LOGIN_USERNAME];
                            [defaults setObject:[attributeDict valueForKey:@"Email"] forKey:LOGIN_EMAIL];
                            [defaults setObject:[attributeDict valueForKey:@"Gender"] forKey:LOGIN_GENDER];
                            [defaults setObject:[NSNumber numberWithBool:0] forKey:LOGIN_HELPINFOSCREEN];
                            [defaults synchronize];
                            //NSLog(@"%@--%@--%@--%@",[defaults objectForKey:LOGIN_USERID],[defaults objectForKey:LOGIN_USERNAME],[defaults objectForKey:LOGIN_EMAIL],[defaults objectForKey:LOGIN_GENDER]);
                            [loginButton setHidden:TRUE];
                            [forgetPassButton setHidden:TRUE];
                            [signupButton setHidden:TRUE];
                            [logoutButton setHidden:FALSE];
                            [newacclabel setHidden:TRUE];
                            [FBSession.activeSession closeAndClearTokenInformation];
                            if(self.delegate != nil)
                                [self.delegate cancelModalView:nil];
                        } else {
                            NSString * result = [attributeDict objectForKey:@"data"];
                            //NSLog(@"%@",result);
                            if(result != nil && [result isEqualToString:@"0"])
                                [self showAlert:@"Failed to login.Please try after sometime."];
                        }
                    }
                }
            }
        }
        responseAsyncData = nil;
    }
    else if (connection==connectionPassReset)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            if(json != nil) {
                NSString * result = [json objectForKey:@"data"];
                //NSLog(@"%@",result);
                if([result isEqualToString:@"success"])
                {
                    [self showAlert:@"Password reset email sent. Please check your email for further instructions."];
                } else if ([result isEqualToString:@"Not registered"]) {
                    [self showAlert:@"User not registered."];
                } else if ([result isEqualToString:@"failed"]) {
                    [self showAlert:@"Failed to reset password. Please try again later."];
                }
            }
        }
        responseAsyncData = nil;
    }
    else if (connection==connectionLogout)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            if(json != nil) {
                NSString * result = [json objectForKey:@"data"];
                //NSLog(@"%@",result);
                if([result isEqualToString:@"logout"])
                {
                    [self showAlert:@"You have successfully logged out."];
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setObject:@"" forKey:LOGIN_USERID];
                    [defaults setObject:@"" forKey:LOGGED_BY];
                    [defaults setObject:@"" forKey:LOGIN_TOKEN];
                    [defaults setObject:@"" forKey:LOGIN_PASSWORD];
                    [defaults setObject:[NSNumber numberWithBool:0] forKey:LOGIN_HELPINFOSCREEN];
                    [defaults synchronize];
                    [self clear];
                    [loginButton setHidden:FALSE];
                    [forgetPassButton setHidden:FALSE];
                    [signupButton setHidden:FALSE];
                    [newacclabel setHidden:FALSE];
                    [logoutButton setHidden:TRUE];
                } else if ([result isEqualToString:@"Already logout"]) {
                    [self showAlert:@"You are already logged out."];
                    [loginButton setHidden:FALSE];
                    [forgetPassButton setHidden:FALSE];
                    [signupButton setHidden:FALSE];
                    [newacclabel setHidden:FALSE];
                    [logoutButton setHidden:TRUE];
                } else {
                    [self showAlert:result];
                }
            }
        }
        responseAsyncData = nil;
    }
    else if (connection==connectionJhalak)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            if(json != nil) {
                NSDictionary *dictionary = nil;
                if([json valueForKey:@"success"] != nil && ([[json valueForKey:@"success"] boolValue] == 0 || [[json valueForKey:@"success"] hasPrefix:@"false"])) {
                    //NSLog(@"NO jhalak");
                    dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:JHALAK_KEY];
                }else{
                    //NSLog(@"got jhalak");
                    dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:JHALAK_KEY];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:JHALAK_NOTIFICATION object:nil userInfo:dictionary];
            }
        }
        connectionJhalak = nil;
        responseAsyncData = nil;
    }
}

-(void) clear
{
    [loginusernametextfield setText:@""];
    [loginpasswordtextfield setText:@""];
    [registeremailtextfield setText:@""];
    [registernametextfield setText:@""];
    [registerpasstextfield setText:@""];
    [registervpasstextfield setText:@""];
}

- (void)viewDidUnload {
    [self setMaleButton:nil];
    [self setFemaleButton:nil];
    [self setLoginusernametextfield:nil];
    [self setLoginpasswordtextfield:nil];
    [self setRegistervpasstextfield:nil];
    [self setRegisterpasstextfield:nil];
    [self setRegisternametextfield:nil];
    [self setRegisteremailtextfield:nil];
    [self setLogoutButton:nil];
    [self setLoginButton:nil];
    [self setForgetPassButton:nil];
    [self setRegisterView:nil];
    [self setLoginView:nil];
    [self setSignupButton:nil];
    [self setNewacclabel:nil];
    [self setOrlabel:nil];
    [self setCancelButton:nil];
    [self setRegisterButton:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
