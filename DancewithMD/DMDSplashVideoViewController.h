//
//  DMDSplashVideoViewController.h
//  DancewithMD
//
//  Created by Rishi on 10/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface DMDSplashVideoViewController : UIViewController {
    MPMoviePlayerController *moviePlayer;
}

@end
