//
//  DMDExpertsObj.m
//  DancewithMD
//
//  Created by Sagar Mody on 13/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDExpertsObj.h"

@implementation DMDExpertsObj


@synthesize clipId;
@synthesize clipTitle;
@synthesize videoThumb;
@synthesize videoType;
@synthesize videoLink;
@synthesize videoText;
@synthesize embedCode;


@end
