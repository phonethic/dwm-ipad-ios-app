//
//  DMDTweetsViewController.m
//  DancewithMD
//
//  Created by Rishi on 25/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDTweetsViewController.h"
#import "DMDTweetObj.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <Twitter/Twitter.h>
#import "MFSideMenu.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "constants.h"
#import "DMDAppDelegate.h"

#define TWEETS_LIST_LINK @"http://search.twitter.com/search.json?q=%23DanceWithMD"

@interface DMDTweetsViewController ()

@end

@implementation DMDTweetsViewController
@synthesize tweetsArray;
@synthesize tweetsTableView;
@synthesize detailTweetView,tweetcontentTextView,tweetuserImageView,tweetusernameLabel,closeBtn;
@synthesize spinnerIndicatior;
@synthesize bearerToken;
@synthesize dataFilePath;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    closeBtn.frame = CGRectMake(detailTweetView.frame.origin.x+detailTweetView.frame.size.width-25, detailTweetView.frame.origin.y-25, closeBtn.frame.size.width, closeBtn.frame.size.height);
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Flurry logEvent:@"Tab_Tweets"];

    UIBarButtonItem *shareButton = [[UIBarButtonItem alloc] initWithTitle:@"Send Tweet" style:UIBarButtonItemStylePlain target:self action:@selector(tweetTapped)];
    self.navigationItem.leftBarButtonItem = shareButton;
    
//    detailTweetView.layer.cornerRadius = 20;
//    detailTweetView.layer.masksToBounds = YES;
//    detailTweetView.layer.borderColor = [UIColor whiteColor].CGColor;
//    detailTweetView.layer.borderWidth = 4.0;
//    
//    closeBtn.layer.cornerRadius = 25;
//    closeBtn.layer.borderColor = [UIColor whiteColor].CGColor;
//    closeBtn.layer.borderWidth = 3.0;
//    
//    tweetusernameLabel.font = [UIFont fontWithName:@"Kabel Md BT" size:30];
//    tweetcontentTextView.font = [UIFont fontWithName:@"Kabel Md BT" size:20];
//    detailTweetView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.9];
//    [detailTweetView setHidden:YES];
//    [closeBtn setHidden:YES];
    
     tweetsArray = [[NSMutableArray alloc] init];

    [self setupMenuBarButtonItems];
    
    [self settweetDataFilePath];
    
    if([DMDDelegate networkavailable])
    {
        [self tweetListAsynchronousCall];
    } else {
        [self parseFromFile];
    }

}

-(void)settweetDataFilePath
{
    // Get the documents directory
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    // Build the path to the data file
    dataFilePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"tweets.archive"]];
}

-(void)parseFromFile
{
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    // Check if the file already exists
    if ([filemgr fileExistsAtPath: dataFilePath])
    {
        tweetsArray = [NSKeyedUnarchiver unarchiveObjectWithFile: dataFilePath];
        //NSLog(@"from file feedbackArray %@",tweetsArray);
        
        if (tweetsArray.count <= 0) {
            [self showError];
        }else{
            [spinnerIndicatior stopAnimating];
            [tweetsTableView reloadData];
        }
    }else
    {
        [self showError];
    }
}

-(void)showError
{
    [spinnerIndicatior stopAnimating];
    UIAlertView *errorView = [[UIAlertView alloc]
                              initWithTitle:@"No Network Connection"
                              message:@"Please check your internet connection and try again."
                              delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
    [errorView show];
}

- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)tweetTapped {
    
    if ([TWTweetComposeViewController canSendTweet])
    {
        TWTweetComposeViewController *tweetSheet = [[TWTweetComposeViewController alloc] init];
        [tweetSheet setInitialText:@"Tweeting from Dance With Madhuri iOS App!!!"];
        tweetSheet.completionHandler = ^(TWTweetComposeViewControllerResult result){
            if (result == TWTweetComposeViewControllerResultCancelled){
                [self dismissModalViewControllerAnimated:YES];
            } else if (result == TWTweetComposeViewControllerResultDone) {
                //NSLog(@"Tweet Sent");
            }
                
        };
        [self presentModalViewController:tweetSheet animated:YES];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

-(void)tweetListAsynchronousCall
{
    [spinnerIndicatior startAnimating];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://api.twitter.com/oauth2/token"] cachePolicy:NO timeoutInterval:30.0];
    //NSLog(@"%@",urlRequest.URL);
    
    //1. The request must be a HTTP POST request.
    [urlRequest setHTTPMethod:@"POST"];
    
    //2 .The request must include an Authorization header with the value of Basic <base64 encoded value from step 1>.
    NSString *base64EncodedTokens = [self base64EncodedBearerTokenCredentialsWithConsumerKey:@"63xdK9qQ3nORq0WOqiXw" consumerSecret:@"rrIgw30gSajYsPEJJ6Gi6dS9uS8RoS9vzePZlPJq6Qo"];
    
    NSString *authorization = [NSString stringWithFormat:@"Basic %@", base64EncodedTokens];
    [urlRequest setValue:authorization forHTTPHeaderField:@"Authorization"];
    
    
    //3. The request must include a Content-Type header with the value of application/x-www-form-urlencoded;charset=UTF-8.
    [urlRequest setValue:@"application/x-www-form-urlencoded;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    
    
    //4. The body of the request must be grant_type=client_credentials.
    NSString *postParamsString = @"grant_type=client_credentials";
    
    NSData *requestData = [NSData dataWithBytes:[postParamsString UTF8String] length:[postParamsString length]];
    [urlRequest setHTTPBody: requestData];
    
    connection1 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

- (void)invalidateBearerToken
{
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://api.twitter.com/1.1/search/tweets.json?count=10&q=%23dancewithmadhuri&result_type=mixed"] cachePolicy:NO timeoutInterval:30.0];
    //NSLog(@"%@",urlRequest.URL);
    
    [urlRequest setHTTPMethod:@"GET"];
    
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSString *authorization = [NSString stringWithFormat:@"Bearer %@", self.bearerToken];
    [urlRequest setValue:authorization forHTTPHeaderField:@"Authorization"];
    
    connection2 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
    
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //NSLog(@"Error: %@", [error localizedDescription]);
    [spinnerIndicatior stopAnimating];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [spinnerIndicatior stopAnimating];
    NSError* error;
    if(connection==connection1)
    {
        if(responseAsyncData != nil)
        {
            NSError *error = nil;
            id json = [NSJSONSerialization JSONObjectWithData:responseAsyncData options:NSJSONReadingMutableLeaves error:&error];
            
            NSString *tokenType = [json valueForKey:@"token_type"];
            if([tokenType isEqualToString:@"bearer"] == YES) {
                self.bearerToken = [json valueForKey:@"access_token"];
                //NSLog(@"bearer access tokes = %@",self.bearerToken);
                responseAsyncData = nil;
                [self invalidateBearerToken];
            }
        }
    } else if(connection==connection2)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
             if(json != nil) {
                [tweetsArray removeAllObjects];
                 
            //     NSDictionary *searchMetadata = [json valueForKey:@"search_metadata"];
                 NSArray *statuses = [json valueForKey:@"statuses"];
                 
                for (NSDictionary* tweetattributeDict in statuses)
                {
                    tweetObj = [[DMDTweetObj alloc] init];
                    tweetObj.tweetText = [tweetattributeDict objectForKey:@"text"];

                    NSDictionary *userDict = [tweetattributeDict valueForKey:@"user"];
                    
                    tweetObj.fromuserName = [userDict objectForKey:@"screen_name"];
                    tweetObj.imageUrl = [userDict objectForKey:@"profile_image_url"];
                    //NSLog(@"\n name: %@  , image: %@  , text: %@\n", tweetObj.fromuserName, tweetObj.imageUrl, tweetObj.tweetText);
                    [tweetsArray addObject:tweetObj];
                    tweetObj = nil;
                }
                [NSKeyedArchiver archiveRootObject:tweetsArray toFile:dataFilePath];
                [tweetsTableView reloadData];
            }
        }
        responseAsyncData = nil;
    }
}


- (NSString *)base64EncodedBearerTokenCredentialsWithConsumerKey:(NSString *)consumerKey consumerSecret:(NSString *)consumerSecret {
    NSString *encodedConsumerToken = consumerKey ;//[consumerKey st_stringByAddingRFC3986PercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *encodedConsumerSecret = consumerSecret;//[consumerSecret st_stringByAddingRFC3986PercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *bearerTokenCredentials = [NSString stringWithFormat:@"%@:%@", encodedConsumerToken, encodedConsumerSecret];
    NSData *data = [bearerTokenCredentials dataUsingEncoding:NSUTF8StringEncoding];
    return [self base64forData:data];
}

- (NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}


#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [tweetsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UILabel *lblTitle;
    UILabel *lblMessage;
    UIView *contentView;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.userInteractionEnabled = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        cell.textLabel.textColor = [UIColor whiteColor];
//        cell.textLabel.font = [UIFont systemFontOfSize:28.0];
//        cell.detailTextLabel.textColor = [UIColor lightGrayColor];
//        cell.detailTextLabel.font = [UIFont systemFontOfSize:24.0];
//        cell.detailTextLabel.numberOfLines = 5;
        
        contentView = [[UIView alloc] initWithFrame:CGRectMake(20, 10, 980, 130)];
        contentView.backgroundColor = [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:0.5];
        contentView.tag = 11;
        contentView.layer.cornerRadius = 10;
        contentView.layer.masksToBounds = YES;
        
        //Initialize Image View with tag 1.(Thumbnail Image)
        UIImageView *thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(20, 25, 80, 80)];
        thumbImg.tag = 1;
        thumbImg.contentMode = UIViewContentModeScaleToFill;
        thumbImg.layer.masksToBounds = YES;
        thumbImg.layer.cornerRadius = 10;
        thumbImg.layer.masksToBounds = YES;
        thumbImg.layer.borderColor = [UIColor lightGrayColor].CGColor;
        thumbImg.layer.borderWidth = 1.0;
        [contentView addSubview:thumbImg];
        
        //Initialize Label with tag 2.(Title Label)
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(120.0,20.0,800.0,50.0)];
        lblTitle.tag = 2;
//        lblTitle.shadowColor   = [UIColor blackColor];
//        lblTitle.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblTitle.font = KABEL_FONT(30);//[UIFont fontWithName:@"Cochin-Bold" size:24];
        lblTitle.textAlignment = UITextAlignmentLeft;
        lblTitle.textColor = [UIColor whiteColor];
        lblTitle.backgroundColor =  [UIColor clearColor];
        [contentView addSubview:lblTitle];
        
        
        //Initialize Label with tag 2.(Title Label)
        lblMessage = [[UILabel alloc] initWithFrame:CGRectMake(120.0,60.0,800.0,50.0)];
        lblMessage.tag = 3;
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblMovie.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblMessage.font = KABEL_FONT(20);//[UIFont fontWithName:@"Cochin-Bold" size:20];
        lblMessage.textAlignment = UITextAlignmentLeft;
        lblMessage.textColor = [UIColor lightGrayColor];
        lblMessage.backgroundColor =  [UIColor clearColor];
        lblMessage.numberOfLines = 5;
        [contentView addSubview:lblMessage];

        [cell.contentView addSubview:contentView];

        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:0];
        cell.selectedBackgroundView = bgColorView;
    }
    
    cell.userInteractionEnabled = NO;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    DMDTweetObj *temptweetObj  = [tweetsArray objectAtIndex:indexPath.row];
    
    contentView = (UIView *)[cell viewWithTag:11];
    
    lblTitle = (UILabel *)[contentView viewWithTag:2];
    lblMessage = (UILabel *)[contentView viewWithTag:3];
    lblTitle.text = temptweetObj.fromuserName;
    lblMessage.text = temptweetObj.tweetText;

     UIImageView *thumbImgview = (UIImageView *)[contentView viewWithTag:1];
    [thumbImgview setImageWithURL:[NSURL URLWithString:temptweetObj.imageUrl]
             placeholderImage:[UIImage imageNamed:@"unknown.jpg"]
                      success:^(UIImage *image) {
                          //DebugLog(@"success");
                      }
                      failure:^(NSError *error) {
                          //DebugLog(@"write error %@", error);
                      }];

    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    [detailTweetView setHidden:NO];
//    [closeBtn setHidden:NO];
//    DMDTweetObj *temptweetObj   =   (DMDTweetObj *)[tweetsArray objectAtIndex:indexPath.row];
//    [tweetuserImageView setImageWithURL:[NSURL URLWithString:temptweetObj.imageUrl]
//                 placeholderImage:[UIImage imageNamed:@"favicon.png"]
//                          success:^(UIImage *image) {
//                              //DebugLog(@"success");
//                          }
//                          failure:^(NSError *error) {
//                              //DebugLog(@"write error %@", error);
//                          }];
//    tweetusernameLabel.text     =   temptweetObj.fromuserName;
//    tweetcontentTextView.text   =   temptweetObj.tweetText;
//    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
}

- (IBAction)closeBtnClicked:(id)sender {
    [detailTweetView setHidden:YES];
    [closeBtn setHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTweetsTableView:nil];
    [self setDetailTweetView:nil];
    [self setTweetcontentTextView:nil];
    [self setTweetuserImageView:nil];
    [self setTweetusernameLabel:nil];
    [self setCloseBtn:nil];
    [self setSpinnerIndicatior:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}
@end
