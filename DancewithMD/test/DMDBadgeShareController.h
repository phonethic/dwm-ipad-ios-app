//
//  SCViewController.h
//  MadhuriDixitHD
//
//  Created by Kirti Nikam on 22/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface DMDBadgeShareController : UIViewController  <FBUserSettingsDelegate,UITextViewDelegate> {
    int status;
    NSMutableData *responseAsyncData;
    NSURLConnection *connection1;
    NSURLConnection *connection2;
}

@property (nonatomic, copy) NSString *FBtitle;
@property (nonatomic, copy) NSString *FBtLink;
@property (nonatomic, copy) NSString *FBtCaption;
@property (nonatomic, copy) NSString *FBtPic;
@property (nonatomic, readwrite) int showorhide;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *appIcon;
@property (strong, nonatomic) NSMutableDictionary *postParams;
@property (strong, nonatomic) IBOutlet UITextView *message;
@property (strong, nonatomic) IBOutlet UIView *mainFBView;
@property (strong, nonatomic) IBOutlet UIButton *facebookLoginBtn;
@property (strong, nonatomic) IBOutlet UIButton *cancelFBBtn;

- (IBAction)announce:(id)sender;
- (IBAction)loginFB:(id)sender;
- (IBAction)cancelModalView:(id)sender;

@end
