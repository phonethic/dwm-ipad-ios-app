//
//  SCViewController.m
//  MadhuriDixitHD
//
//  Created by Kirti Nikam on 22/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDBadgeShareController.h"
#import "DMDAppDelegate.h"
#import "DMDSongDetailViewController.h"

NSString *const kPlaceholderPostMessage1 = @"Say something about this...";

@interface DMDBadgeShareController ()<UINavigationControllerDelegate>

@property (strong, nonatomic) FBUserSettingsViewController *settingsViewController;
@property (strong, nonatomic) IBOutlet FBProfilePictureView *userProfileImage;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UIButton *announceButton;
@property (strong, nonatomic) IBOutlet UIButton *loginFBBtn;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
- (void)populateUserDetails;
- (void)centerAndShowActivityIndicator;
@end

@implementation DMDBadgeShareController
@synthesize userNameLabel = _userNameLabel;
@synthesize userProfileImage = _userProfileImage;
@synthesize announceButton = _announceButton;
@synthesize activityIndicator = _activityIndicator;
@synthesize cancelButton = _cancelButton;
@synthesize settingsViewController = _settingsViewController;
@synthesize postParams = _postParams;
@synthesize appIcon = _appIcon;
@synthesize message = _message;
@synthesize loginFBBtn = _loginFBBtn;
@synthesize FBtitle = _FBtitle;
@synthesize FBtLink = _FBtLink;
@synthesize FBtPic = _FBtPic;
@synthesize showorhide = _showorhide;
@synthesize facebookLoginBtn = _facebookLoginBtn;
@synthesize FBtCaption = _FBtCaption;


- (void)resetPostMessage
{
    self.message.text = @"";
    self.message.textColor = [UIColor blackColor];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    // Clear the message text when the user starts editing
    if ([textView.text isEqualToString:kPlaceholderPostMessage1]) {
        [self resetPostMessage];
    }
    textView.textColor = [UIColor blackColor];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    // Reset to placeholder text if the user is done
    // editing and no message has been entered.
    if ([textView.text isEqualToString:@""]) {
        self.message.text = kPlaceholderPostMessage1;
        textView.textColor = [UIColor blackColor];
        
    }
}
#pragma mark open graph


// FBSample logic
// Handles the user clicking the Announce button, by either creating an Open Graph Action
// or first uploading a photo and then creating the action.
- (IBAction)announce:(id)sender {
    
    // Add user message parameter if user filled it in
    if (![self.message.text isEqualToString:kPlaceholderPostMessage1] &&
        ![self.message.text isEqualToString:@""]) {
        [self.postParams setObject:self.message.text forKey:@"message"];
    }
    
    [self.postParams setObject:self.FBtitle forKey:@"name"];
    [self.postParams setObject:self.FBtCaption forKey:@"caption"];
    [self.postParams setObject:self.FBtPic forKey:@"picture"];

    
    [FBRequestConnection
     startWithGraphPath:@"me/feed"
     parameters:self.postParams
     HTTPMethod:@"POST"
     completionHandler:^(FBRequestConnection *connection,
                         id result,
                         NSError *error) {
         NSString *alertText;
         if (error) {
             alertText = [NSString stringWithFormat:
                          @"error: domain = %@, code = %d",
                          error.domain, error.code];
         } else {
             //             alertText = [NSString stringWithFormat:
             //                          @"Posted action, id: %@",
             //                          [result objectForKey:@"id"]];
             alertText = @"Your message has been successfully posted on your facebook wall.";
         }
         // Show the result in an alert
         [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                     message:alertText
                                    delegate:nil
                           cancelButtonTitle:@"OK!"
                           otherButtonTitles:nil]
          show];
         NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
         NSString *LoggedBy = [defaults objectForKey:LOGGED_BY];
         NSString *LoggedToken = [defaults objectForKey:LOGIN_TOKEN];
         //NSLog(@"before %@ %@",LoggedToken,FBSession.activeSession.accessToken);
         
         if ([LoggedBy isEqualToString:DWMSERVER]) {
             //NSLog(@"User Logged by DWM server");
             [defaults setObject:@"" forKey:LOGIN_TOKEN];
             [defaults synchronize];
             [FBSession.activeSession closeAndClearTokenInformation];
             //NSLog(@"after %@ %@",LoggedToken,FBSession.activeSession.accessToken);
         }else if ([LoggedBy isEqualToString:FACEBOOKSERVER]){
             //NSLog(@"User Logged by Facebook server");
         }else{
             //NSLog(@"User Logged by Other server -%@-",LoggedBy);
         }
     }];
//    DMDSongDetailViewController *parent = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
//    //call the method on the parent view controller
//    if(parent != nil)
//    {
//        [parent setFullscreenOffShareFB];
//    }
//    parent = nil;
    [self dismissModalViewControllerAnimated:YES];
    
}

- (void)centerAndShowActivityIndicator {
    CGRect frame = self.view.frame;
    CGPoint center = CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame));
    self.activityIndicator.center = center;
    [self.activityIndicator startAnimating];
    
}
// FBSample logic
// Displays the user's name and profile picture so they are aware of the Facebook
// identity they are logged in as.
- (void)populateUserDetails {
    if (FBSession.activeSession.isOpen) {
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
             if (!error) {
                 //NSLog(@"user ID %@",user.id);
                 if(self.showorhide==1)
                 {
//                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//                     [defaults setObject:user.id forKey:@"userId"];
//                     [defaults setObject:[FBSession.activeSession accessToken] forKey:@"accessToken"];
//                     [defaults setObject:[user first_name] forKey:@"userName"];
//                     [defaults setObject:[user objectForKey:@"email"] forKey:@"userEmail"];
//                     [defaults setObject:[user objectForKey:@"gender"] forKey:@"gender"];
//                     [defaults synchronize];
                     [self cancelModalView:nil];
                     return ;
                 }
                 //DebugLog(@"%@",user.name);
                 self.facebookLoginBtn.hidden = TRUE;
                 self.cancelFBBtn.hidden = TRUE;
                 self.userNameLabel.text = user.name;
                 self.userProfileImage.profileID = [user objectForKey:@"id"];
                 _announceButton.enabled =  TRUE;
                 _announceButton.hidden = FALSE;
                 self.mainFBView.hidden = FALSE;
                 //[_loginFBBtn setTitle:@"Logout" forState:UIControlStateNormal];
                 _loginFBBtn.hidden = TRUE;
             } else {
                 _announceButton.enabled =  FALSE;
                 _announceButton.hidden = TRUE;
                 _loginFBBtn.hidden = FALSE;
                 self.facebookLoginBtn.hidden = FALSE;
                 self.cancelFBBtn.hidden = FALSE;
                 self.mainFBView.hidden = TRUE;
                 //[_loginFBBtn setTitle:@"Login" forState:UIControlStateNormal];
             }
         }];
    }
}




- (IBAction)loginFB:(id)sender {
    if([DMDDelegate networkavailable])
    {
        DMDAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        [appDelegate openSessionWithAllowLoginUI:YES];
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
    }
}

- (IBAction)cancelModalView:(id)sender {
    if (![[self modalViewController] isBeingDismissed])
        [self dismissModalViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.postParams =
    [[NSMutableDictionary alloc] initWithObjectsAndKeys:
     @"http://www.dancewithmadhuri.com", @"link",
     @"http://madhuridixit-nene.com/uploads/Madhuri_App_Icon.png", @"picture",
     @"Title of Post", @"name",
     @"", @"caption",
     @"A revolutionized way of learning dance with the Bollywood Dance Diva herself. Come Dance with Madhuri.", @"description",
     nil];
    UIImage *cancelButtonImage;
    cancelButtonImage = [[UIImage imageNamed:@"DEFacebookSendButtonPortrait"] stretchableImageWithLeftCapWidth:4 topCapHeight:0];
    [self.cancelButton setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
    [self.announceButton setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
    [self.loginFBBtn setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
    //_cancelButton.frame = CGRectMake(8, 7, 63, 30);
    //    self.postParams =
    //    [[NSMutableDictionary alloc] initWithObjectsAndKeys:
    //     @"https://developers.facebook.com/ios", @"link",
    //     @"https://developers.facebook.com/attachment/iossdk_logo.png", @"picture",
    //     @"Facebook SDK for iOS", @"name",
    //     @"Build great social apps and get more installs.", @"caption",
    //     @"The Facebook SDK for iOS makes it easier and faster to develop Facebook integrated iOS apps.", @"description",
    //     nil];
    
    // Set up the post information, hard-coded for this sample
    //self.name.text = [self.postParams objectForKey:@"name"];
    //self.caption.text = [self.postParams objectForKey:@"caption"];
    //[self.caption sizeToFit];
    //self.description.text = [self.postParams objectForKey:@"description"];
    //[self.description sizeToFit];
    
    //self.message.text = [NSString stringWithFormat:@"%@\n", self.FBtitle];
    self.message.delegate =  self;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithTitle:@"Settings"
                                              style:UIBarButtonItemStyleBordered
                                              target:self
                                              action:@selector(settingsButtonWasPressed:)];
  
  
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityIndicator.hidesWhenStopped = YES;
    [self.view addSubview:self.activityIndicator];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sessionStateChanged:)
                                                 name:SCSessionStateChangedNotification
                                               object:nil];
    self.mainFBView.layer.cornerRadius = 10;
    self.mainFBView.layer.masksToBounds = YES;
    
    _announceButton.hidden = TRUE;
    _loginFBBtn.hidden = FALSE;
    
    //MDAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [DMDDelegate openSessionWithAllowLoginUI:NO];
    

}

/*
 * A simple way to dismiss the message text view:
 * whenever the user clicks outside the view.
 */
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *) event
{
    UITouch *touch = [[event allTouches] anyObject];
    if ([self.message isFirstResponder] &&
        (self.message != touch.view))
    {
        [self.message resignFirstResponder];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (FBSession.activeSession.isOpen) {
        [self populateUserDetails];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [_message becomeFirstResponder];
}

-(void)settingsButtonWasPressed:(id)sender {
    if (self.settingsViewController == nil) {
        self.settingsViewController = [[FBUserSettingsViewController alloc] init];
    }
    [self.navigationController pushViewController:self.settingsViewController animated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setCancelButton:nil];
    [self setAnnounceButton:nil];
    [self setUserNameLabel:nil];
    [self setMessage:nil];
    [self setLoginFBBtn:nil];
    [self setMainFBView:nil];
    [self setFacebookLoginBtn:nil];
    [self setCancelFBBtn:nil];
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}

- (void)sessionStateChanged:(NSNotification*)notification {
    // A more complex app might check the state to see what the appropriate course of
    // action is, but our needs are simple, so just make sure our idea of the session is
    // up to date and repopulate the user's name and picture (which will fail if the session
    // has become invalid).
    [self populateUserDetails];
}

@end
