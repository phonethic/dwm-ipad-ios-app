//
//  DMDViewController.h
//  DancewithMD
//
//  Created by Rishi on 06/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@class DMDSongObj;
@interface DMDViewController : UIViewController {
    NSMutableData *responseData;
    NSError  *connectionError;
    NSURLConnection *conn;
    int status;
    NSMutableData *responseAsyncData;
    DMDSongObj *songObj;
    NSURLConnection *listConnection;
    int close;
    int fullscreen;
}
@property (nonatomic, copy) NSString *selectedStyleID;
@property (strong, nonatomic) NSMutableArray *songArray;
@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;
@property (strong, nonatomic) IBOutlet UITableView *songTableView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinnerIndicatior;

@end
