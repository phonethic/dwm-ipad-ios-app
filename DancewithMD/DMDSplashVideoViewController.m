//
//  DMDSplashVideoViewController.m
//  DancewithMD
//
//  Created by Rishi on 10/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDSplashVideoViewController.h"
#import "DMDAppDelegate.h"

@interface DMDSplashVideoViewController ()

@end

@implementation DMDSplashVideoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"test1" ofType:@"mp4"]];
    moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:url];
    moviePlayer.view.frame = CGRectMake(0, 0, 1024, 768);

    moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
    moviePlayer.movieSourceType = MPMovieSourceTypeFile;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:moviePlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerDidExitFullscreenNotification
                                               object:moviePlayer];
    if (SYSTEM_VERSION_LESS_THAN(@"6.0"))
    {
    }
    else
    {
        DebugLog(@"Got ios 6");
        [moviePlayer.view setBounds:CGRectMake( 0, 0, 1024, 768)];
        [moviePlayer.view setCenter:CGPointMake(374, 512)];
        if(self.interfaceOrientation == UIInterfaceOrientationLandscapeLeft)
        {
            //NSLog(@"UIInterfaceOrientationLandscapeLeft");
            [moviePlayer.view setTransform:CGAffineTransformMakeRotation(M_PI / -2)];
        }
        if(self.interfaceOrientation == UIInterfaceOrientationLandscapeRight)
        {
            //NSLog(@"UIInterfaceOrientationLandscapeRight");
            [moviePlayer.view setTransform:CGAffineTransformMakeRotation(M_PI / 2)];
        }
    }

    moviePlayer.controlStyle = MPMovieControlStyleNone;
    moviePlayer.shouldAutoplay = YES;
    [moviePlayer setFullscreen:YES animated:NO];
    [moviePlayer prepareToPlay];
    [self.view addSubview:moviePlayer.view];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(moviePlayer != nil)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMoviePlayerDidExitFullscreenNotification
                                                      object:moviePlayer];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:moviePlayer];
        [moviePlayer stop];
        [moviePlayer.view removeFromSuperview];
        [(DMDAppDelegate *)[[UIApplication sharedApplication] delegate] animationStop];
    }
}


- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    
    MPMoviePlayerController *player = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerDidExitFullscreenNotification
                                                  object:player];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:player];
    [moviePlayer stop];
    [moviePlayer.view removeFromSuperview];
    
    [(DMDAppDelegate *)[[UIApplication sharedApplication] delegate] animationStop];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
