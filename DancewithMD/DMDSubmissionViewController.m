//
//  DMDSubmissionViewController.m
//  DancewithMD
//
//  Created by Rishi on 21/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDSubmissionViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <FacebookSDK/FacebookSDK.h>
#import "MFSideMenu.h"
#import "DMDSubmissionDetailViewController.h"
#import "DMDSongObj.h"
#import "constants.h"
#import "DMDAppDelegate.h"

//#define SONG_LIST_LINK @"http://dancewithmadhuri.com/api/v1/retrieve/songinfo"
#define UPLOAD_VIDEO_URL @"http://dancewithmadhuri.com/submit.php"

#define SONG_LIST_LINK @"http://dancewithmadhuri.com/api/retrieve/songinfo"

@interface DMDSubmissionViewController ()

@end

@implementation DMDSubmissionViewController
@synthesize submitProgressBar;
@synthesize moviePlayer;
//@synthesize submissionCategorypicker;
@synthesize selectedvideoId;
@synthesize selectedvideoUserId;
@synthesize selectedsongBtn,uploadBtn;
@synthesize submissionInfoLabel,uploadInfoLabel;
@synthesize submissionTableView,uploadTableView;
@synthesize songArray;
@synthesize uploadinglbl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self closeTableViews];
}

-(void)closeTableViews
{
    submissionTableView.frame = CGRectMake(submissionTableView.frame.origin.x,submissionTableView.frame.origin.y, submissionTableView.frame.size.width,0);
    submissionTableView.alpha = 0.0;
    selectedsongBtn.enabled = TRUE;
    selectdBtnStatus = NO;
    [selectedsongBtn setBackgroundImage: [UIImage imageNamed:@"button_off.png"] forState:UIControlStateNormal];
    
    uploadTableView.frame = CGRectMake(uploadTableView.frame.origin.x,uploadTableView.frame.origin.y, uploadTableView.frame.size.width,0);
    uploadTableView.alpha = 0.0;
    uploadBtn.enabled = TRUE;
    uploadBtnStatus = NO;
    [uploadBtn setBackgroundImage: [UIImage imageNamed:@"button_off.png"] forState:UIControlStateNormal];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Flurry logEvent:@"Tab_Submission"];

//    submissionCategorypicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-216, self.view.frame.size.width, self.view.frame.size.height)];
//    submissionCategorypicker.showsSelectionIndicator = YES;
//    submissionCategorypicker.dataSource = self;
//    submissionCategorypicker.delegate = self;
//    submissionCategorypicker.hidden = TRUE;
//    [self.view addSubview:submissionCategorypicker];
    
    selectedvideoId = @"";
    selectedvideoUserId = @"";
    
    submissionInfoLabel.font = KABEL_FONT(20);
    uploadInfoLabel.font = KABEL_FONT(20);
    submissionInfoLabel.textColor = [UIColor whiteColor];
    uploadInfoLabel.textColor = [UIColor whiteColor];
    selectedsongBtn.titleLabel.font = KABEL_FONT(30);
    uploadBtn.titleLabel.font = KABEL_FONT(30);
    uploadinglbl.font = KABEL_FONT(20);
    [uploadinglbl setHidden:TRUE];
    
    
    submissionTableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dropdown.png"]];
    uploadTableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dropdown.png"]];
    
    songArray = [[NSMutableArray alloc] init];
    
    [self songListAsynchronousCall];
    [self setupMenuBarButtonItems];
}

- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)songListAsynchronousCall
{
   	/****************Asynchronous Request**********************/
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:SONG_LIST_LINK] cachePolicy:NO timeoutInterval:10.0];
    //Get list of all songs
    connection4 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}


#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //NSLog(@"Error: %@", [error localizedDescription]);
    [submitProgressBar stopAnimating];
    [uploadinglbl setHidden:TRUE];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if(connection==connection3)
    {
        NSString *result = [[NSString alloc] initWithBytes:[responseAsyncData mutableBytes]
                                                    length:[responseAsyncData length] encoding:NSUTF8StringEncoding];
        //NSLog(@"%@", result ) ;
        [submitProgressBar stopAnimating];
        [uploadinglbl setHidden:TRUE];
        if(result != nil) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                message:result
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
        }
        responseAsyncData = nil;
        connection3=nil;
    }
    else if(connection==connection4)
    {
        if(responseAsyncData != nil)
        {
            //NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
            //NSLog(@"\n result:%@\n\n", result);
            NSError* error;
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    [songArray removeAllObjects];
                     NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* songattributeDict in dataArray) {
                        songObj = [[DMDSongObj alloc] init];
                        songObj.songId = [songattributeDict objectForKey:@"Song_ID"];
                        songObj.songTitle = [songattributeDict objectForKey:@"Title"];
                        songObj.songMovie = [songattributeDict objectForKey:@"Movie"];
                        songObj.songChoreographer = [songattributeDict objectForKey:@"Choreographer"];
                        songObj.songPic = [songattributeDict objectForKey:@"Thumbnail"];
                        songObj.songLock = [songattributeDict objectForKey:@"Lock_Type"];
                        songObj.songCategory = [songattributeDict objectForKey:@"Category"];
                        songObj.songCategoryName = [songattributeDict objectForKey:@"Category_Name"];
                        //NSLog(@"\nsongid: %@ , title: %@  , movie: %@  , choreographer: %@  , style: %@  , picture: %@\n", songObj.songId, songObj.songTitle, songObj.songMovie, songObj.songChoreographer, songObj.songStyle, songObj.songPic);
                        if(![[songattributeDict objectForKey:@"Category"] isEqualToString:@"5"])
                        {
                            [songArray addObject:songObj];
                        }
                        songObj = nil;
                    }
                }
                [submissionTableView reloadData];
                [uploadTableView reloadData];
            }
        }
		responseAsyncData = nil;
        connection4=nil;

    }
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:submissionTableView] || [tableView isEqual:uploadTableView]) {
            return 58;
    }
    return 100;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([tableView isEqual:submissionTableView] || [tableView isEqual:uploadTableView]) {
        return [songArray count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([tableView isEqual:submissionTableView] || [tableView isEqual:uploadTableView])
    {
        static NSString *CellIdentifier;
        if ([tableView isEqual:submissionTableView])
        {
            CellIdentifier = @"submissionCell";
        }else{
            CellIdentifier = @"uploadCell";
        }
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.accessoryType              = UITableViewCellAccessoryNone;
            cell.userInteractionEnabled     = YES;
            cell.selectionStyle             = UITableViewCellSelectionStyleNone;
            cell.textLabel.textColor        = [UIColor whiteColor];
            cell.textLabel.font             = KABEL_FONT(30);
            cell.textLabel.textAlignment    = UITextAlignmentCenter;
            cell.textLabel.shadowColor = [UIColor blackColor];
            cell.textLabel.shadowOffset = CGSizeMake(0, -1);
            
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:0.5];
            cell.selectedBackgroundView = bgColorView;
           // //NSLog(@"height %f width %f",cell.frame.size.height,cell.frame.size.width);
            
            UIImageView *seperatorImg = [[UIImageView alloc] initWithFrame:CGRectMake(0,54, 348, 8)];
            seperatorImg.image = [UIImage imageNamed:@"dropdown_table_line.png"];
            seperatorImg.tag = 5;
            seperatorImg.contentMode = UIViewContentModeScaleAspectFit;
            [cell.contentView addSubview:seperatorImg];
            
        }
        cell.userInteractionEnabled = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        //NSLog(@"songArray %@",[songArray objectAtIndex:indexPath.row]);
        DMDSongObj *tempsongObj  = [songArray objectAtIndex:indexPath.row];
        cell.textLabel.text = [[NSString stringWithFormat:@"%@",tempsongObj.songTitle] lowercaseString];
        return cell;

    }
      return nil;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:submissionTableView]) {
        //NSLog(@"submission : You selected %@",[songArray objectAtIndex:indexPath.row]);
        DMDSongObj *tempsongObj  = (DMDSongObj *)[songArray objectAtIndex:indexPath.row];
        DMDSubmissionDetailViewController *songdetailController = [[DMDSubmissionDetailViewController alloc] initWithNibName:@"DMDSubmissionDetailViewController" bundle:nil];
        songdetailController.title = tempsongObj.songTitle;
        songdetailController.selectedSongId = tempsongObj.songId;
        [self.navigationController pushViewController:songdetailController animated:YES];
        [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
    }
    else if ([tableView isEqual:uploadTableView]) {
        //NSLog(@"upload: You selected %@",[songArray objectAtIndex:indexPath.row]);
        DMDSongObj *tempsongObj  = (DMDSongObj *)[songArray objectAtIndex:indexPath.row];
        selectedvideoId = tempsongObj.songId;
        [self showActionsheet:tempsongObj.songTitle];
        [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
    }
}

//#pragma mark Pickerview methods
//- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
//	
//	return 1;
//}
//
//- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
//	
//    return [categoryArray count];
//}
//
//- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
//    return [categoryArray objectAtIndex:row];
//}
//
//- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
//    ////NSLog(@"Selected Color: %@. Index of selected color: %i", [categoryArray objectAtIndex:row], row);
//    ////NSLog(@"Selected Song : %@",  [NSString stringWithFormat:@"%d",row+1]);
//    [self videoListAsynchronousCall:[NSString stringWithFormat:@"%d",row+1]];
//}

-(void)playVideo:(NSString *)videoUrl
{

    if(self.moviePlayer != nil) {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:videoUrl]];
    self.moviePlayer.view.frame = CGRectMake(590, 100, 400, 400);
    [moviePlayer setControlStyle:MPMovieControlStyleDefault];
    self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
    self.moviePlayer.shouldAutoplay = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.moviePlayer];
    [self.view addSubview:moviePlayer.view];
    [self.view bringSubviewToFront:moviePlayer.view];
    [moviePlayer prepareToPlay];
    [moviePlayer play];

}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    [moviePlayer setFullscreen:FALSE animated:TRUE];  
    MPMoviePlayerController *player = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    if ([player respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [player.view removeFromSuperview];
    }
    moviePlayer = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidUnload {
    [self setUploadBtn:nil];
    [self setSelectedsongBtn:nil];
    [self setSubmissionInfoLabel:nil];
    [self setUploadInfoLabel:nil];
    [self setSubmissionTableView:nil];
    [self setUploadTableView:nil];
    [self setSubmitProgressBar:nil];
    [self setUploadinglbl:nil];
    [super viewDidUnload];
}
- (IBAction)selectedsongBtnPressed:(UIButton *)sender {
    if([DMDDelegate networkavailable])
    {
        [self pullUpDownAnimation:submissionTableView btnstatus:selectdBtnStatus btn:sender];
        if (selectdBtnStatus) {
            selectdBtnStatus = NO;
            [sender setBackgroundImage: [UIImage imageNamed:@"button_off.png"] forState:UIControlStateNormal];
        }
        else
        {
            selectdBtnStatus = YES;
            [sender setBackgroundImage:[UIImage imageNamed:@"button_on.png"] forState:UIControlStateNormal];
        }
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
    }


   //* if (submissionCategorypicker) submissionCategorypicker.hidden = !submissionCategorypicker.hidden;
   // NSDictionary *params = @[@"ids" : pageId];
//*    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
//	[params setObject:@"ids" forKey:@"http://dancewithmadhuri.com/video.php%3Fvid=  24"];
//   [FBRequestConnection startWithGraphPath:@"comments" parameters:params HTTPMethod:@"GET"
//                        completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//                            NSArray* collection = (NSArray*)[result data];
//                            //NSLog(@"You have %d albums", [collection count]);
//     }];
}


//------------- upload Video----------------------
- (IBAction)uploadBtnPressed:(UIButton *)sender {
    
    
    if([DMDDelegate networkavailable])
    {
        [self pullUpDownAnimation:uploadTableView btnstatus:uploadBtnStatus btn:sender];
        if (uploadBtnStatus) {
            uploadBtnStatus = NO;
            [sender setTitle:@"upload video" forState:UIControlStateNormal];
            [sender setBackgroundImage: [UIImage imageNamed:@"button_off.png"] forState:UIControlStateNormal];
        }else{
            uploadBtnStatus = YES;
            [sender setTitle:@"select song" forState:UIControlStateNormal];
            [sender setBackgroundImage:[UIImage imageNamed:@"button_on.png"] forState:UIControlStateNormal];
        }
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
    }
    //NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"video" ofType:@"mp4"]];
    ////NSLog(@"url=%@",url);
    //[self sendVideo:url];
}

-(void)showActionsheet:(NSString *)songName
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIActionSheet *uiActionSheetP= [[UIActionSheet alloc]
                                        initWithTitle: [NSString stringWithFormat:@"upload video for %@ song",songName]
                                        delegate:self
                                        cancelButtonTitle:@"CANCEL"
                                        destructiveButtonTitle:nil
                                        otherButtonTitles:@"Take Video",@"Choose Existing Video", nil];
        uiActionSheetP.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        [uiActionSheetP showInView:self.view];
    }
    else
    {
        UIActionSheet *uiActionSheetP= [[UIActionSheet alloc]
                                        initWithTitle: [NSString stringWithFormat:@"upload video for %@ song",songName]
                                        delegate:self
                                        cancelButtonTitle:@"CANCEL"
                                        destructiveButtonTitle:nil
                                        otherButtonTitles:@"Choose Existing Video", nil];
        uiActionSheetP.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        uiActionSheetP.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        [uiActionSheetP showInView:self.view];
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == [actionSheet numberOfButtons]-1)
        return;

    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && [[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Take Video"])
    {
        [self useCamera];
    }
    else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum] && [[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Choose Existing Video"])
    {
        [self useCameraRoll];
    }
//	if (buttonIndex == 0) {
//        [self useCamera];
//    } else if (buttonIndex == 1) {
//        [self useCameraRoll];
//    }
}
- (void) useCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeMovie,nil];
        imagePicker.videoQuality = UIImagePickerControllerQualityTypeMedium;
        imagePicker.allowsEditing = NO;
        imagePicker.videoMaximumDuration = 300;  //in Seconds
        //[self presentModalViewController:imagePicker animated:YES];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            popover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
            [popover presentPopoverFromRect:CGRectMake(0, 0, 320, 480) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            //self.popover = popover;
        } else {
            [self presentModalViewController:imagePicker animated:YES];
        }
    }
}

- (void) useCameraRoll
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        UIImagePickerController *imagePicker =  [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeMovie,nil];
        imagePicker.allowsEditing = NO;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            popover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
            [popover presentPopoverFromRect:CGRectMake(0, 0, 320, 480) inView:uploadTableView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            popover.delegate = self;
            //self.popover = popover;
        } else {
            [self presentModalViewController:imagePicker animated:YES];
        }
    }
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    [self dismissModalViewControllerAnimated:YES];
    if (popover != nil) {
        [popover dismissPopoverAnimated:YES];
        popover=nil;
    }
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        // Code here to support image if enabled
        //NSLog(@"image picked");
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
		// Code here to support video if enabled
        //NSLog(@"%@",[info objectForKey:@"UIImagePickerControllerMediaURL"]);
        NSString *moviePath = [[info objectForKey:
                                UIImagePickerControllerMediaURL] path];
        
        if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (moviePath)) {
            UISaveVideoAtPathToSavedPhotosAlbum (
                                                 moviePath, nil, nil, nil);
        }
        [self sendVideo:[info objectForKey:UIImagePickerControllerMediaURL]];
	}
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController*)picker
{
    [picker dismissModalViewControllerAnimated:YES];
    if (popover != nil) {
        [popover dismissPopoverAnimated:YES];
        popover=nil;
    }
    
}


-(void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:ALERT_TITLE
                              message: @"Failed to save image"\
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

-(void)sendVideo:(NSURL *)movieUrl
{
    //NSLog(@"selected song id to upload %@",selectedvideoId);
    [submitProgressBar startAnimating];
    [uploadinglbl setHidden:FALSE];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:@"userId"];
    NSData *webData = [NSData dataWithContentsOfURL:movieUrl];
    NSString *postLength = [NSString stringWithFormat:@"%d", [webData length]];
    NSString *filename = @"video";
    NSMutableURLRequest *request= [[NSMutableURLRequest alloc] init];
    NSString *boundary = @"---------------------------127412016815782193391561906476";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    
    [request setURL:[NSURL URLWithString:UPLOAD_VIDEO_URL]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *postbody = [NSMutableData data];
    
    //1 "Song id"
    [postbody appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"song_id\"\r\n\r\n\%@",selectedvideoId] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //2 "upload_file"
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"upload_file\"; filename=\"%@.mp4\"", filename] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithString:@"\r\nContent-Type: video/mp4\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[NSData dataWithData:webData]];
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //3 "code"
    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"code\"\r\n\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //4 "code"
    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"user_id\"\r\n\r\n%@", userID] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:postbody];
    
    connection3 = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    
    //    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    //    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    //    //NSLog(@"%@", returnString);
}



-(void)pullUpDownAnimation:(UITableView *)tableView btnstatus:(BOOL)btnStatus btn:(UIButton *)lbtn
{
    //NSLog(@"tableView.frame.origin.y %f -------- tableView.frame.size.height %f",tableView.frame.origin.y,tableView.frame.size.height);
    lbtn.enabled = FALSE;
    if (btnStatus)
    {
        [UIView  transitionWithView:tableView duration:1  options:UIViewAnimationOptionTransitionNone
                         animations:^(void) {
                             tableView.frame = CGRectMake(tableView.frame.origin.x,tableView.frame.origin.y, tableView.frame.size.width,0);
                         }
                         completion:^(BOOL finished) {
                             tableView.alpha = 0.0;
                             lbtn.enabled = TRUE;
                         }];
    }
    else
    {
        //NSLog(@"y : %f",tableView.frame.origin.y-tableView.frame.size.height);
        [UIView  transitionWithView:tableView duration:1  options:UIViewAnimationOptionTransitionNone
                         animations:^(void) {
                             tableView.alpha = 1.0;
                             tableView.frame = CGRectMake(tableView.frame.origin.x,tableView.frame.origin.y, tableView.frame.size.width,150);
                         }
                         completion:^(BOOL finished) {
                             lbtn.enabled = TRUE;
                         }];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}
@end
