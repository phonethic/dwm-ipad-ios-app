//
//  DMDFBViewController.m
//  DancewithMD
//
//  Created by Rishi on 09/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDFBViewController.h"
#import "MFSideMenu.h"
#import "DMDAppDelegate.h"

//#define FACEBOOK_LINK @"http://192.168.254.22:81/test.html"
#define FACEBOOK_LINK @"http://m.facebook.com/DanceWithMadhuriOfficial?v=feed&refid=17"//@"https://www.facebook.com/DanceWithMadhuriOfficial?v=feed"


@interface DMDFBViewController ()

@end

@implementation DMDFBViewController
@synthesize fbWebView;
@synthesize spinnerIndicatior;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupMenuBarButtonItems];
    [_webviewtoolBar setHidden:TRUE];
    [fbWebView setHidden:TRUE];
    
    if([DMDDelegate networkavailable]) {
        NSURL *url = [NSURL URLWithString:FACEBOOK_LINK];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [fbWebView setDelegate:self];
        [fbWebView loadRequest:requestObj];
        [fbWebView setHidden:FALSE];
        [_webviewtoolBar setHidden:FALSE];
        [_barbackBtn setEnabled:FALSE];
        [_barfwdBtn setEnabled:FALSE];
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
    }
    
}
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setFbWebView:nil];
    [self setBarbackBtn:nil];
    [self setBarfwdBtn:nil];
    [self setBarrefreshBtn:nil];
    [self setWebviewtoolBar:nil];
    [self setSpinnerIndicatior:nil];
    [super viewDidUnload];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    //NSLog(@"request : %@",webView.request.URL);
    [spinnerIndicatior startAnimating];
    [_barrefreshBtn setEnabled:FALSE];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [spinnerIndicatior stopAnimating];
    if(fbWebView.canGoBack)
    {
        [_barbackBtn setEnabled:TRUE];
    } else {
        [_barbackBtn setEnabled:FALSE];
    }
    if(fbWebView.canGoForward)
    {
        [_barfwdBtn setEnabled:TRUE];
    } else {
        [_barfwdBtn setEnabled:FALSE];
    }
    [_barrefreshBtn setEnabled:TRUE];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [spinnerIndicatior stopAnimating];
    [_barrefreshBtn setEnabled:TRUE];
}

- (IBAction)webviewbackBtnPressed:(id)sender {
    [fbWebView goBack];
}

- (IBAction)webviewfwdBtnPressed:(id)sender {
    [fbWebView goForward];
}

- (IBAction)webviewrefreshBtnPressed:(id)sender {
    [fbWebView reload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}
@end
