//
//  DMDLearnDanceViewController.m
//  DancewithMD
//
//  Created by Kirti Nikam on 12/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDLearnDanceViewController.h"
#import "DMDViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "MFSideMenu.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DMDAppDelegate.h"
#import "DMDSongCategoryObj.h"

//#define SONG_CATEGORY_LIST_LINK [NSString stringWithFormat:@"%@/api/v1/retrieve/category",LIVE_DWM_SERVER]
#define SONG_CATEGORY_LIST_LINK [NSString stringWithFormat:@"%@/api/retrieve/category",LIVE_DWM_SERVER]

@interface DMDLearnDanceViewController ()

@end

@implementation DMDLearnDanceViewController
@synthesize songCategoryTableView;
@synthesize spinnerIndicatior;
@synthesize songCategoryArray;
@synthesize moviePlayer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //NSLog(@"viewWillDisappear called");
    if(self.moviePlayer != nil && !fullscreen) {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Flurry logEvent:@"Tab_LearnDance"];
    [self setupMenuBarButtonItems];
    songCategoryArray = [[NSMutableArray alloc] init];
    
    if([DMDDelegate networkavailable])
    {
        [self songsListAsynchronousCall];
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        [spinnerIndicatior stopAnimating];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}
-(void)dealloc{
    if (listConnection != nil) {
        [listConnection cancel];
        listConnection = nil;
    }
}
- (void)viewDidUnload {
    [self setSpinnerIndicatior:nil];
    [self setSongCategoryTableView:nil];
    [super viewDidUnload];
}


#pragma Internal Callbacks
-(void)songsListAsynchronousCall
{
    [spinnerIndicatior startAnimating];
	/****************Asynchronous Request**********************/
    
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:SONG_CATEGORY_LIST_LINK] cachePolicy:NO timeoutInterval:15.0];
	listConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
	//NSLog(@"%@",urlRequest.URL);
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //NSLog(@"Error: %@", [error localizedDescription]);
    [spinnerIndicatior stopAnimating];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [spinnerIndicatior stopAnimating];
	if(responseAsyncData != nil)
    {
		NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
		//NSLog(@"\n result:%@\n\n", result);
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
        //NSLog(@"json: %@", json);
        if(json != nil) {
            NSString *statusValue = [json objectForKey:@"success"];
            if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                [songCategoryArray removeAllObjects];
                NSArray *dataArray = [json objectForKey:@"data"];
                for (NSDictionary* songattributeDict in dataArray) {
                    songCatObj = [[DMDSongCategoryObj alloc] init];
                    songCatObj.styleId      = [songattributeDict objectForKey:@"Style_ID"];
                    songCatObj.styleName    = [songattributeDict objectForKey:@"Style_Name"];
                    songCatObj.styleThumbnail   = [songattributeDict objectForKey:@"Thumbnail"];
                    songCatObj.styleVideoLink   = [songattributeDict objectForKey:@"Video_Link"];
                    songCatObj.styleImageLink   = [songattributeDict objectForKey:@"ImageLink"];
                    songCatObj.styleCategory    = [songattributeDict objectForKey:@"Category"];
//                    NSLog(@"styleId: %@ , styleName: %@  , styleThumbnail: %@  , styleVideoLink: %@  , styleImageLink: %@  , styleCategory: %@\n", songCatObj.styleId,songCatObj.styleName,songCatObj.styleThumbnail,songCatObj.styleVideoLink,songCatObj.styleImageLink,songCatObj.styleCategory);
                    if(![[songattributeDict objectForKey:@"Style_ID"] isEqualToString:@"5"])
                    {
                        [songCategoryArray addObject:songCatObj];
                    }
                    songCatObj = nil;
                }
            }
        }
	}
    listConnection = nil;
    responseAsyncData = nil;
    //NSLog(@"array count: %d", [songCategoryArray count]);
    [songCategoryTableView reloadData];
}
#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 226;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [songCategoryArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UIImageView *thumbImg;
    UILabel *lblCatgName;
    static NSString *CellIdentifier = @"CatgCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        //Initialize Image View with tag 1.(Thumbnail Image)
        thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(64, 2, 320, 218)];
        thumbImg.tag = 1;
        thumbImg.userInteractionEnabled = TRUE;
        thumbImg.multipleTouchEnabled = TRUE;
        thumbImg.contentMode = UIViewContentModeScaleToFill;
        thumbImg.layer.masksToBounds = YES;
        [cell.contentView addSubview:thumbImg];
        
        lblCatgName = [[UILabel alloc] initWithFrame:CGRectMake(570.0,30.0,400.0,150.0)];
        lblCatgName.text = @"lblCatgName";
        lblCatgName.font = KABEL_FONT(40);
        lblCatgName.tag = 2;
        lblCatgName.textAlignment = UITextAlignmentLeft;
        lblCatgName.textColor = [UIColor whiteColor];
        lblCatgName.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblCatgName];
        
        UIButton *videobutton = [UIButton buttonWithType:UIButtonTypeCustom];
        [videobutton setFrame:CGRectMake(64, 2, 320, 218)];
        [videobutton setBackgroundColor:[UIColor clearColor]];
        videobutton.tag = 100;
        [videobutton addTarget:self action:@selector(songIntroBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:videobutton];
        
        UIImageView *videoImg = [[UIImageView alloc] init];
        videoImg.image = [UIImage imageNamed:@"playbutton.png"];
        videoImg.contentMode = UIViewContentModeScaleAspectFit;
        videoImg.tag = 3;
        [videoImg setHidden:TRUE];
        //NSLog(@"%f %f",bgbutton.center.x+50,bgbutton.center.y-50);
        videoImg.frame = CGRectMake(175.0,59.5,100.0,100.0);
        [cell.contentView addSubview:videoImg];
        
    }
    DMDSongCategoryObj *tempCategObj  = [songCategoryArray objectAtIndex:indexPath.row];
    thumbImg = (UIImageView *)[cell viewWithTag:1];
    lblCatgName = (UILabel *)[cell viewWithTag:2];
    lblCatgName.text =  tempCategObj.styleName;
    //NSLog(@"%@ %@",tempCategObj.styleThumbnail,[DMDDelegate URLEncode:tempCategObj.styleThumbnail]);
    [thumbImg setImageWithURL:[NSURL URLWithString:[DMDDelegate URLEncode:tempCategObj.styleThumbnail]]
                 placeholderImage:nil
                          success:^(UIImage *image) {
                              //NSLog(@"success");
                          }
                          failure:^(NSError *error) {
                              NSLog(@"write error %@", error);
                          }];
    
    UIImageView *playImgview = (UIImageView *)[cell viewWithTag:3];
    if(tempCategObj.styleVideoLink == nil || [tempCategObj.styleVideoLink isEqualToString:@""])
    {
        [playImgview setHidden:TRUE];
    } else {
        [playImgview setHidden:FALSE];
    }
    
    UIImageView *bg = [[UIImageView alloc] initWithFrame:cell.frame];
    UIImageView *bgon = [[UIImageView alloc] initWithFrame:cell.frame];
    bg.image = [UIImage imageNamed:@"tab_off.png"];
    bgon.image = [UIImage imageNamed:@"tab_on.png"];
    cell.backgroundView = bg;
    cell.selectedBackgroundView = bgon;
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    DMDSongCategoryObj *tempCategObj  = [songCategoryArray objectAtIndex:indexPath.row];
    DMDViewController *categdetailViewController = [[DMDViewController alloc] initWithNibName:@"DMDViewController" bundle:nil] ;
    categdetailViewController.selectedStyleID = tempCategObj.styleId;
    //NSLog(@"selectedStyleID %@",tempCategObj.styleId);
    categdetailViewController.title = tempCategObj.styleName;
    [self.navigationController pushViewController:categdetailViewController animated:YES];
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:NO];
    [self stopVideo];
}

- (void)songIntroBtnTapped:(id)sender event:(id)event
{
    NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.songCategoryTableView];
	NSIndexPath *indexPath = [self.songCategoryTableView indexPathForRowAtPoint: currentTouchPosition];
	if (indexPath != nil)
	{
        UITableViewCell *cell  = (UITableViewCell *)[self.songCategoryTableView cellForRowAtIndexPath:indexPath];
        //UIButton *button = (UIButton *)[cell viewWithTag:100];
        //NSLog(@"Button Tap = %d",indexPath.row);
        DMDSongCategoryObj *tempCategObj  = [songCategoryArray objectAtIndex:indexPath.row];
        //NSLog(@"----IS VIDEO--->%@",tempCategObj.styleVideoLink);
        if(tempCategObj.styleVideoLink == nil || [tempCategObj.styleVideoLink isEqualToString:@""])
        {
            //NSLog(@"----IS VIDEO--->%@",tempsongObj.isVideo);
        } else {
            [self playThumnailVideo:cell index:indexPath];
        }
    }
}
-(void)playThumnailVideo:(UITableViewCell *)cell index:(NSIndexPath *)lindexPath
{
    DMDSongCategoryObj *tempCategObj  = [songCategoryArray objectAtIndex:lindexPath.row];
    //NSLog(@"VIDEO URL=%@",tempsongObj.videoLink);
    
    if(self.moviePlayer != nil) {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:[DMDDelegate URLEncode:tempCategObj.styleVideoLink]]];
    self.moviePlayer.view.frame = CGRectMake(64, 2, 320, 218);
    [moviePlayer setControlStyle:MPMovieControlStyleDefault];
    self.moviePlayer.scalingMode = MPMovieScalingModeFill;
    self.moviePlayer.shouldAutoplay = YES;
    self.moviePlayer.controlStyle = MPMovieControlStyleEmbedded;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayEnterFullScreen:)
                                                 name:MPMoviePlayerWillEnterFullscreenNotification
                                               object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayExitFullScreen:)
                                                 name:MPMoviePlayerDidExitFullscreenNotification
                                               object:self.moviePlayer];
    //working
    moviePlayer.view.tag = 420;
    [cell.contentView addSubview:moviePlayer.view];
    [cell bringSubviewToFront:moviePlayer.view];
    [moviePlayer prepareToPlay];
    [moviePlayer play];
}

-(void)stopVideo
{
    if(self.moviePlayer != nil) {
        close = 1;
        [moviePlayer stop];
        //NSLog(@"close=%d",close);
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
    
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    //NSLog(@"moviePlayBackDidFinish close=%d",close);
    [moviePlayer setFullscreen:FALSE animated:TRUE];
    MPMoviePlayerController *player = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerWillEnterFullscreenNotification object:player];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerDidExitFullscreenNotification object:player];
    
    if ([player respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [player.view removeFromSuperview];
    }
    moviePlayer = nil;
}

- (void) moviePlayEnterFullScreen:(NSNotification*)notification {
    //NSLog(@"moviePlayEnterFullScreen");
    fullscreen = 1;
    self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
    
}

- (void) moviePlayExitFullScreen:(NSNotification*)notification {
    //NSLog(@"moviePlayExitFullScreen");
    fullscreen = 0;
    [moviePlayer play];
    self.moviePlayer.scalingMode = MPMovieScalingModeFill;
}

@end
