//
//  DMDFeedbackViewController.m
//  DancewithMD
//
//  Created by Rishi on 25/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDFeedbackViewController.h"
#import "DMDFeedbackObj.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MFSideMenu.h"
#import "constants.h"
#import "DMDAppDelegate.h"

//#define FEEDBACK_LIST_LINK [NSString stringWithFormat:@"%@/api/v1/retrieve/feedbacks",LIVE_DWM_SERVER]

#define FEEDBACK_LIST_LINK [NSString stringWithFormat:@"%@/api/retrieve/feedbacks",LIVE_DWM_SERVER]

@interface DMDFeedbackViewController ()

@end

@implementation DMDFeedbackViewController
@synthesize feedbackArray;
@synthesize feedbackTableView;
@synthesize feedbackView,feedbackTextView,feedbackuserImageView,feedbackusernameLabel,closeBtn;
@synthesize spinnerIndicatior;
@synthesize dataFilePath;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    closeBtn.frame = CGRectMake(feedbackView.frame.origin.x+feedbackView.frame.size.width-25, feedbackView.frame.origin.y-25, closeBtn.frame.size.width, closeBtn.frame.size.height);
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Flurry logEvent:@"Tab_FeedBack"];

    UIBarButtonItem *shareButton = [[UIBarButtonItem alloc] initWithTitle:@"Send Feedback" style:UIBarButtonItemStylePlain target:self action:@selector(send_Clicked:)];
    self.navigationItem.leftBarButtonItem = shareButton;
    
    feedbackView.layer.cornerRadius = 20;
    feedbackView.layer.masksToBounds = YES;
    feedbackView.layer.borderColor = [UIColor whiteColor].CGColor;
    feedbackView.layer.borderWidth = 4.0;
    
    feedbackuserImageView.layer.cornerRadius = 10;
    feedbackuserImageView.layer.masksToBounds = YES;
    
    closeBtn.layer.cornerRadius = 25;
    closeBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    closeBtn.layer.borderWidth = 3.0;
    
    feedbackusernameLabel.font = KABEL_FONT(30);
    feedbackTextView.font = KABEL_FONT(20);
    
    feedbackView.backgroundColor = [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:0.9];
    [feedbackView setHidden:YES];
    [closeBtn setHidden:YES];
    
    feedbackArray = [[NSMutableArray alloc] init];
    
    [self setfeedDataFilePath];
    
    [self setupMenuBarButtonItems];
    
    if([DMDDelegate networkavailable])
    {
        [self feedbackListAsynchronousCall];
    } else {
        [self parseFromFile];
    }

}

- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)send_Clicked:(id)sender {
    @try{
        MFMailComposeViewController *picker=[[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate=self;
        NSString *msgBody = [NSString stringWithFormat:@""];
        picker.navigationBar.tintColor = [UIColor blackColor];
        [picker setToRecipients:[NSArray arrayWithObject:@"feedback@rnmmovingpictures.com"]];
        [picker setSubject:@""];
        [picker setMessageBody:msgBody isHTML:NO];
        [self presentModalViewController:picker animated:YES];
    }
    @catch (NSException *exception)
    {
        //DebugLog(@"Exception %@",exception.reason);
    }
}

-(void)setfeedDataFilePath
{
    // Get the documents directory
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    // Build the path to the data file
    dataFilePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"feedback.archive"]];
}

-(void)parseFromFile
{
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    // Check if the file already exists
    if ([filemgr fileExistsAtPath: dataFilePath])
    {
        feedbackArray = [NSKeyedUnarchiver unarchiveObjectWithFile: dataFilePath];
        DebugLog(@"from file feedbackArray %@",feedbackArray);
        
        if (feedbackArray.count <= 0) {
            [self showError];
        }else{
            [spinnerIndicatior stopAnimating];
            [feedbackTableView reloadData];
        }
    }else
    {
        [self showError];
    }
}
-(void)showError
{
    [spinnerIndicatior stopAnimating];
    UIAlertView *errorView = [[UIAlertView alloc]
                              initWithTitle:@"No Network Connection"
                              message:@"Please check your internet connection and try again."
                              delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
    [errorView show];
}


#pragma mark mailComposeController Delegate Methods

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result == MFMailComposeResultSent)
    {
        //NSLog(@"Mail sent");
    }
    [controller dismissModalViewControllerAnimated:YES];
}

-(void)feedbackListAsynchronousCall
{
    [spinnerIndicatior startAnimating];
	/****************Asynchronous Request**********************/
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:FEEDBACK_LIST_LINK] cachePolicy:NO timeoutInterval:10.0];
    //NSLog(@"%@",urlRequest.URL);
    //Get list of all songs
    connection1 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //NSLog(@"Error: %@", [error localizedDescription]);
    [spinnerIndicatior stopAnimating];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [spinnerIndicatior stopAnimating];
    NSError* error;
    if(connection==connection1)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            if(json != nil) {
                 NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    [feedbackArray removeAllObjects];
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* feedbackattributeDict in dataArray)
                    {
                        feedbackObj = [[DMDFeedbackObj alloc] init];
                        feedbackObj.userId = [feedbackattributeDict objectForKey:@"User_ID"];
                        feedbackObj.userName = [feedbackattributeDict objectForKey:@"User_Name"];
                        NSString * picUrl = [feedbackattributeDict objectForKey:@"Picture"];
                        if([picUrl hasPrefix:@"images/"])
                        {
                            //NSLog(@"%@",[NSString stringWithFormat:@"http://dancewithmadhuri.com/%@",picUrl]);
                            feedbackObj.picture = [NSString stringWithFormat:@"http://dancewithmadhuri.com/%@",picUrl];
                        } else {
                            feedbackObj.picture = picUrl;
                        }
                        feedbackObj.feedbackText = [feedbackattributeDict objectForKey:@"Feedback_Txt"];
                        feedbackObj.name = [feedbackattributeDict objectForKey:@"Name"];
                        ////NSLog(@"\n songid: %@ , name: %@  , clipid: %@  , sequence: %@  , shareid: %@ \n", songDetailObj.songId, songDetailObj.songName, songDetailObj.songClip_Part_ID, songDetailObj.songSequence, songDetailObj.songShare_ID);
                        [feedbackArray addObject:feedbackObj];
                        feedbackObj = nil;
                    }
                }
            }
        }
        responseAsyncData = nil;
        ////NSLog(@"detail array count: %d", [songDetailArray count]);
        [NSKeyedArchiver archiveRootObject:feedbackArray toFile:dataFilePath];
        [feedbackTableView reloadData];
    }
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [feedbackArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UILabel *lblName;
    UILabel *lblMessage;
    UIView *contentView;

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.userInteractionEnabled = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        cell.textLabel.textColor = [UIColor whiteColor];
//        cell.textLabel.font = [UIFont systemFontOfSize:28.0];
//        cell.detailTextLabel.textColor = [UIColor lightGrayColor];
//        cell.detailTextLabel.font = [UIFont systemFontOfSize:24.0];
//        cell.detailTextLabel.numberOfLines = 5;
        
        contentView = [[UIView alloc] initWithFrame:CGRectMake(20, 10, 980, 130)];
        contentView.backgroundColor = [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:0.5];
        contentView.tag = 11;
        contentView.layer.cornerRadius = 10;
        contentView.layer.masksToBounds = YES;
        
        //Initialize Image View with tag 1.(Thumbnail Image)
        UIImageView *thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(30, 15, 100, 100)];
        thumbImg.tag = 1;
        thumbImg.contentMode = UIViewContentModeScaleToFill;
        thumbImg.layer.masksToBounds = YES;
        thumbImg.layer.cornerRadius = 25;
        thumbImg.layer.borderColor = [UIColor darkGrayColor].CGColor;
        thumbImg.layer.borderWidth = 4;
        [contentView addSubview:thumbImg];
        
        
        //Initialize Label with tag 2.(Title Label)
        lblName = [[UILabel alloc] initWithFrame:CGRectMake(150.0,10,500.0,50.0)];
        lblName.tag = 2;
//        lblName.shadowColor   = [UIColor blackColor];
//        lblName.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblName.font = KABEL_FONT(30);//[UIFont fontWithName:@"Cochin-Bold" size:24];
        lblName.textAlignment = UITextAlignmentLeft;
        lblName.textColor = [UIColor whiteColor];
        lblName.backgroundColor =  [UIColor clearColor];
        [contentView addSubview:lblName];
        
        //Initialize Label with tag 2.(Title Label)
        lblMessage = [[UILabel alloc] initWithFrame:CGRectMake(150.0,65.0,800.0,50.0)];
        lblMessage.tag = 3;
        lblMessage.numberOfLines = 2;
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblTitle.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblMessage.adjustsFontSizeToFitWidth = TRUE;
        lblMessage.minimumFontSize = 15.0;
        lblMessage.font = KABEL_FONT(20);//[UIFont fontWithName:@"Cochin-Bold" size:20];
        lblMessage.textAlignment = UITextAlignmentLeft;
        lblMessage.textColor = [UIColor lightGrayColor];
        lblMessage.backgroundColor =  [UIColor clearColor];
        [contentView addSubview:lblMessage];
        
        [cell.contentView addSubview:contentView];

        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:0];
        cell.selectedBackgroundView = bgColorView;
    }
    
    cell.userInteractionEnabled = YES;
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    contentView = (UIView *)[cell viewWithTag:11];
    DMDFeedbackObj *tempfeedObj  = [feedbackArray objectAtIndex:indexPath.row];
    lblName = (UILabel *)[contentView viewWithTag:2];
    lblMessage = (UILabel *)[contentView viewWithTag:3];
    
    if(![tempfeedObj.userName isEqualToString:@""])
        lblName.text = tempfeedObj.userName;
    else
        lblName.text = tempfeedObj.name;

    lblMessage.text = tempfeedObj.feedbackText;
//    [cell.imageView setImageWithURL:[NSURL URLWithString:tempfeedObj.picture]
//                   placeholderImage:[UIImage imageNamed:@"favicon.png"]
//                            success:^(UIImage *image) {
//                                //DebugLog(@"success");
//                            }
//                            failure:^(NSError *error) {
//                                //DebugLog(@"write error %@", error);
//                            }];
    
    UIImageView *thumbImgview = (UIImageView *)[contentView viewWithTag:1];
    [thumbImgview setImageWithURL:[NSURL URLWithString:tempfeedObj.picture]
                 placeholderImage:[UIImage imageNamed:@"unknown.jpg"]
                          success:^(UIImage *image) {
                              //DebugLog(@"success");
                          }
                          failure:^(NSError *error) {
                              //DebugLog(@"write error %@", error);
                          }];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [feedbackView setHidden:NO];
    [closeBtn setHidden:NO];
    tableView.userInteractionEnabled = FALSE;
    DMDFeedbackObj *tempfeedObj   =   (DMDFeedbackObj *)[feedbackArray objectAtIndex:indexPath.row];
    [feedbackuserImageView setImageWithURL:[NSURL URLWithString:tempfeedObj.picture]
                 placeholderImage:[UIImage imageNamed:@"unknown.jpg"]
                          success:^(UIImage *image) {
                              //DebugLog(@"success");
                          }
                          failure:^(NSError *error) {
                              //DebugLog(@"write error %@", error);
                          }];
    if(![tempfeedObj.userName isEqualToString:@""])
        feedbackusernameLabel.text = tempfeedObj.userName;
    else
        feedbackusernameLabel.text = tempfeedObj.name;
    
    feedbackTextView.text    =   tempfeedObj.feedbackText;
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
}

- (IBAction)closeBtnClicked:(id)sender {
    [feedbackView setHidden:YES];
    [closeBtn setHidden:YES];
    feedbackTableView.userInteractionEnabled = TRUE;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setFeedbackTableView:nil];
    [self setCloseBtn:nil];
    [self setFeedbackView:nil];
    [self setFeedbackTextView:nil];
    [self setFeedbackuserImageView:nil];
    [self setFeedbackusernameLabel:nil];
    [self setSpinnerIndicatior:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}
@end
