//
//  DMDMyProfileViewController.h
//  DancewithMD
//
//  Created by Rishi on 20/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMDMyBadge;
@interface DMDMyProfileViewController : UIViewController {
    int status;
    NSMutableData *responseAsyncData;
    NSURLConnection *connection1;
    NSURLConnection *connection2;
    NSURLConnection *connection3;
    DMDMyBadge *mybadgeObj;
}

@property (strong, nonatomic) IBOutlet UILabel *badgescountlbl;
@property (strong, nonatomic) IBOutlet UILabel *specialbadgescountlbl;
@property (strong, nonatomic) NSMutableArray *mybadgeArray;
@property (strong, nonatomic) IBOutlet UITableView *mybadgeTableView;
@property (strong, nonatomic) IBOutlet UIImageView *userPicImgView;
@property (strong, nonatomic) IBOutlet UILabel *userNamelbl;
@property (strong, nonatomic) IBOutlet UILabel *welcomelbl;
@property (strong, nonatomic) IBOutlet UIButton *downloadedVideoBtn;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinnerIndicatior;
@property (strong, nonatomic) IBOutlet UILabel *ranklbl;
@property (strong, nonatomic) IBOutlet UILabel *pointslbl;
@property (strong, nonatomic) IBOutlet UIButton *leaderboardBtn;
@property (strong, nonatomic) IBOutlet UILabel *dancerslbl;

- (IBAction)downloadedVideoBtnPressed:(id)sender;
- (IBAction)leaderboardBtnPressed:(id)sender;
@end
