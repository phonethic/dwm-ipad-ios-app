//
//  DMDSongDetailViewController.h
//  DancewithMD
//
//  Created by Rishi on 12/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "iCarousel.h"
#import "FXImageView.h"

@class DMDSongDetailObj;
@class DMDBadgeObj;
@interface DMDSongDetailViewController : UIViewController <iCarouselDataSource, iCarouselDelegate> {
    NSMutableData *responseData;
    NSError  *connectionError;
    NSURLConnection *conn;
    int status;
    float contentSize;
    NSMutableData *responseAsyncData;
    NSMutableData *responseDownloadAsyncData;
    DMDSongDetailObj *songDetailObj;
    DMDBadgeObj *badgeObj;
    NSURLConnection *connection1;
    NSURLConnection *connection2;
    NSURLConnection *connection3;
    NSURLConnection *connection4;
    NSURLConnection *connection5;
    NSURLConnection *connection6;
    NSURLConnection *connection7;
    int small;
    int close;
    int fullscreen;
}
@property (nonatomic, strong) IBOutlet iCarousel *carousel;
@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;
@property (nonatomic, copy) NSString *songID;
@property (nonatomic, copy) NSString *seenCount;
@property (nonatomic, copy) NSString *selectedsongName;
@property (nonatomic, readwrite) int selectedsongindex;
@property (nonatomic, copy) NSString *selectedsongclipID;
@property (strong, nonatomic) IBOutlet UITableView *songdetailTableView;
@property (strong, nonatomic) NSMutableArray *songDetailArray;
@property (strong, nonatomic) NSMutableArray *badgeArray;
//Badge View
@property (strong, nonatomic) IBOutlet UIView *badgeView;
@property (strong, nonatomic) IBOutlet UIImageView *badgeImageView;
@property (strong, nonatomic) IBOutlet UILabel *badgelbl;
@property (strong, nonatomic) IBOutlet UIButton *downloadBtn;
@property (strong, nonatomic) IBOutlet UILabel *titlelbl;
@property (strong, nonatomic) IBOutlet UILabel *namelbl;
@property (strong, nonatomic) IBOutlet UIProgressView *progressBar;
@property (strong, nonatomic) IBOutlet UIView *videoView;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (strong, nonatomic) IBOutlet UIImageView *lightspotImageView;
@property (strong, nonatomic) IBOutlet UIImageView *reflectionView;
@property (strong, nonatomic) IBOutlet UIImageView *videoboxImageView;
@property (strong, nonatomic) IBOutlet UIImageView *screenshotView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinnerIndicatior;
@property (strong, nonatomic) IBOutlet UIButton *shareBtn;

- (IBAction)closevideoBtnPressed:(id)sender;
- (IBAction)fbshareBtnPressed:(id)sender;
- (IBAction)fbsharecloseBtnPressed:(id)sender;
- (IBAction)downloadBtnPressed:(id)sender;
- (IBAction)resizeCarousal:(id)sender;

-(void)setFullscreenOffShareFB;

@end
