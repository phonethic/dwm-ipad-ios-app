    //
//  main.m
//  DancewithMD
//
//  Created by Rishi on 06/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DMDAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DMDAppDelegate class]));
    }
}
