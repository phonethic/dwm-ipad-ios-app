//
//  DMDSubmittedVideosObj.h
//  DancewithMD
//
//  Created by Rishi on 26/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DMDSubmittedVideosObj : NSObject {
    
}
@property (nonatomic, copy) NSString *videoId;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *userPic;
@property (nonatomic, copy) NSString *songId;
@property (nonatomic, copy) NSString *videoLink;
@property (nonatomic, copy) NSString *votes;

@end
