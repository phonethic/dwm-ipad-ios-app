//
//  DMDExpertsViewController.m
//  DancewithMD
//
//  Created by Rishi on 19/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDExpertsViewController.h"
#import "MFSideMenu.h"
#import "DMDAppDelegate.h"
#import "DMDExpertsObj.h"
#import <SDWebImage/UIImageView+WebCache.h>

//#define GURU_VIDEO_LINK [NSString stringWithFormat:@"%@/api/v1/retrieve/guruspeak",LIVE_DWM_SERVER]
#define GURU_VIDEO_LINK [NSString stringWithFormat:@"%@/api/retrieve/guruspeak",LIVE_DWM_SERVER]


@interface DMDExpertsViewController ()

@end

@implementation DMDExpertsViewController
@synthesize carousel;
@synthesize moviePlayer;
@synthesize videoDetailArray;
@synthesize backgroundImageView,lightspotImageView;
@synthesize videoView,screenshotView,videoboxImageView,titlelbl,namelbl,reflectionView;
@synthesize descriptionView,descriptionImageView,descriptionTextView;
@synthesize spinnerIndicatior;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if(self.moviePlayer != nil && !fullscreen) {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
    if(videowebView != nil && self.navigationController.viewControllers == nil){
        [videowebView removeFromSuperview];
        videowebView = nil;
    }
    //NSLog(@"--%@--",self.navigationController.viewControllers);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Flurry logEvent:@"Tab_Star_Speaks"];
    fullscreen = 0;
    carousel.type = iCarouselTypeCoverFlow;
    titlelbl.text = @"";
    namelbl.text = @"";
    titlelbl.font = KABEL_FONT(15);
    namelbl.font = KABEL_FONT(18);
    titlelbl.textColor = GRAY_TEXT_COLOR;
    namelbl.textColor = GRAY_TEXT_COLOR;
    videoDetailArray = [[NSMutableArray alloc] init];
    //[videoDetailArray addObject:@"qAweN181vns.jpg"];
    //[videoDetailArray addObject:@"nHDHr3qw6RM.jpg"];
    //[carousel reloadData];
    if([DMDDelegate networkavailable])
    {
         [self guruvideoListAsynchronousCall];
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
    }
   
    [self setupMenuBarButtonItems];
    [videoView setHidden:TRUE];
    small = 0;
    
    descriptionView.backgroundColor = [UIColor clearColor];
    descriptionTextView.backgroundColor = [UIColor clearColor];
    descriptionImageView.image = [UIImage imageNamed:@"guruspeak_box.png"];
    descriptionTextView.font = KABEL_FONT(18);
    descriptionTextView.textColor = [UIColor whiteColor];
    descriptionTextView.textAlignment = UITextAlignmentLeft;
    descriptionTextView.editable = FALSE;
    
    descriptionView.alpha = 0.0;
}

//-(void)viewDidAppear:(BOOL)animated
//{
//    [super viewDidAppear:animated];
//    [carousel reloadData];
//}
-(void)showDescriptionView:(BOOL)show
{
    if (show) {
        [UIView animateWithDuration:0.5 animations:^(void) {
                             [descriptionView setAlpha:1.0];
                         }
                         completion:^(BOOL finished) {
                         }];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^(void) {
                        [descriptionView setAlpha:0.0];
                        }
                        completion:^(BOOL finished) {
                        }];
    }
}
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)guruvideoListAsynchronousCall
{
    [spinnerIndicatior startAnimating];
	/****************Asynchronous Request**********************/
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:GURU_VIDEO_LINK] cachePolicy:NO timeoutInterval:10.0];
    
    //Get list of all songs
    connection1 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}
#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //NSLog(@"Error: %@", [error localizedDescription]);
    [spinnerIndicatior stopAnimating];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [spinnerIndicatior stopAnimating];
    NSError* error;
    if(connection==connection1)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                     NSArray *dataArray = [json objectForKey:@"data"];
                    [videoDetailArray removeAllObjects];
                    for (NSDictionary* videoattributeDict in dataArray)
                    {
                        expvideosObj = [[DMDExpertsObj alloc] init];
                        expvideosObj.clipId = [videoattributeDict objectForKey:@"ID"];
                        expvideosObj.clipTitle = [videoattributeDict objectForKey:@"Title"];
                        expvideosObj.videoText = [videoattributeDict objectForKey:@"Description"];
                        expvideosObj.videoThumb = [videoattributeDict objectForKey:@"Thumbnail"];
                        expvideosObj.videoType = [videoattributeDict objectForKey:@"Type"];
                        expvideosObj.videoLink = [videoattributeDict objectForKey:@"Video_Link"];
                        //NSLog(@"\n videoid: %@ , videotitle: %@ , videotext: %@  , image: %@ , link: %@ \n", expvideosObj.clipId, expvideosObj.clipTitle, expvideosObj.videoText, expvideosObj.videoThumb, expvideosObj.videoLink);
                        [videoDetailArray addObject:expvideosObj];
                        expvideosObj = nil;
                    }
                    if (videoDetailArray.count > 0) {
                        [carousel reloadData];
                        DMDExpertsObj *tempvideoObj = (DMDExpertsObj *)[videoDetailArray objectAtIndex:0];
                        self.descriptionTextView.text = tempvideoObj.videoText;
                        [self showDescriptionView:YES];
                    }
                }
            }
        }
        responseAsyncData = nil;
        connection1 = nil;
    }
}


-(void)playVideoInWebView:(NSString *)link
{
    //NSLog(@"link = %@",link);
    NSString *embedHTML =[NSString stringWithFormat:@"\
                          <html><head>\
                          <style type=\"text/css\">\
                          body {\
                          background-color: transparent;\
                          color: blue;\
                          }\
                          </style>\
                          </head><body style=\"margin:0\">\
                          <iframe width=\"548\" height=\"298\" src=\"%@\" frameborder=\"0\" allowfullscreen></iframe>\
                          </body></html>",link];
    //NSLog(@"%@",embedHTML);
    videowebView = [[UIWebView alloc] initWithFrame:CGRectMake(239, 246, 548, 298)];
    videowebView.backgroundColor = [UIColor clearColor];
    [videowebView loadHTMLString:embedHTML baseURL:nil];
    [videowebView.scrollView setBounces:NO];
    [videowebView.scrollView setScrollEnabled:NO];
    [videowebView setOpaque:NO];
    [videowebView setHidden:TRUE];
    [self.view addSubview:videowebView];

}

-(void)playVideofromUrl:(NSString *)videoUrl
{
    //NSLog(@"%@",videoUrl);
    if(self.moviePlayer != nil) {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }

    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:videoUrl]];
    self.moviePlayer.view.frame = CGRectMake(239, 246, 548, 298);
    [moviePlayer setControlStyle:MPMovieControlStyleDefault];
    self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
    self.moviePlayer.shouldAutoplay = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieEnterFullScreen:)
                                                 name:MPMoviePlayerWillEnterFullscreenNotification
                                               object:self.moviePlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieExitFullScreen:)
                                                 name:MPMoviePlayerDidExitFullscreenNotification
                                               object:self.moviePlayer];
    [self.view addSubview:moviePlayer.view];
    [self.view bringSubviewToFront:moviePlayer.view];
    [moviePlayer.view setHidden:TRUE];
    [moviePlayer prepareToPlay];
    [moviePlayer play];
    
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    [moviePlayer setFullscreen:FALSE animated:TRUE];  
    MPMoviePlayerController *player = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    
    if ([player respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [player.view removeFromSuperview];
    }
    moviePlayer = nil;
}

- (void) movieEnterFullScreen:(NSNotification*)notification {
    fullscreen = 1;
}
- (void) movieExitFullScreen:(NSNotification*)notification {
    fullscreen = 0;
}


#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [videoDetailArray count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        FXImageView *imageView = [[FXImageView alloc] initWithFrame:CGRectMake(0, 0, 300.0f, 300.0f)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        ((UIImageView *)view).image = [UIImage imageNamed:@"carousal_lock.png"];
        imageView.asynchronous = YES;
        imageView.reflectionScale = 0.5f;
        imageView.reflectionAlpha = 0.25f;
        imageView.reflectionGap = 10.0f;
        imageView.shadowOffset = CGSizeMake(0.0f, 2.0f);
        imageView.shadowBlur = 5.0f;
        imageView.cornerRadius = 10.0f;
        view = imageView;
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 170, 300, 50)];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = UITextAlignmentCenter;
        label.textColor = [UIColor lightGrayColor];
        label.font = KABEL_FONT(22);
        label.tag = 1;
        [view addSubview:label];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
    }
    
    ////NSLog(@"image %@ , count %d",[videoDetailArray objectAtIndex:index],[videoDetailArray count]);
    
    
    if(small==1)
    {
        CABasicAnimation *shrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        shrink.fromValue = [NSNumber numberWithDouble:1.0];
        shrink.toValue = [NSNumber numberWithDouble:0.5];
        shrink.duration = 0.6;
        shrink.fillMode=kCAFillModeForwards;
        shrink.removedOnCompletion=NO;
        //shrink.delegate = self;
        [((UIImageView *)view).layer addAnimation:shrink forKey:@"shrink"];
        self.carousel.userInteractionEnabled = FALSE;
    } else if(small==2) {
        CABasicAnimation *unshrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        unshrink.fromValue = [NSNumber numberWithDouble:0.5];
        unshrink.toValue = [NSNumber numberWithDouble:1.0];
        unshrink.duration = 0.6;
        unshrink.fillMode=kCAFillModeForwards;
        unshrink.removedOnCompletion=NO;
        //unshrink.delegate = self;
        [((UIImageView *)view).layer addAnimation:unshrink forKey:@"shrink"];
        self.carousel.userInteractionEnabled = TRUE;
    }
    
    
    DMDExpertsObj *tempvideoObj = (DMDExpertsObj *)[videoDetailArray objectAtIndex:index];
    UIImageView *thumbImgview = ((UIImageView *)view);
    [thumbImgview setImageWithURL:[NSURL URLWithString:[DMDDelegate URLEncode:tempvideoObj.videoThumb]]
                 placeholderImage:[UIImage imageNamed:@"carousal_unlock.png"]
                          success:^(UIImage *image) {
                              //DebugLog(@"success");
                          }
                          failure:^(NSError *error) {
                              //DebugLog(@"write error %@", error);
                          }];
    
    //((UIImageView *)view).image = [UIImage imageNamed:[videoDetailArray objectAtIndex:index]];
    label.text = @"";// tempvideoObj.clipTitle;
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    
    
    
    return view;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    [self showDescriptionView:NO];

    DMDExpertsObj *tempvideoObj = (DMDExpertsObj *)[videoDetailArray objectAtIndex:index];
    titlelbl.text = tempvideoObj.clipTitle;
    [self showVideoView];
    if (small==0 || small==2) {
        small = 1;
    } else if(small==1){
        small = 2;
    }
    
    self.reflectionView.image = [UIImage imageNamed:@"video_box_transparent.png"];
    
    [videoView setHidden:FALSE];
    screenshotView.hidden = FALSE;
    
    UIGraphicsBeginImageContext(videoboxImageView.frame.size);
    [videoView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    screenshotView.image = viewImage;
    UIGraphicsEndImageContext();
    //UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
    
    
    // determine the size of the reflection to create
    NSUInteger reflectionHeight = self.videoboxImageView.bounds.size.height * kDefaultReflectionFraction;
    
    // create the reflection image and assign it to the UIImageView
    self.reflectionView.image = [DMDDelegate reflectedImage:screenshotView withHeight:reflectionHeight];
    //self.reflectionView.image = [self reflectedImage:screenshotView withHeight:reflectionHeight];
    self.reflectionView.alpha = kDefaultReflectionOpacity;
    
    screenshotView.image = [UIImage imageNamed:@"video_box_transparent.png"];
    screenshotView.hidden = TRUE;
    
    
    [videoView setHidden:FALSE];
    
    [self.carousel reloadData];
    
    //[self playVideofromUrl:tempvideoObj.videoLink];
    if([tempvideoObj.videoType isEqualToString:@"embedd"])
    {
        [self playVideoInWebView:tempvideoObj.videoLink];
    } else {
        [self playVideofromUrl:[DMDDelegate URLEncode:tempvideoObj.videoLink]];
    }

}

- (CGFloat)carousel:(iCarousel *)_carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionFadeMin:
        {
            return -0.2;
        }
        case iCarouselOptionFadeMax:
        {
            return 0.2;
        }
        case iCarouselOptionFadeRange:
        {
            return 3.0;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return 0.599131;
        }
        case iCarouselOptionTilt:
        {
            return 0.495158;
        }
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return NO;
        }
        default:
        {
            return value;
        }
    }
}
- (void)carouselWillBeginDragging:(iCarousel *)lcarousel
{
    //NSLog(@"carouselWillBeginDragging %d",lcarousel.currentItemIndex);
    [self showDescriptionView:NO];
}
- (void)carouselCurrentItemIndexDidChange:(iCarousel *)lcarousel{
    //NSLog(@"carouselCurrentItemIndexDidChange %d",lcarousel.currentItemIndex);
    DMDExpertsObj *tempvideoObj = (DMDExpertsObj *)[videoDetailArray objectAtIndex:lcarousel.currentItemIndex];
    self.descriptionTextView.text = tempvideoObj.videoText;
    [self showDescriptionView:YES];
}
- (void)carouselDidEndDragging:(iCarousel *)lcarousel willDecelerate:(BOOL)decelerate
{
    //NSLog(@"carouselDidEndDragging %d",lcarousel.currentItemIndex);
    [self showDescriptionView:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setCarousel:nil];
    [self setBackgroundImageView:nil];
    [self setScreenshotView:nil];
    [self setVideoboxImageView:nil];
    [self setTitlelbl:nil];
    [self setNamelbl:nil];
    [self setReflectionView:nil];
    [self setLightspotImageView:nil];
    [self setVideoView:nil];
    [self setBackgroundImageView:nil];
    [self setDescriptionTextView:nil];
    [self setDescriptionImageView:nil];
    [self setDescriptionView:nil];
    [self setSpinnerIndicatior:nil];
    [super viewDidUnload];
}
- (IBAction)closevideoBtnPressed:(id)sender {
    if(self.moviePlayer != nil) {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
    [videowebView removeFromSuperview];
    videowebView = nil;
    [self hideVideoView];
    [self showDescriptionView:YES];
}

-(void)showVideoView
{
    backgroundImageView.image = [UIImage imageNamed:@"BG_zoomed_out.jpg"];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.8f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    
    [backgroundImageView.layer addAnimation:transition forKey:nil];
    
    [videoView setHidden:TRUE];
    [videoView  setAlpha:0.0];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [lightspotImageView setAlpha:0.0];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.4];
    [videoView setAlpha:1.0];
    [UIView commitAnimations];
    
    CABasicAnimation *shrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    shrink.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    shrink.fromValue = [NSNumber numberWithDouble:1.5];
    shrink.toValue = [NSNumber numberWithDouble:1.0];
    shrink.duration = 0.8;
    shrink.fillMode=kCAFillModeForwards;
    shrink.removedOnCompletion=NO;
    shrink.delegate = self;
    [CATransaction setCompletionBlock:^{
        [videowebView setHidden:FALSE];
        [self.view bringSubviewToFront:videowebView];
        if(moviePlayer != nil) {
            [moviePlayer.view setHidden:FALSE];
        }
    }];
    [videoView.layer addAnimation:shrink forKey:@"shrink"];
}

-(void)hideVideoView {
    backgroundImageView.image = [UIImage imageNamed:@"BG_default.jpg"];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.8f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [backgroundImageView.layer addAnimation:transition forKey:nil];
    
    CABasicAnimation *unshrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    unshrink.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    unshrink.fromValue = [NSNumber numberWithDouble:1.0];
    unshrink.toValue = [NSNumber numberWithDouble:1.5];
    unshrink.duration = 0.8;
    unshrink.fillMode = kCAFillModeForwards;
    unshrink.removedOnCompletion = NO;
    [videoView.layer addAnimation:unshrink forKey:@"shrink"];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.6];
    [videoView setAlpha:0.0];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [lightspotImageView setAlpha:1.0];
    [UIView commitAnimations];
    
    small = 2;
    [carousel reloadData];
    //[videoView setHidden:TRUE];
}

//- (void)animationDidStart:(CAAnimation *)anim
//{
//    //NSLog(@"animation start");
//}
//- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag
//{
//    //NSLog(@"animation stopped");
//}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}
@end
