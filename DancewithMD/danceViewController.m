//
//  danceViewController.m
//  dancefloor1
//
//  Created by Sagar Mody on 27/04/13.
//  Copyright (c) 2013 phonethics. All rights reserved.
//

#import "danceViewController.h"
#import "MFSideMenu.h"
#import "Flurry.h"

@interface danceViewController ()

@end

@implementation danceViewController
//static int counter=0;
@synthesize videoPlayerArray;
@synthesize pathArray;

- (void)viewDidLoad
{
    [super viewDidLoad];
    //NSLog(@"----viewDidLoad----");
	// Do any additional setup after loading the view, typically from a nib.
    [Flurry logEvent:@"Tab_DanceFloor"];

    [self setupMenuBarButtonItems];
    [self.view setMultipleTouchEnabled:YES];
    
    videoPlayerArray = [[NSMutableArray alloc] init];
    pathArray = [[NSMutableArray alloc] init];
    
    for (int i=1; i<=35; i++) {
        NSError *error;
        NSString *path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%d",i] ofType:@"wav"];
        NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];
        ////NSLog(@"path=%@", [filePath path]);
        AVAudioPlayer *audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:filePath error:&error];
        audioPlayer.delegate = self;
        ////NSLog(@"url=%@", audioPlayer.url);
        if (error)
        {
            NSLog(@"Error: %@", [error localizedDescription]);
        } else {
            [videoPlayerArray addObject:audioPlayer];
        }
    }
    ////NSLog(@"count=%d", [videoPlayerArray count]);
    [self randomOnOffBtnPressed:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
     //NSLog(@"----viewWillDisappear----");
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    //NSLog(@"----viewDidDisappear----");
    if ( [_glowTimer isValid]) {
        //NSLog(@"invalidate glowtimer");
        [_glowTimer invalidate], _glowTimer=nil;
    }
    for (int i=1; i<=35; i++) {
        AVAudioPlayer *audioPlayer = (AVAudioPlayer *)[videoPlayerArray objectAtIndex:i-1];
        if(audioPlayer != nil)
        {
            [audioPlayer stop];
            audioPlayer = nil;
        }
    }
    [self.view.layer removeAllAnimations];
}

-(void)playAudio:(int)playerIndex
{
    //NSLog(@"play id=%d", playerIndex);
    AVAudioPlayer *audioPlayer = (AVAudioPlayer *)[videoPlayerArray objectAtIndex:playerIndex];
    if(audioPlayer != nil)
    {
        [audioPlayer play];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    // Return if there are no subviews
    if ([[self.view subviews] count] == 0) return;
    
    if ( [_glowTimer isValid]) {
        [_glowTimer invalidate], _glowTimer=nil;
    }
    
    UITouch *touch = [touches anyObject];
    
    CGPoint location = [touch locationInView: self.view];
    ////NSLog(@"start %f %f",location.x,location.y);
    
    for (UIView *subview in [self.view subviews])
    {
        if(![subview isKindOfClass:[UIImageView class]])
            return;
        if(CGRectContainsPoint(subview.frame, location) && subview.tag != 1000)
        {
            if(subview.tag < 100)
            {
                subview.tag = subview.tag + 100;
                //NSLog(@"tag==%d",subview.tag);
                [self playAudio:(subview.tag-100) -1];
            }
            ((UIImageView*)subview).image = [UIImage imageNamed:@"tap_light.png"];
            CATransition *transition = [CATransition animation];
            transition.duration = 0.50;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
            transition.type = kCATransitionFade;
            transition.removedOnCompletion = YES;
            [subview.layer addAnimation:transition forKey:nil];
        }
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    CGPoint location = [touch locationInView: self.view];
    ////NSLog(@"%f %f",location.x,location.y);
        
    for (UIView *subview in [self.view subviews])
    {
        if(![subview isKindOfClass:[UIImageView class]])
            return;
        if(CGRectContainsPoint(subview.frame, location) && subview.tag != 1000)
        {
           if(subview.tag < 100)
           {
                subview.tag = subview.tag + 100;
                [self playAudio:(subview.tag-100) -1];
           }
            ((UIImageView*)subview).image = [UIImage imageNamed:@"tap_light.png"];
            CATransition *transition = [CATransition animation];
            transition.duration = 0.50;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
            transition.type = kCATransitionFade;
            transition.removedOnCompletion = YES;
            [subview.layer addAnimation:transition forKey:nil];
        } else if(subview.tag != 1000){
            if(subview.tag > 100)
            {
                subview.tag = subview.tag - 100;
            }
             ((UIImageView*)subview).image = [UIImage imageNamed:@"tap_off.png"];
        }
    }
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    // Return if there are no subviews
    if ([[self.view subviews] count] == 0) return;
    
    [self performSelector:@selector(randomOnOffBtnPressed:) withObject:nil afterDelay:5.0];
    
    UITouch *touch = [touches anyObject];
    
    CGPoint location = [touch locationInView: self.view];
    ////NSLog(@"stop %f %f",location.x,location.y);
    
    for (UIView *subview in [self.view subviews])
    {
        if(![subview isKindOfClass:[UIImageView class]])
            return;
        if(CGRectContainsPoint(subview.frame, location) && subview.tag != 1000)
        {
            ((UIImageView*)subview).image = [UIImage imageNamed:@"tap_off.png"];
            CATransition *transition = [CATransition animation];
            transition.duration = 0.50;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
            transition.type = kCATransitionFade;
            transition.removedOnCompletion = YES;
            [subview.layer addAnimation:transition forKey:nil];
            if(subview.tag > 100)
            {
                subview.tag = subview.tag - 100;
            }
        } 
    }
}

- (void)viewDidUnload {
    [self setPadOneImgView:nil];
    [self setPadTwoImgView:nil];
    [self setPadThreeImgView:nil];
    [self setPadFourImgView:nil];
    [self setPadFiveImgView:nil];
    [self setPadSixImgView:nil];
    [self setPadSevenImgView:nil];
    [self setPadEightImgView:nil];
    [self setPadNineImgView:nil];
    [self setPadTenImgView:nil];
    [self setPadElevenImgView:nil];
    [self setPadTwelveImgView:nil];
    [self setPadThirteenImgView:nil];
    [self setPadFourteenImgView:nil];
    [self setPadFifttenImgView:nil];
    [self setPadSixteenImgView:nil];
    [self setPadSeventeenImgView:nil];
    [self setPadEighteenImgView:nil];
    [self setPadNineteenImgView:nil];
    [self setPadTwentyImgView:nil];
    [self setPadTwentyOneImgView:nil];
    [self setPadTwentyTwoImgView:nil];
    [self setPadTwentyThreeImgView:nil];
    [self setPadTwentyThreeImgView:nil];
    [self setPadTwentyFourImgView:nil];
    [self setPadTwentyFiveImgView:nil];
    [self setPadTwentysixImgView:nil];
    [self setPadTwentySevenImgView:nil];
    [self setPadTwentyEightImgView:nil];
    [self setPadTwentyNineImgView:nil];
    [self setPadThirtyImgView:nil];
    [self setPadThirtyOneImgView:nil];
    [self setPadThirtyTwoView:nil];
    [self setPadThirtyThreeImgView:nil];
    [self setPadThirtyFourImgView:nil];
    [self setPadThirtyFiveImgView:nil];
    [super viewDidUnload];
}

- (void)randomOnOffBtnPressed:(id)sender {
    if (![_glowTimer isValid])  {
            _glowTimer = [NSTimer scheduledTimerWithTimeInterval:0.8 target:self
                                                      selector: @selector(glowTapFloor) userInfo:nil repeats:YES];
    }
}

-(void)glowTapFloor
{
    int tag = (arc4random() % 34) + 1;
    ////NSLog(@"%d",tag);
    if(self.isViewLoaded && self.view.window)
    {
        [self playAudio:tag];
    }
    [self performSelector:@selector(setTapOn:) withObject:[NSNumber numberWithInt:tag] afterDelay:0.0];
    [self performSelector:@selector(setTapOff:) withObject:[NSNumber numberWithInt:tag] afterDelay:1.5];
}

-(void)setTapOn:(NSNumber*)glowViewTag
{
    ////NSLog(@"%d",[glowViewTag intValue]);
    UIImageView *tap = (UIImageView *)[self.view viewWithTag:[glowViewTag intValue]];
    tap.image = [UIImage imageNamed:@"tap_light.png"];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.50;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionFade;
    transition.removedOnCompletion = YES;
    [tap.layer addAnimation:transition forKey:nil];
}

-(void)setTapOff:(NSNumber*)glowViewTag
{
    ////NSLog(@"%d",[glowViewTag intValue]);
    UIImageView *tap = (UIImageView *)[self.view viewWithTag:[glowViewTag intValue]];
    tap.image = [UIImage imageNamed:@"tap_off.png"];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.50;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionFade;
    transition.removedOnCompletion = YES;
    [tap.layer addAnimation:transition forKey:nil];
}

- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}
 
@end
