//
//  DMDSubmissionDetailViewController.m
//  DancewithMD
//
//  Created by Kirti Nikam on 09/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDSubmissionDetailViewController.h"
#import "DMDSubmittedVideosObj.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MFSideMenu.h"
#import "DMDAppDelegate.h"

//#define LATEST_SUBMISSION_LIST_LINK(SONGID) [NSString stringWithFormat:@"%@/api/v1/retrieve/submissions/%@/latest",LIVE_DWM_SERVER,SONGID]
//#define VOTES_SUBMISSION_LIST_LINK(SONGID) [NSString stringWithFormat:@"%@/api/v1/retrieve/submissions/%@/votes",LIVE_DWM_SERVER,SONGID]
//#define VOTE_LINK [NSString stringWithFormat:@"%@/api/v1/retrieve/setvotes",LIVE_DWM_SERVER]

#define LATEST_SUBMISSION_LIST_LINK(SONGID) [NSString stringWithFormat:@"%@/api/retrieve/submissions/%@/latest",LIVE_DWM_SERVER,SONGID]
#define VOTE_LINK [NSString stringWithFormat:@"%@/api/retrieve/setvotes",LIVE_DWM_SERVER]

@interface DMDSubmissionDetailViewController ()

@end

@implementation DMDSubmissionDetailViewController
@synthesize videoArray;
@synthesize selectedvideoId,selectedvideoUserId;
@synthesize moviePlayer;
@synthesize selectedSongId;
@synthesize carousel;
@synthesize backgroundImageView,lightspotImageView;
@synthesize videoView,screenshotView,videoboxImageView,progressBar,titlelbl,namelbl,reflectionView;
@synthesize voteBtn;
@synthesize spinnerIndicatior;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //NSLog(@"viewWillDisappear called");
    if(self.moviePlayer != nil && !fullscreen) {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    fullscreen = 0;
    carousel.type = iCarouselTypeCoverFlow;
    //NSLog(@"selected song id %@",selectedSongId);
    titlelbl.text = @"";
    namelbl.text = @"";
    titlelbl.font = KABEL_FONT(30);
    namelbl.font = KABEL_FONT(18);
    titlelbl.textColor = GRAY_TEXT_COLOR;
    namelbl.textColor = GRAY_TEXT_COLOR;
    selectedvideoId = @"";
    selectedvideoUserId = @"";
    videoArray = [[NSMutableArray alloc] init];
    //[self setupMenuBarButtonItems];
    
    if([DMDDelegate networkavailable])
    {
        [self videoListAsynchronousCall:selectedSongId];
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        
    }
   
    [videoView setHidden:TRUE];
    small = 0;
    [voteBtn setHighlighted:FALSE];
    [voteBtn setUserInteractionEnabled:TRUE];
    
//    NSArray *animationArray=[NSArray arrayWithObjects:
//                             [UIImage imageNamed:@"stars1.png"],
//                             [UIImage imageNamed:@"stars1.png"],
//                             [UIImage imageNamed:@"stars3.png"],
//                             nil];
//    animationView=[[UIImageView alloc]initWithFrame:CGRectMake(312, 595, 399, 56)];
//    animationView.backgroundColor=[UIColor clearColor];
//    animationView.animationImages=animationArray;
//    animationView.animationDuration=0.5;
//    animationView.animationRepeatCount=0;
//    [animationView startAnimating];
//    [self.view insertSubview:animationView belowSubview:carousel];
}

- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewDidUnload {
    [self setVoteBtn:nil];
    [self setCarousel:nil];
    [self setVideoView:nil];
    [self setLightspotImageView:nil];
    [self setBackgroundImageView:nil];
    [self setScreenshotView:nil];
    [self setVideoboxImageView:nil];
    [self setProgressBar:nil];
    [self setTitlelbl:nil];
    [self setNamelbl:nil];
    [self setReflectionView:nil];
    [self setSpinnerIndicatior:nil];
    [super viewDidUnload];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}

-(void)videoListAsynchronousCall:(NSString *)songID
{
    [spinnerIndicatior startAnimating];
	/****************Asynchronous Request**********************/
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:LATEST_SUBMISSION_LIST_LINK(songID)] cachePolicy:NO timeoutInterval:10.0];
    //NSLog(@"%@",urlRequest.URL);
    //Get list of all songs
    connection1 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}
#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //NSLog(@"Error: %@", [error localizedDescription]);
    [spinnerIndicatior stopAnimating];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [spinnerIndicatior stopAnimating];
    NSError* error;
    if(connection==connection1)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    [videoArray removeAllObjects];
                     NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* videoattributeDict in dataArray)
                    {
                        if(![[videoattributeDict valueForKey:@"data"] isEqualToString:@"0"])
                        {   videosObj = [[DMDSubmittedVideosObj alloc] init];
                            videosObj.videoId = [videoattributeDict objectForKey:@"Video_ID"];
                            videosObj.userId = [videoattributeDict objectForKey:@"User_ID"];
                            videosObj.userName = [videoattributeDict objectForKey:@"User_Name"];
                            videosObj.userPic = [videoattributeDict objectForKey:@"Picture"];
                            videosObj.songId = [videoattributeDict objectForKey:@"Song_ID"];
                            videosObj.videoLink =  [videoattributeDict objectForKey:@"Mp4_Link"];
                            videosObj.votes = [videoattributeDict objectForKey:@"Votes"];
                            ////NSLog(@"\n videoid: %@ , userid: %@ , username: %@  , image: %@  , songid: %@  , link: %@ , votes: %@ \n", videosObj.videoId, videosObj.userId, videosObj.userName, videosObj.userPic, videosObj.songId, videosObj.videoLink, videosObj.votes);
                            [videoArray addObject:videosObj];
                            videosObj = nil;
                        }
                    }
                }
                [carousel reloadData];
            }
        }
        responseAsyncData = nil;
        connection1 = nil;
    } else  if(connection==connection2)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                NSString *message;
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    if([json objectForKey:@"data"] != nil) {
                         message = [json objectForKey:@"data"];
                    }
                } else {
                     message = [json objectForKey:@"message"];
                }
                if(message != nil && ([message hasPrefix:@"Thank you for voting"] || [message hasPrefix:@"You already voted"]))
                {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                        message:message
                                                                       delegate:nil
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                    [alertView show];
                    [voteBtn setHighlighted:TRUE];
                    [voteBtn setUserInteractionEnabled:FALSE];
                }
            }
        }
        responseAsyncData = nil;
        connection2 = nil;
    }
}

-(void)playVideo:(NSString *)videoUrl
{
    
    if(self.moviePlayer != nil) {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:videoUrl]];
    self.moviePlayer.view.frame = CGRectMake(239, 246, 548, 298);
    [moviePlayer setControlStyle:MPMovieControlStyleDefault];
    self.moviePlayer.scalingMode = MPMovieScalingModeFill;
    self.moviePlayer.shouldAutoplay = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayEnterFullScreen:)
                                                 name:MPMoviePlayerWillEnterFullscreenNotification
                                               object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayExitFullScreen:)
                                                 name:MPMoviePlayerDidExitFullscreenNotification
                                               object:self.moviePlayer];
    [self.view addSubview:moviePlayer.view];
    [self.view bringSubviewToFront:moviePlayer.view];
    [moviePlayer.view setHidden:TRUE];
    [moviePlayer prepareToPlay];
    [moviePlayer play];
    
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    [moviePlayer setFullscreen:FALSE animated:TRUE];
    MPMoviePlayerController *player = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    if ([player respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [player.view removeFromSuperview];
    }
    moviePlayer = nil;
    [self hideVideoView];
}

- (void) moviePlayEnterFullScreen:(NSNotification*)notification {
    //NSLog(@"moviePlayEnterFullScreen");
    fullscreen = 1;
    self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
    
}

- (void) moviePlayExitFullScreen:(NSNotification*)notification {
    //NSLog(@"moviePlayExitFullScreen");
    fullscreen = 0;
    self.moviePlayer.scalingMode = MPMovieScalingModeFill;
}

- (IBAction)voteBtnPressed:(id)sender {
    if([selectedvideoId isEqualToString:@""] || [selectedvideoId isEqualToString:@""])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                            message:@"Please choose a video to vote."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    } else {
        //NSLog(@"-%@-%@-",selectedvideoId,selectedvideoUserId);
        [self VoteSetAsynchronousCall:selectedvideoId videouserID:selectedvideoId];
    }
}

- (IBAction)closevideoBtnPressed:(id)sender {
    if(self.moviePlayer != nil) {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
    [self hideVideoView];
}


-(void)VoteSetAsynchronousCall:(NSString *)videoId videouserID:(NSString *)vuserId
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:@"userId"];
   
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:VOTE_LINK] cachePolicy:NO timeoutInterval:30.0];
    //NSLog(@"URL=%@",urlRequest.URL);
    NSMutableDictionary *postReq = [[NSMutableDictionary alloc] init];
    [postReq setObject:userID forKey:@"user_id"]; 
    [postReq setObject:videoId forKey:@"video_id"];   //Video_ID
    [postReq setObject:vuserId forKey:@"video_user"]; //User_ID
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReq
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (!jsonData) {
        //NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //NSLog(@"---%@---",jsonString);
    }
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody: jsonData];
    connection2 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}


#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [videoArray count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    UILabel *nameLabel = nil;
    UILabel *votesLabel = nil;

    //create new view if no view is available for recycling
    if (view == nil)
    {
        FXImageView *imageView = [[FXImageView alloc] initWithFrame:CGRectMake(0, 0, 300.0f, 300.0f)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        ((UIImageView *)view).image = [UIImage imageNamed:@"users_video_icon.png"];
        imageView.asynchronous = YES;
        imageView.reflectionScale = 0.5f;
        imageView.reflectionAlpha = 0.25f;
        imageView.reflectionGap = 10.0f;
        imageView.shadowOffset = CGSizeMake(0.0f, 2.0f);
        imageView.shadowBlur = 5.0f;
        imageView.cornerRadius = 10.0f;
        view = imageView;
        
        nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 168, 190, 50)];
        nameLabel.backgroundColor = [UIColor clearColor];
        nameLabel.textAlignment = UITextAlignmentCenter;
        nameLabel.textColor = GRAY_TEXT_COLOR;
        nameLabel.font = KABEL_FONT(22);
        nameLabel.tag = 1;
        [view addSubview:nameLabel];
        
        votesLabel = [[UILabel alloc] initWithFrame:CGRectMake(205, 168, 53, 50)];
        votesLabel.backgroundColor = [UIColor clearColor];
        votesLabel.textAlignment = UITextAlignmentRight;
        votesLabel.textColor = GRAY_TEXT_COLOR;
        votesLabel.font = KABEL_FONT(22);
        votesLabel.tag = 2;
        [view addSubview:votesLabel];
    }
    else
    {
        //get a reference to the label in the recycled view
        nameLabel = (UILabel *)[view viewWithTag:1];
        votesLabel = (UILabel *)[view viewWithTag:2];

    }

   ((UIImageView *)view).image = [UIImage imageNamed:@"users_video_icon.png"];
    
    if(small==1)
    {
        CABasicAnimation *shrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        shrink.fromValue = [NSNumber numberWithDouble:1.0];
        shrink.toValue = [NSNumber numberWithDouble:0.5];
        shrink.duration = 0.6;
        shrink.fillMode=kCAFillModeForwards;
        shrink.removedOnCompletion=NO;
        //shrink.delegate = self;
        [((UIImageView *)view).layer addAnimation:shrink forKey:@"shrink"];
        self.carousel.userInteractionEnabled = FALSE;
    } else if(small==2) {
        CABasicAnimation *unshrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        unshrink.fromValue = [NSNumber numberWithDouble:0.5];
        unshrink.toValue = [NSNumber numberWithDouble:1.0];
        unshrink.duration = 0.6;
        unshrink.fillMode=kCAFillModeForwards;
        unshrink.removedOnCompletion=NO;
        //unshrink.delegate = self;
        [((UIImageView *)view).layer addAnimation:unshrink forKey:@"shrink"];
        self.carousel.userInteractionEnabled = TRUE;
    }
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    DMDSubmittedVideosObj *tempvideoObj = (DMDSubmittedVideosObj *)[videoArray objectAtIndex:index];
    nameLabel.text = tempvideoObj.userName;
    votesLabel.text = tempvideoObj.votes;
    return view;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    DMDSubmittedVideosObj *tempvideoObj = (DMDSubmittedVideosObj *)[videoArray objectAtIndex:index];
    //NSLog(@"video url=%@",tempvideoObj.videoLink);
    selectedvideoId = tempvideoObj.videoId;
    selectedvideoUserId = tempvideoObj.userId;
    titlelbl.text = tempvideoObj.userName;
    namelbl.text = [NSString stringWithFormat:@"Votes             %@",tempvideoObj.votes];
    [voteBtn setHighlighted:FALSE];
    [voteBtn setUserInteractionEnabled:TRUE];
    [self showVideoView];
    if (small==0 || small==2) {
        small = 1;
    } else if(small==1){
        small = 2;
    }
    
    self.reflectionView.image = [UIImage imageNamed:@"video_box_transparent.png"];
    
    [videoView setHidden:FALSE];
    screenshotView.hidden = FALSE;
    
    UIGraphicsBeginImageContext(videoboxImageView.frame.size);
    [videoView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    screenshotView.image = viewImage;
    UIGraphicsEndImageContext();
    //UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
    
    
    // determine the size of the reflection to create
    NSUInteger reflectionHeight = self.videoboxImageView.bounds.size.height * kDefaultReflectionFraction;
    
    // create the reflection image and assign it to the UIImageView
    self.reflectionView.image = [DMDDelegate reflectedImage:screenshotView withHeight:reflectionHeight];
    //self.reflectionView.image = [self reflectedImage:screenshotView withHeight:reflectionHeight];
    self.reflectionView.alpha = kDefaultReflectionOpacity;
    
    screenshotView.image = [UIImage imageNamed:@"video_box_transparent.png"];
    screenshotView.hidden = TRUE;

    [videoView setHidden:FALSE];
    
    [self.carousel reloadData];
    
    [self playVideo:tempvideoObj.videoLink];
}
- (CGFloat)carousel:(iCarousel *)_carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionFadeMin:
        {
            return -0.2;
        }
        case iCarouselOptionFadeMax:
        {
            return 0.2;
        }
        case iCarouselOptionFadeRange:
        {
            return 3.0;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return 0.599131;
        }
        case iCarouselOptionTilt:
        {
            return 0.495158;
        }
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return NO;
        }
        default:
        {
            return value;
        }
    }
}

//- (void)carouselDidEndDecelerating:(iCarousel *)carousel
//{
//    //NSLog(@"carouselDidEndDecelerating");
//    if(!animationView.isAnimating)
//        [animationView startAnimating];
//}
//
//- (void)carouselWillBeginDragging:(iCarousel *)carousel
//{
//    //NSLog(@"carouselWillBeginDragging");
//    if(animationView.isAnimating)
//        [animationView stopAnimating];
//}

-(void)showVideoView
{
    backgroundImageView.image = [UIImage imageNamed:@"BG_zoomed_out.jpg"];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.8f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    
    [backgroundImageView.layer addAnimation:transition forKey:nil];
    
    [videoView setHidden:TRUE];
    [videoView  setAlpha:0.0];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [lightspotImageView setAlpha:0.0];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.4];
    [videoView setAlpha:1.0];
    [UIView commitAnimations];
    
    CABasicAnimation *shrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    shrink.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    shrink.fromValue = [NSNumber numberWithDouble:1.5];
    shrink.toValue = [NSNumber numberWithDouble:1.0];
    shrink.duration = 0.8;
    shrink.fillMode=kCAFillModeForwards;
    shrink.removedOnCompletion=NO;
    [CATransaction setCompletionBlock:^{
        [moviePlayer.view setHidden:FALSE];
    }];
    //unshrink.delegate = self;
    [videoView.layer addAnimation:shrink forKey:@"shrink"];
}

-(void)hideVideoView {
    backgroundImageView.image = [UIImage imageNamed:@"BG_default.jpg"];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.8f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [backgroundImageView.layer addAnimation:transition forKey:nil];
    
    CABasicAnimation *unshrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    unshrink.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    unshrink.fromValue = [NSNumber numberWithDouble:1.0];
    unshrink.toValue = [NSNumber numberWithDouble:1.5];
    unshrink.duration = 0.8;
    unshrink.fillMode = kCAFillModeForwards;
    unshrink.removedOnCompletion = NO;
    [videoView.layer addAnimation:unshrink forKey:@"shrink"];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.6];
    [videoView setAlpha:0.0];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [lightspotImageView setAlpha:1.0];
    [UIView commitAnimations];
    
    small = 2;
    [carousel reloadData];
    //[videoView setHidden:TRUE];
}

@end
