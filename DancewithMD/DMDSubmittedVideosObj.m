//
//  DMDSubmittedVideosObj.m
//  DancewithMD
//
//  Created by Rishi on 26/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDSubmittedVideosObj.h"

@implementation DMDSubmittedVideosObj

@synthesize videoId;
@synthesize userId;
@synthesize userName;
@synthesize userPic;
@synthesize songId;
@synthesize videoLink;
@synthesize votes;

@end
