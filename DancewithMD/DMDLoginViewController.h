//
//  DMDLoginViewController.h
//  DancewithMD
//
//  Created by Rishi on 04/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DMDViewControllerDelegate <NSObject>
- (void)didDismissPresentedViewController;
- (void)cancelModalView:(id)sender;
@end

@interface DMDLoginViewController : UIViewController {
    NSMutableData *responseAsyncData;
    NSError  *connectionError;
    NSURLConnection *connectionRegister;
    NSURLConnection *connectionLogin;
    NSURLConnection *connectionUserDetail;
    NSURLConnection *connectionPassReset;
    NSURLConnection *connectionLogout;
    NSURLConnection *connectionJhalak;
    int status;
}

@property (nonatomic, weak) id<DMDViewControllerDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIButton *maleButton;
@property (strong, nonatomic) IBOutlet UIButton *femaleButton;
@property (strong, nonatomic) IBOutlet UITextField *loginusernametextfield;
@property (strong, nonatomic) IBOutlet UITextField *loginpasswordtextfield;
@property (strong, nonatomic) IBOutlet UITextField *registerpasstextfield;
@property (strong, nonatomic) IBOutlet UITextField *registernametextfield;
@property (strong, nonatomic) IBOutlet UIButton *logoutButton;
@property (strong, nonatomic) IBOutlet UITextField *registeremailtextfield;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) IBOutlet UIButton *forgetPassButton;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loginprogressbar;
@property (strong, nonatomic) IBOutlet UIView *registerView;
@property (strong, nonatomic) IBOutlet UIView *loginView;
@property (strong, nonatomic) IBOutlet UIButton *signupButton;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UIButton *registerButton;
@property (strong, nonatomic) IBOutlet UITextField *registervpasstextfield;
@property (strong, nonatomic) IBOutlet UILabel *newacclabel;
@property (strong, nonatomic) IBOutlet UILabel *orlabel;

- (IBAction)registerBtnPressed:(id)sender;
- (IBAction)forgotpassBtnPressed:(id)sender;
- (IBAction)loginBtnPressed:(id)sender;
- (IBAction)maleBtnPressed:(id)sender;
- (IBAction)femaleBtnPressed:(id)sender;
- (IBAction)logoutBtnPressed:(id)sender;
- (IBAction)signupBtnPressed:(id)sender;
- (IBAction)cancelBtnPressed:(id)sender;
- (IBAction)facebookLoginBtnPressed:(id)sender;

@end
