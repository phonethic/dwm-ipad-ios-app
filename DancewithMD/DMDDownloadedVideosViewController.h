//
//  DMDDownloadedVideosViewController.h
//  DancewithMD
//
//  Created by Rishi on 15/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "FXImageView.h"
#import <MediaPlayer/MediaPlayer.h>

@interface DMDDownloadedVideosViewController : UIViewController
{
    int small;
    int fullscreen;
}
@property (strong, nonatomic) NSMutableArray *videoArray;
@property (readwrite, nonatomic) int selectedIndex;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (strong, nonatomic) IBOutlet iCarousel *carousel;
@property (strong, nonatomic) IBOutlet UIImageView *lightspotImageView;
@property (strong, nonatomic) IBOutlet UIView *videoView;
@property (strong, nonatomic) IBOutlet UIImageView *screenshotView;
@property (strong, nonatomic) IBOutlet UIImageView *videoboxImageView;
@property (strong, nonatomic) IBOutlet UILabel *titlelbl;
@property (strong, nonatomic) IBOutlet UILabel *namelbl;
@property (strong, nonatomic) IBOutlet UIImageView *reflectionView;
@property (nonatomic, copy) NSString *songID;
@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;
@property (strong, nonatomic) IBOutlet UIButton *deleteVideoBtn;

- (IBAction)closevideoBtnPressed:(id)sender;
- (IBAction)deletebtnPressed:(id)sender;

@end
