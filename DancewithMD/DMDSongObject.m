//
//  DMDSongObject.m
//  DancewithMD
//
//  Created by Rishi on 03/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDSongObject.h"

@implementation DMDSongObject
@synthesize songId;
@synthesize songTitle;
@synthesize songMovie;
@synthesize songChoreographer;
@synthesize songPic;
@synthesize songLock;
@synthesize songCategory;
@synthesize songCategoryName;

@end
