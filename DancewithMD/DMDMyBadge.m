//
//  DMDMyBadge.m
//  DancewithMD
//
//  Created by Rishi on 22/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDMyBadge.h"

@implementation DMDMyBadge

@synthesize mybadge_name;
@synthesize mybadge_count;
@synthesize mybadge_songs;
@synthesize mybadge_icon;

@end
