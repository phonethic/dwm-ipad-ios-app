//
//  DMDSongDetailViewController.m
//  DancewithMD
//
//  Created by Rishi on 12/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDSongDetailViewController.h"
#import "DMDSongDetailObj.h"
#import "DMDBadgeObj.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SCViewController.h"
#import "DMDBadgeShareController.h"
#import "DMDAppDelegate.h"

//#define SONG_DETAIL_LIST_LINK(SONGID) [NSString stringWithFormat:@"%@/api/v1/retrieve/songs/%@",LIVE_DWM_SERVER,SONGID]
//#define SONG_SEEN_LINK(USERID,SONGID) [NSString stringWithFormat:@"%@/api/v1/retrieve/points?where={\"User_ID\":\"%@\",\"Song_ID\":%@}",LIVE_DWM_SERVER,USERID,SONGID]
//#define SONG_CLIP_ID_LINK(CLIPID) [NSString stringWithFormat:@"%@/api/v1/retrieve/clips/%@",LIVE_DWM_SERVER,CLIPID]
//#define BADGE_LIST_LINK [NSString stringWithFormat:@"%@/api/v1/retrieve/badge",LIVE_DWM_SERVER]
//#define BADGE_SET_LINK [NSString stringWithFormat:@"%@/api/v1/retrieve/setbadge",LIVE_DWM_SERVER]

#define SONG_DETAIL_LIST_LINK(SONGID) [NSString stringWithFormat:@"%@/api/retrieve/songs/%@",LIVE_DWM_SERVER,SONGID]
#define SONG_SEEN_LINK(USERID,SONGID) [NSString stringWithFormat:@"%@/api/retrieve/getpoints?where={\"User_ID\":\"%@\",\"Song_ID\":%@}",LIVE_DWM_SERVER,USERID,SONGID]
#define SONG_CLIP_ID_LINK(CLIPID) [NSString stringWithFormat:@"%@/api/retrieve/clips/%@",LIVE_DWM_SERVER,CLIPID]
#define BADGE_LIST_LINK [NSString stringWithFormat:@"%@/api/retrieve/badge",LIVE_DWM_SERVER]
#define BADGE_SET_LINK [NSString stringWithFormat:@"%@/api/retrieve/setbadge",LIVE_DWM_SERVER]

#define BADGE_IMAGE_LINK(SHAREPIC) [NSString stringWithFormat:@"%@/images/%@",LIVE_DWM_SERVER,SHAREPIC]
#define SONG_PLAYED_LINK(USERID,SONGID,CLIPID) [NSString stringWithFormat:@"%@/point.php?uid=%@&sid=%@&cid=%d", LIVE_DWM_SERVER,USERID,SONGID,CLIPID]


@interface DMDSongDetailViewController ()

@end

@implementation DMDSongDetailViewController
@synthesize songID;
@synthesize moviePlayer;
@synthesize selectedsongName;
@synthesize songDetailArray;
@synthesize songdetailTableView;
@synthesize seenCount;
@synthesize selectedsongindex;
@synthesize badgeArray;
@synthesize badgeImageView;
@synthesize badgelbl;
@synthesize badgeView;
@synthesize selectedsongclipID;
@synthesize carousel;
@synthesize downloadBtn;
@synthesize titlelbl;
@synthesize namelbl;
@synthesize progressBar;
@synthesize videoView;
@synthesize backgroundImageView;
@synthesize lightspotImageView;
@synthesize videoboxImageView;
@synthesize screenshotView;
@synthesize spinnerIndicatior;
@synthesize shareBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)setFullscreenOffShareFB
{
    //NSLog(@"setFullscreenOffShareFB");
    fullscreen = 0;
}

-(void)viewWillDisappear:(BOOL)animated
{
    //NSLog(@"DMDSongdetails viewWillDisappear");
    if(self.moviePlayer != nil && !fullscreen)
    {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
        [self cancelVideoDownload];
        [progressBar setProgress:0];
    }
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    fullscreen = 0;
    carousel.type = iCarouselTypeCoverFlow;
    self.badgeView.layer.cornerRadius = 10;
    self.badgeView.layer.masksToBounds = YES;
    self.seenCount = @"";
    titlelbl.text = @"";
    titlelbl.textColor = GRAY_TEXT_COLOR;
    namelbl.textColor = GRAY_TEXT_COLOR;
    namelbl.text = @"";
    titlelbl.font = KABEL_FONT(30);
    namelbl.font = KABEL_FONT(18);
    badgelbl.font = KABEL_FONT(18);
    [shareBtn.titleLabel setFont:KABEL_FONT(20)];
    [downloadBtn.titleLabel setFont:KABEL_FONT(22)];
    downloadBtn.titleLabel.lineBreakMode = UILineBreakModeWordWrap;
    downloadBtn.titleLabel.textAlignment = UITextAlignmentCenter;
    [downloadBtn setTitleColor:GRAY_TEXT_COLOR forState:UIControlStateNormal];
    [downloadBtn setTitleColor:GRAY_TEXT_COLOR forState:UIControlStateHighlighted];
    downloadBtn.userInteractionEnabled = TRUE;
    songDetailArray = [[NSMutableArray alloc] init];
    badgeArray = [[NSMutableArray alloc] init];
    [badgeView setHidden:TRUE];
    if([DMDDelegate networkavailable])
    {
        [self songDetailListAsynchronousCall];
        [self badgeListAsynchronousCall];
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        
    }
    progressBar.hidden = TRUE;
    [videoView setHidden:TRUE];
    small = 0;
    close = 0;
    //    NSArray *fonts = [UIFont familyNames];
    //
    //    for(NSString *string in fonts){
    //        //NSLog(@"%@", string);
    //    }
}

-(void)songDetailListAsynchronousCall
{
    [spinnerIndicatior startAnimating];
	/****************Asynchronous Request**********************/
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:SONG_DETAIL_LIST_LINK(self.songID)] cachePolicy:NO timeoutInterval:5.0];
    //NSLog(@"%@",urlRequest.URL);
    //Get list of all songs
    connection1 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)songLockAsynchronousCall 
{
    [spinnerIndicatior startAnimating];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:@"userId"];
	/****************Asynchronous Request**********************/
    NSMutableString *requestString = [NSMutableString stringWithString:[DMDDelegate URLEncode:SONG_SEEN_LINK(userID,self.songID)]];
//    [requestString appendString: [NSString stringWithFormat:@"%@", userID]];
//    [requestString appendString: @"%22,%22Song_ID%22:"];
//    [requestString appendString: [NSString stringWithFormat:@"%@", (self.songID)]];
//    [requestString appendString: @"%7D"];
    NSLog(@"SEEN LINK=%@",requestString);
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:requestString] cachePolicy:NO timeoutInterval:15.0];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"HH:mm:ss"];
    
    NSDate *now = [[NSDate alloc] init];
    
    NSString *theDate = [dateFormat stringFromDate:now];
    NSString *theTime = [timeFormat stringFromDate:now];
    
    NSLog(@"\n"
          "theDate: |%@| \n"
          "theTime: |%@| \n"
          , theDate, theTime);
    //Get count of total unlocked videos
    connection2 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)songUrlAsynchronousCall:(NSString*) clipId
{
	/****************Asynchronous Request**********************/
    //    NSMutableString *requestString = [NSMutableString stringWithString:SONG_CLIP_ID_LINK];
    //    [requestString appendString: [NSString stringWithFormat:@"%@", clipId]];
    //    [requestString appendString: @"%22%7D"];
    NSString *requestString = [NSMutableString stringWithString:SONG_CLIP_ID_LINK(clipId)];
    //NSLog(@"CLIP LINK=%@",requestString);
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestString] cachePolicy:NO timeoutInterval:5.0];
    
    //Get URL of video to be played
    connection3 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)songPlayedAsynchronousCall:(int)clipID
{
	/****************Asynchronous Request**********************/
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:@"userId"];
    //NSLog(@"%@",SONG_PLAYED_LINK(userID,self.songID,clipID));
    NSMutableString *requestString = [NSMutableString stringWithString:SONG_PLAYED_LINK(userID,self.songID,clipID)];
    //NSLog(@"%@",SONG_PLAYED_LINK(userID,self.songID,clipID));
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestString] cachePolicy:NO timeoutInterval:5.0];
    
    //Update unlock videos count by currently played song ID
    connection4 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)badgeListAsynchronousCall
{
	/****************Asynchronous Request**********************/
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:BADGE_LIST_LINK] cachePolicy:NO timeoutInterval:5.0];
    
    //Get list of all Badges
    connection5 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)badgeSetAsynchronousCall:(NSString *)badgeName
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:@"userId"];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:BADGE_SET_LINK] cachePolicy:NO timeoutInterval:30.0];
    NSMutableDictionary *postReq = [[NSMutableDictionary alloc] init];
    [postReq setObject:userID forKey:@"user_id"];
    [postReq setObject:self.songID forKey:@"song_id"];
    [postReq setObject:badgeName forKey:@"badge"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReq
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (!jsonData) {
        //NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //NSLog(@"---%@---",jsonString);
    }
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody: jsonData];
    
    //Update badge for particular UserId
    connection6 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)videoDownloadAsynchronousCall
{
    [downloadBtn setBackgroundImage:[UIImage imageNamed:@"dwn_button_on.png"] forState:UIControlStateNormal];
    [downloadBtn setBackgroundImage:[UIImage imageNamed:@"dwn_button_on.png"] forState:UIControlStateHighlighted];
    [downloadBtn setTitle:@"Downloading" forState:UIControlStateNormal];
    [downloadBtn setTitle:@"Downloading" forState:UIControlStateHighlighted];
    [downloadBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [downloadBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
	/****************Asynchronous Request**********************/
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:self.moviePlayer.contentURL.relativeString] cachePolicy:NO timeoutInterval:15.0];
    
    //Download particular video
    connection7 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if(connection==connection7) {
            contentSize = [httpResponse expectedContentLength];
        }
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil && connection!=connection7)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    if(responseDownloadAsyncData==nil && connection==connection7)
	{
		responseDownloadAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    if(connection!=connection7)
    {
        [responseAsyncData appendData:data];
    } else if (connection==connection7) {
         [responseDownloadAsyncData appendData:data];
    }

    if(connection==connection7) {
        float progress = (float)[responseDownloadAsyncData length] / (float)contentSize;
        //NSLog(@"---%f---",progress);
        [progressBar setProgress:progress];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //NSLog(@"Error: %@", [error localizedDescription]);
    [spinnerIndicatior stopAnimating];
    if(connection==connection7) {
        [self cancelVideoDownload];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                            message:@"Video download failed. Please try again later."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [spinnerIndicatior stopAnimating];
    NSError* error;
    if(connection==connection1)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    [songDetailArray removeAllObjects];
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* songattributeDict in dataArray)
                    {
                        songDetailObj = [[DMDSongDetailObj alloc] init];
                        songDetailObj.songId = [songattributeDict objectForKey:@"Song_ID"];
                        songDetailObj.songName = [songattributeDict objectForKey:@"Song_Name"];
                        songDetailObj.songClip_Part_ID = [songattributeDict objectForKey:@"Clip_Part_ID"];
                        songDetailObj.songSequence = [songattributeDict objectForKey:@"Sequence"];
                        songDetailObj.songShare_ID = [songattributeDict objectForKey:@"Share_ID"];
                        ////NSLog(@"\n songid: %@ , name: %@  , clipid: %@  , sequence: %@  , shareid: %@ \n", songDetailObj.songId, songDetailObj.songName, songDetailObj.songClip_Part_ID, songDetailObj.songSequence, songDetailObj.songShare_ID);
                        [songDetailArray addObject:songDetailObj];
                        songDetailObj = nil;
                    }
                }
            }
        }
        responseAsyncData = nil;
        connection1 = nil;
        ////NSLog(@"detail array count: %d", [songDetailArray count]);
        [songdetailTableView reloadData];
        [carousel reloadData];
        [self songLockAsynchronousCall];
    }
    else if (connection==connection2)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            NSLog(@" result=%@", json ) ;
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* lockattributeDict in dataArray)
                    {
                        if([lockattributeDict valueForKey:@"User_ID"] != nil) {
//                            NSString *uidString = [lockattributeDict objectForKey:@"User_ID"];
//                            NSString *sidString = [lockattributeDict objectForKey:@"Song_ID"];
//                            NSString *seenString = [lockattributeDict objectForKey:@"Seen_Clip"];
                            self.seenCount = [lockattributeDict objectForKey:@"Seen_Clips"] ;
                            ////NSLog(@"\n uid: %@ , sid: %@  , seen: %@  , seenclips: %@ \n", uidString, sidString, seenString, self.seenCount);
                        } else {
                            //NSString *dataString = [lockattributeDict objectForKey:@"data"];
                            ////NSLog(@"%@",dataString);
                        }
                    }
                }
            }
        }
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        
        NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
        [timeFormat setDateFormat:@"HH:mm:ss"];
        
        NSDate *now = [[NSDate alloc] init];
        
        NSString *theDate = [dateFormat stringFromDate:now];
        NSString *theTime = [timeFormat stringFromDate:now];
        
        NSLog(@"\n"
              "theDate: |%@| \n"
              "theTime: |%@| \n"
              , theDate, theTime);
        connection2 = nil;
        responseAsyncData = nil;
        [songdetailTableView reloadData];
        [carousel reloadData];
        [spinnerIndicatior stopAnimating];
    }
    else if (connection==connection3)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //NSLog(@"json: %@", json);
            NSString *urlString;
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* songattributeDict in dataArray)
                    {
                        urlString = [songattributeDict objectForKey:@"Video_URL"];
                    }
                    NSString *videourl = [DMDDelegate URLEncode:[NSString stringWithFormat:@"%@",urlString]];
                    //NSLog(@"VIDEO URL=%@",videourl);
                    
                    if(self.moviePlayer != nil) {
                        [moviePlayer stop];
                        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                                       object:self.moviePlayer];
                        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                                       object:self.moviePlayer];
                        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                                       object:self.moviePlayer];
                        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
                        {
                            [moviePlayer.view removeFromSuperview];
                        }
                        self.moviePlayer = nil;
                    }
                    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:videourl]];
                    self.moviePlayer.view.frame = CGRectMake(239, 246, 548, 298);
                    //self.moviePlayer.view.clipsToBounds = TRUE;
                    //self.moviePlayer.view.layer.cornerRadius = 5;
                    //self.moviePlayer.view.layer.borderColor = [UIColor darkGrayColor].CGColor;
                    //self.moviePlayer.view.layer.borderWidth = 4;
                    [moviePlayer setControlStyle:MPMovieControlStyleDefault];
                    self.moviePlayer.scalingMode = MPMovieScalingModeFill;
                    self.moviePlayer.shouldAutoplay = YES;
                    [[NSNotificationCenter defaultCenter] addObserver:self
                                                             selector:@selector(moviePlayBackDidFinish:)
                                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                                               object:self.moviePlayer];
                    [[NSNotificationCenter defaultCenter] addObserver:self
                                                             selector:@selector(moviePlayEnterFullScreen:)
                                                                 name:MPMoviePlayerWillEnterFullscreenNotification
                                                               object:self.moviePlayer];
                    [[NSNotificationCenter defaultCenter] addObserver:self
                                                             selector:@selector(moviePlayExitFullScreen:)
                                                                 name:MPMoviePlayerDidExitFullscreenNotification
                                                               object:self.moviePlayer];
                    [self.view addSubview:moviePlayer.view];
                    [self.view bringSubviewToFront:moviePlayer.view];
                    [moviePlayer prepareToPlay];
                    [moviePlayer play];
                    close = 0;
                    downloadBtn.hidden = FALSE;
                    progressBar.hidden = FALSE;
                }
            }
        }
        connection3 = nil;
        responseAsyncData = nil;
    }
    else if (connection==connection4)
    {
        if(responseAsyncData != nil)
        {
            NSString *result = [[NSString alloc] initWithBytes:[responseAsyncData mutableBytes]
                                                        length:[responseAsyncData length] encoding:NSASCIIStringEncoding];
            //NSLog(@"connection 4 result = %@", result ) ;
            if(result!=nil && [result isEqualToString:@"1"])
            {
                self.seenCount = [NSString stringWithFormat:@"%d",[self.seenCount intValue] + 1];
                [songdetailTableView reloadData];
                [carousel reloadData];
                //Switch to Next Video
                if(connection7==nil)
                {
                    //NSLog(@"connection 4 result 1 connection 7 NULL move to next video") ;
                    [carousel scrollToItemAtIndex:selectedsongindex+1 duration:1.5];
                } else {
                    //NSLog(@"connection 4 result 1 connection 7 Not NULL") ;
                }
            } else if (result!=nil && [result isEqualToString:@"2"]) {
                if(connection7==nil)
                {
                    //NSLog(@"connection 4 result 2 connection 7 NULL move to next video") ;
                    [carousel scrollToItemAtIndex:selectedsongindex+1 duration:1.5];
                } else {
                    //NSLog(@"connection 4 result 2 connection 7 Not NULL") ;
                }
            }
        }
        if(connection7==nil)
        {
            //NSLog(@"connection 4 connection 7 NULL hide video view") ;
            [self hideVideoView];
        }
        connection4 = nil;
        responseAsyncData = nil;
    }
    else if (connection==connection5)
    {
        if(responseAsyncData != nil)
        {
            //NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
            ////NSLog(@"\n result:%@\n\n", result);
            NSError* error;
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            ////NSLog(@"json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    [badgeArray removeAllObjects];
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* badgeattributeDict in dataArray) {
                        badgeObj = [[DMDBadgeObj alloc] init];
                        badgeObj.songId = [badgeattributeDict objectForKey:@"Song_ID"];
                        badgeObj.clip_partId = [badgeattributeDict objectForKey:@"Clip_Part_ID"];
                        badgeObj.shareId = [badgeattributeDict objectForKey:@"Share_ID"];
                        badgeObj.sharePic = [badgeattributeDict objectForKey:@"Share_Pic"];
                        badgeObj.badgeName = [badgeattributeDict objectForKey:@"Badge_Name"];
                        badgeObj.shareText = [badgeattributeDict objectForKey:@"Share_Text"];
                        ////NSLog(@"\n songid: %@ , clipartid: %@  , shareid: %@  , sharepic: %@  , badgename: %@  , sharetext: %@\n", badgeObj.songId, badgeObj.clip_partId, badgeObj.shareId, badgeObj.sharePic, badgeObj.badgeName, badgeObj.shareText);
                        [badgeArray addObject:badgeObj];
                        badgeObj = nil;
                    }
                }
            }
        }
        connection5=nil;
        responseAsyncData = nil;
    }
    else if (connection==connection6)
    {
        if(responseAsyncData != nil)
        {
            NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
            //NSLog(@"\n result:%@\n\n", result);
        }
        connection6=nil;
        responseAsyncData = nil;
    }
    else if (connection==connection7)
    {
        if(responseDownloadAsyncData)
        {
            NSArray *dirPaths;
            NSString *fullPath;
            NSString *docsDir;
            NSFileManager *filemgr;
            
            // Get the documents directory
            dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            docsDir = [dirPaths objectAtIndex:0];
            //NSLog(@"song Name=%@",self.selectedsongName);
            fullPath = [docsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"Videos/%@",self.selectedsongName]];
            
            //NSLog(@"%@",fullPath);
            
            filemgr =[NSFileManager defaultManager];
            
            if ([filemgr fileExistsAtPath:fullPath])
            {
                //NSLog(@"Directory exists");
            } else {
                //NSLog(@"Directory not Found.");
                if ([filemgr createDirectoryAtPath:fullPath withIntermediateDirectories:YES
                                        attributes:nil error: NULL] == NO)
                {
                    //NSLog(@"Failed to create directory");
                } else {
                    //NSLog(@"Directory Created");
                }
            }
            NSString *url = connection.currentRequest.URL.relativeString;
            //NSLog(@"url = %@", url);
            NSArray *arr = [url componentsSeparatedByString:@"/"];
            //NSLog(@"array = %@", arr);
            NSString * fileName = [NSString stringWithFormat:@"%@#%@",[arr objectAtIndex:[arr count]-4],[arr objectAtIndex:[arr count]-1]];
            fileName = [fileName stringByReplacingOccurrencesOfString:@"%20" withString:@"_"];
            //NSLog(@"%@",fileName);
            
            NSString *filepath = [fullPath stringByAppendingPathComponent:fileName];
            //NSLog(@"filepath = %@" , filepath);
            if ([filemgr fileExistsAtPath:filepath])
            {
                //NSLog(@"file exists");
            } else {
                //NSLog(@"file not exists");
                [progressBar setProgress:1.0];
                [downloadBtn setBackgroundImage:[UIImage imageNamed:@"dwn_button_on.png"] forState:UIControlStateNormal];
                [downloadBtn setBackgroundImage:[UIImage imageNamed:@"dwn_button_on.png"] forState:UIControlStateHighlighted];
                [downloadBtn setTitle:@"Completed" forState:UIControlStateNormal];
                [downloadBtn setTitle:@"Completed" forState:UIControlStateHighlighted];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                    message:[NSString stringWithFormat:@"%@ downloaded successfully.",fileName]
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                [alertView show];
                BOOL pass = [responseDownloadAsyncData writeToFile:filepath atomically:YES];
                if (pass) {
                    //NSLog(@"Saved to file: %@", filepath);
                    [downloadBtn setUserInteractionEnabled:FALSE];
                } else {
                    //NSLog(@"problem");
                }
            }
        }
        connection7=nil;
        responseDownloadAsyncData = nil;
    }
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    //NSLog(@"moviePlayBackDidFinish close=%d",close);
    [moviePlayer setFullscreen:FALSE animated:TRUE];    
    MPMoviePlayerController *player = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerWillEnterFullscreenNotification object:player];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerDidExitFullscreenNotification object:player];
    //NSLog(@"sondID=%@ , clipID=%@",self.songID, self.selectedsongclipID);
    if(!close) {
        //NSLog(@"moviePlayBackDidFinish songPlayedAsynchronousCall function called");
        [self showbadgeView:self.songID clipPartId:self.selectedsongclipID];
        [self songPlayedAsynchronousCall : selectedsongindex+1]; //Unlock next video
    }
    if ([player respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [player.view removeFromSuperview];
    }
    moviePlayer = nil;
}

- (void) moviePlayBackDidFinishLocal:(NSNotification*)notification {
    [moviePlayer setFullscreen:FALSE animated:TRUE];
    NSNumber* reason = [[notification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    //NSLog(@"%d",[reason intValue]);
    switch ([reason intValue]) {
        case MPMovieFinishReasonPlaybackEnded:
            //NSLog(@"Playback Ended");
            break;
        case MPMovieFinishReasonPlaybackError:
        {
            //NSLog(@"Playback Error");
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Sorry!! This video is corrupted.\n Please delete this video from downloaded videos section and try to download again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlert show];
        }
            
            break;
        case MPMovieFinishReasonUserExited:
            //NSLog(@"User Exited");
            break;
        default:
            break;
    }
    MPMoviePlayerController *player = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerWillEnterFullscreenNotification object:player];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerDidExitFullscreenNotification object:player];
    if ([player respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [player.view removeFromSuperview];
    }
    moviePlayer = nil;
    if(!close && [reason intValue] != MPMovieFinishReasonPlaybackError)
    {
        [self showbadgeView:self.songID clipPartId:self.selectedsongclipID];
        [carousel scrollToItemAtIndex:selectedsongindex+1 duration:1.5];
    }
    if ([reason intValue] != MPMovieFinishReasonPlaybackError) {
        [self hideVideoView];
    }
}

- (void) moviePlayEnterFullScreen:(NSNotification*)notification {
    //NSLog(@"moviePlayEnterFullScreen");
    fullscreen = 1;
    self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
    
}

- (void) moviePlayExitFullScreen:(NSNotification*)notification {
    //NSLog(@"moviePlayExitFullScreen");
    fullscreen = 0;
    self.moviePlayer.scalingMode = MPMovieScalingModeFill;
}

-(void)showbadgeView:(NSString *)songid clipPartId:(NSString *)clipid
{
    for (DMDBadgeObj * tempbadgeObj in badgeArray) {
        if([clipid isEqualToString:tempbadgeObj.clip_partId])
        {
            [badgeView setHidden:FALSE];
            //NSLog(@"%@ %@", tempbadgeObj.clip_partId, BADGE_IMAGE_LINK(tempbadgeObj.sharePic));
            
            //badgeImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:BADGE_IMAGE_LINK(tempbadgeObj.sharePic)]]];
            [badgeImageView setImageWithURL:[NSURL URLWithString:BADGE_IMAGE_LINK(tempbadgeObj.sharePic)]
                           placeholderImage:[UIImage imageNamed:@"pop-up.png"]
                                    success:^(UIImage *image) {
                                        //DebugLog(@"success");
                                    }
                                    failure:^(NSError *error) {
                                        //DebugLog(@"write error %@", error);
                                    }];    
            
            
            if ((id)tempbadgeObj.shareText != [NSNull null]) {
                badgelbl.text = tempbadgeObj.shareText;
            }
            if ((id)tempbadgeObj.badgeName != [NSNull null]) {
                [self badgeSetAsynchronousCall:tempbadgeObj.badgeName];
            }
            break;
        } else {
            [badgeView setHidden:TRUE];
        }
    }
    
}


#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [songDetailArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.imageView.frame = CGRectMake(0,0,50,50);
        cell.userInteractionEnabled = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.font = KABEL_FONT(28);
        cell.detailTextLabel.textColor = [UIColor darkGrayColor];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:24.0];
        cell.imageView.image = [UIImage imageNamed:@"lock.png"];
    }
    //NSLog(@"row=%d count=%d",indexPath.row,[self.seenCount intValue]);
    if([self.seenCount intValue]>=indexPath.row)
    {
        cell.userInteractionEnabled = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.imageView.image = [UIImage imageNamed:@"unlock.png"];
    } else {
        cell.userInteractionEnabled = NO;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.imageView.image = [UIImage imageNamed:@"lock.png"];
    }
    DMDSongDetailObj *tempdetailsongObj  = [songDetailArray objectAtIndex:indexPath.row];
    cell.textLabel.text = tempdetailsongObj.songName;
    cell.detailTextLabel.text = tempdetailsongObj.songClip_Part_ID;
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    DMDSongDetailObj *tempdetailsongObj  = [songDetailArray objectAtIndex:indexPath.row];
    selectedsongindex = indexPath.row;
    //NSLog(@"index111==%d",selectedsongindex);
    //NSLog(@"clipID=%@",tempdetailsongObj.songClip_Part_ID);
    selectedsongclipID = tempdetailsongObj.songClip_Part_ID;
    //[self showbadgeView:self.songID clipPartId:selectedsongclipID]; //Should be commented in final version
    [self songUrlAsynchronousCall:tempdetailsongObj.songClip_Part_ID]; //Get Url of the video selected from tableview
    //NSLog(@"songID=%@ , clipID=%@",self.songID, self.selectedsongclipID);
    selectedsongName =  [tempdetailsongObj.songName stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setSongdetailTableView:nil];
    [self setBadgeView:nil];
    [self setBadgeImageView:nil];
    [self setBadgelbl:nil];
    [self setDownloadBtn:nil];
    [self setTitlelbl:nil];
    [self setNamelbl:nil];
    [self setProgressBar:nil];
    [self setVideoView:nil];
    [self setBackgroundImageView:nil];
    [self setLightspotImageView:nil];
    [self setReflectionView:nil];
    [self setVideoboxImageView:nil];
    [self setScreenshotView:nil];
    [self setSpinnerIndicatior:nil];
    [self setShareBtn:nil];
    [super viewDidUnload];
}

- (IBAction)closevideoBtnPressed:(id)sender {
    if(connection7==nil)
    {
        if(self.moviePlayer != nil) {
            close = 1;
            [moviePlayer stop];
            //NSLog(@"close=%d",close);
            [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                             name:MPMoviePlayerPlaybackDidFinishNotification
                                                           object:self.moviePlayer];
            [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                             name:MPMoviePlayerWillEnterFullscreenNotification
                                                           object:self.moviePlayer];
            [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                             name:MPMoviePlayerDidExitFullscreenNotification
                                                           object:self.moviePlayer];
            if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
            {
                [moviePlayer.view removeFromSuperview];
            }
            self.moviePlayer = nil;
        }
        [progressBar setProgress:0];
        [self hideVideoView];
    }
    else {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"Are you sure you want to cancel the download ?"
                                  delegate:self
                                  cancelButtonTitle:nil
                                  otherButtonTitles:@"No",@"Yes",nil];
        alertView.tag = 1;
        [alertView show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==1)
    {
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        if([title isEqualToString:@"Yes"])
        {
            //NSLog(@"Button YES was selected.");
            [self cancelVideoDownload];
            if(self.moviePlayer != nil) {
                [moviePlayer stop];
                [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                                               object:self.moviePlayer];
                [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                                 name:MPMoviePlayerWillEnterFullscreenNotification
                                                               object:self.moviePlayer];
                [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                                 name:MPMoviePlayerDidExitFullscreenNotification
                                                               object:self.moviePlayer];
                if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
                {
                    [moviePlayer.view removeFromSuperview];
                }
                self.moviePlayer = nil;
            }
            [self hideVideoView];
        }
    } else {
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        if([title isEqualToString:@"Yes"])
        {
            //NSLog(@"Button YES was selected.");
            [self cancelVideoDownload];
        }
    }
}

- (IBAction)fbshareBtnPressed:(id)sender {
    NSString *text;
    NSString *pic;
    //fullscreen = 1;
    for (DMDBadgeObj * tempbadgeObj in badgeArray) {
        if([self.selectedsongclipID isEqualToString:tempbadgeObj.clip_partId])
        {
            //NSLog(@"text======%@===========",tempbadgeObj.shareText);
            //NSLog(@"link======%@===========",BADGE_IMAGE_LINK(tempbadgeObj.sharePic));
            text = tempbadgeObj.shareText;
            pic = BADGE_IMAGE_LINK(tempbadgeObj.sharePic);
        }
    }
    DMDBadgeShareController *facebookViewComposer = [[DMDBadgeShareController alloc] initWithNibName:@"DMDBadgeShareController" bundle:nil];
    facebookViewComposer.title = @"FACEBOOK";
    facebookViewComposer.FBtitle = @"Dance with Madhuri";
    facebookViewComposer.FBtCaption = text;
    facebookViewComposer.FBtPic = pic;
    facebookViewComposer.modalPresentationStyle = UIModalPresentationCurrentContext;
    facebookViewComposer.showorhide = 0;
    NSLog(@"--%@--,--%@--",text,pic);
    [self.navigationController presentModalViewController:facebookViewComposer animated:YES];
}

- (IBAction)fbsharecloseBtnPressed:(id)sender {
    [badgeView setHidden:TRUE];
}

- (IBAction)downloadBtnPressed:(id)sender {
    if(connection7==nil)
    {
        //NSLog(@"Connection is NULL so create new and download");
        [self videoDownloadAsynchronousCall];
    } else {
        //NSLog(@"Connection not NULL so cancel it");
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:ALERT_TITLE
                                      message:@"Are you sure you want to cancel the download ?"
                                      delegate:self
                                      cancelButtonTitle:nil
                                      otherButtonTitles:@"No",@"Yes",nil];
        alertView.tag = 2;
        [alertView show];
    }
}

-(void)cancelVideoDownload
{
    if (connection7 != nil) {
        [connection7 cancel];
        connection7 = nil;
        responseDownloadAsyncData = nil;
        [progressBar setProgress:0 animated:TRUE];
        [downloadBtn setBackgroundImage:[UIImage imageNamed:@"dwn_button_default.png"] forState:UIControlStateNormal];
        [downloadBtn setBackgroundImage:[UIImage imageNamed:@"dwn_button_default.png"] forState:UIControlStateHighlighted];
        [downloadBtn setTitle:@"Download" forState:UIControlStateNormal];
        [downloadBtn setTitle:@"Download" forState:UIControlStateHighlighted];
        [downloadBtn setTitleColor:GRAY_TEXT_COLOR forState:UIControlStateNormal];
        [downloadBtn setTitleColor:GRAY_TEXT_COLOR forState:UIControlStateHighlighted];
        downloadBtn.userInteractionEnabled = TRUE;
    }
}

- (IBAction)resizeCarousal:(id)sender {
    //NSLog(@"%f %f %f %f",carousel.frame.origin.x,carousel.frame.origin.y,carousel.frame.size.width,carousel.frame.size.height);
    if (small==0 || small==2) {
        small = 1;
    } else if(small==1){
        small = 2;
    }
    [carousel reloadData];
}

-(void) loadLocalDocument:(NSString *)filePath {
    if(self.moviePlayer != nil) {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
    NSURL* videoURL = [NSURL fileURLWithPath: filePath];
    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:videoURL];
    self.moviePlayer.view.frame = CGRectMake(239, 246, 548, 298);
    //self.moviePlayer.view.clipsToBounds = TRUE;
    //self.moviePlayer.view.layer.cornerRadius = 5;
    //self.moviePlayer.view.layer.borderColor = [UIColor darkGrayColor].CGColor;
    //self.moviePlayer.view.layer.borderWidth = 4;
    [moviePlayer setControlStyle:MPMovieControlStyleDefault];
    self.moviePlayer.scalingMode = MPMovieScalingModeFill;
    self.moviePlayer.shouldAutoplay = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinishLocal:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayEnterFullScreen:)
                                                 name:MPMoviePlayerWillEnterFullscreenNotification
                                               object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(moviePlayExitFullScreen:)
                                                 name:MPMoviePlayerDidExitFullscreenNotification
                                               object:self.moviePlayer];
    [self.view addSubview:moviePlayer.view];
    [moviePlayer prepareToPlay];
    [moviePlayer play];
    close = 0;
}

-(void)showVideoView
{
    backgroundImageView.image = [UIImage imageNamed:@"BG_zoomed_out.jpg"];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.8f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    
    [backgroundImageView.layer addAnimation:transition forKey:nil];
    
    [videoView setHidden:TRUE];
    [videoView  setAlpha:0.0];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [lightspotImageView setAlpha:0.0];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.4];
    [videoView setAlpha:1.0];
    [UIView commitAnimations];
    
    CABasicAnimation *shrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    shrink.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    shrink.fromValue = [NSNumber numberWithDouble:1.5];
    shrink.toValue = [NSNumber numberWithDouble:1.0];
    shrink.duration = 0.8;
    shrink.fillMode=kCAFillModeForwards;
    shrink.removedOnCompletion=NO;
    //unshrink.delegate = self;
    [videoView.layer addAnimation:shrink forKey:@"shrink"];
}

-(void)hideVideoView {
    backgroundImageView.image = [UIImage imageNamed:@"BG_default.jpg"];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.8f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [backgroundImageView.layer addAnimation:transition forKey:nil];
    
    CABasicAnimation *unshrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    unshrink.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    unshrink.fromValue = [NSNumber numberWithDouble:1.0];
    unshrink.toValue = [NSNumber numberWithDouble:1.5];
    unshrink.duration = 0.8;
    unshrink.fillMode = kCAFillModeForwards;
    unshrink.removedOnCompletion = NO;
    [videoView.layer addAnimation:unshrink forKey:@"shrink"];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.6];
    [videoView setAlpha:0.0];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [lightspotImageView setAlpha:1.0];
    [UIView commitAnimations];
    
    small = 2;
    [carousel reloadData];
    //[videoView setHidden:TRUE];
}

-(void)fadeOut:(UIView*)viewToDissolve withDuration:(NSTimeInterval)duration
{
    [UIView beginAnimations: @"Fade Out" context:nil];
    [UIView setAnimationDelay:0];
    [UIView setAnimationDuration:duration];
    viewToDissolve.alpha = 0.0;
    [UIView commitAnimations];
}

-(void)fadeIn:(UIView*)viewToFadeIn withDuration:(NSTimeInterval)duration
{
    [UIView beginAnimations: @"Fade In" context:nil];
    [UIView setAnimationDelay:0];
    [UIView setAnimationDuration:duration];
    viewToFadeIn.alpha = 1;
    [UIView commitAnimations];
    
}

#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [songDetailArray count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        FXImageView *imageView = [[FXImageView alloc] initWithFrame:CGRectMake(0, 0, 300.0f, 300.0f)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        ((UIImageView *)view).image = [UIImage imageNamed:@"carousal_lock.png"];
        imageView.asynchronous = YES;
        imageView.reflectionScale = 0.5f;
        imageView.reflectionAlpha = 0.25f;
        imageView.reflectionGap = 10.0f;
        imageView.shadowOffset = CGSizeMake(0.0f, 2.0f);
        imageView.shadowBlur = 5.0f;
        imageView.cornerRadius = 10.0f;
        view = imageView;
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(50, 168, 200, 50)];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = UITextAlignmentCenter;
        label.textColor = GRAY_TEXT_COLOR;
        label.font = KABEL_FONT(22);
        label.tag = 1;
        [view addSubview:label];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
    }
    ////NSLog(@"%f %f",((UIImageView *)view).frame.size.width,((UIImageView *)view).frame.size.height);
    
    ////NSLog(@"irow=%d icount=%d",index,[self.seenCount intValue]);
    if([self.seenCount intValue] >= index)
    {
        ((UIImageView *)view).image = [UIImage imageNamed:@"carousal_unlock.png"];
        //NSLog(@"unlock");
    } else {
        ((UIImageView *)view).image = [UIImage imageNamed:@"carousal_lock.png"];
        //NSLog(@"locked");
    }
    
    if(small==1)
    {
        CABasicAnimation *shrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        shrink.fromValue = [NSNumber numberWithDouble:1.0];
        shrink.toValue = [NSNumber numberWithDouble:0.5];
        shrink.duration = 0.6;
        shrink.fillMode=kCAFillModeForwards;
        shrink.removedOnCompletion=NO;
        //shrink.delegate = self;
        [((UIImageView *)view).layer addAnimation:shrink forKey:@"shrink"];
        self.carousel.userInteractionEnabled = FALSE;
    } else if(small==2) {
        CABasicAnimation *unshrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        unshrink.fromValue = [NSNumber numberWithDouble:0.5];
        unshrink.toValue = [NSNumber numberWithDouble:1.0];
        unshrink.duration = 0.6;
        unshrink.fillMode=kCAFillModeForwards;
        unshrink.removedOnCompletion=NO;
        //unshrink.delegate = self;
        [((UIImageView *)view).layer addAnimation:unshrink forKey:@"shrink"];
        self.carousel.userInteractionEnabled = TRUE;
    }
    
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    
    DMDSongDetailObj *tempdetailsongObj  = [songDetailArray objectAtIndex:index];
    NSArray *arr = [tempdetailsongObj.songClip_Part_ID componentsSeparatedByString:@"_"];
    ////NSLog(@"array = %@", arr);
    label.text = [NSString stringWithFormat:@"Lesson %@   Part %@",[arr objectAtIndex:[arr count]-2],[arr objectAtIndex:[arr count]-1]];
    return view;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    if([self.seenCount intValue] >= index)
    {
        //NSLog(@"index222==%d",index);
        [self fbsharecloseBtnPressed:nil];
        DMDSongDetailObj *tempdetailsongObj  = [songDetailArray objectAtIndex:index];
        selectedsongindex = index;
        selectedsongclipID = tempdetailsongObj.songClip_Part_ID;
        selectedsongName =  tempdetailsongObj.songName;
        //[self showbadgeView:self.songID clipPartId:selectedsongclipID];
        //NSLog(@"songID=%@ , clipID=%@ songName=%@",self.songID, self.selectedsongclipID,self.selectedsongName);
        titlelbl.text = [NSString stringWithFormat:@"%@",tempdetailsongObj.songName];
        NSArray *arr = [tempdetailsongObj.songClip_Part_ID componentsSeparatedByString:@"_"];
        //NSLog(@"%@", arr);
        namelbl.text = [NSString stringWithFormat:@"Lesson %@     Part %@",[arr objectAtIndex:[arr count]-2],[arr objectAtIndex:[arr count]-1]];
        NSString * fileName = [NSString stringWithFormat:@"%@#%@.mp4",tempdetailsongObj.songName,[NSString stringWithFormat:@"Lesson %@ Part %@",[arr objectAtIndex:[arr count]-2],[arr objectAtIndex:[arr count]-1]]];
        fileName = [fileName stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        //NSLog(@"filename=%@",fileName);
        
        NSArray *dirPaths;
        NSString *fullPath;
        NSString *docsDir;
        NSFileManager *filemgr =[NSFileManager defaultManager];
        // Get the documents directory
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        docsDir = [dirPaths objectAtIndex:0];
        //NSLog(@"song id = %@",self.songID);
        fullPath = [docsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"Videos/%@",self.selectedsongName]];
        NSString *filepath = [fullPath stringByAppendingPathComponent:fileName];
        //NSLog(@"filepath = %@" , filepath);
        if ([filemgr fileExistsAtPath:filepath])
        {
            //NSLog(@"file exists");
            progressBar.hidden = FALSE;
            [progressBar setProgress:1.0];
            [downloadBtn setBackgroundImage:[UIImage imageNamed:@"dwn_button_on.png"] forState:UIControlStateNormal];
            [downloadBtn setBackgroundImage:[UIImage imageNamed:@"dwn_button_on.png"] forState:UIControlStateHighlighted];
            [downloadBtn setTitle:@"Downloaded" forState:UIControlStateNormal];
            [downloadBtn setTitle:@"Downloaded" forState:UIControlStateHighlighted];
            downloadBtn.userInteractionEnabled = FALSE;
            [self showVideoView];
            if (small==0 || small==2) {
                small = 1;
            } else if(small==1){
                small = 2;
            }
            self.reflectionView.image = [UIImage imageNamed:@"video_box_transparent.png"];
            progressBar.hidden = FALSE;
            [videoView setHidden:FALSE];
            screenshotView.hidden = FALSE;
            
            UIGraphicsBeginImageContext(videoboxImageView.frame.size);
            [videoView.layer renderInContext:UIGraphicsGetCurrentContext()];
            UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
            screenshotView.image = viewImage;
            UIGraphicsEndImageContext();
            //UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
            
            
            // determine the size of the reflection to create
            NSUInteger reflectionHeight = self.videoboxImageView.bounds.size.height * kDefaultReflectionFraction;
            
            // create the reflection image and assign it to the UIImageView
            self.reflectionView.image = [DMDDelegate reflectedImage:screenshotView withHeight:reflectionHeight];
            //self.reflectionView.image = [self reflectedImage:screenshotView withHeight:reflectionHeight];
            self.reflectionView.alpha = kDefaultReflectionOpacity;
            
            screenshotView.image = [UIImage imageNamed:@"video_box_transparent.png"];
            screenshotView.hidden = TRUE;
            
            [self.carousel reloadData];
            [self performSelector:@selector(loadLocalDocument:) withObject:filepath afterDelay:0.5];
            
        } else {
            //NSLog(@"file not exists");
            downloadBtn.userInteractionEnabled = TRUE;
            [downloadBtn setBackgroundImage:[UIImage imageNamed:@"dwn_button_default.png"] forState:UIControlStateNormal];
            [downloadBtn setBackgroundImage:[UIImage imageNamed:@"dwn_button_default.png"] forState:UIControlStateHighlighted];
            [downloadBtn setTitle:@"Download" forState:UIControlStateNormal];
            [downloadBtn setTitle:@"Download" forState:UIControlStateHighlighted];
            [progressBar setProgress:0];
            [self showVideoView];
            if (small==0 || small==2) {
                small = 1;
            } else if(small==1){
                small = 2;
            }
            self.reflectionView.image = [UIImage imageNamed:@"video_box_transparent.png"];
            progressBar.hidden = FALSE;
            [videoView setHidden:FALSE];
            screenshotView.hidden = FALSE;
            
            UIGraphicsBeginImageContext(videoboxImageView.frame.size);
            [videoView.layer renderInContext:UIGraphicsGetCurrentContext()];
            UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
            screenshotView.image = viewImage;
            UIGraphicsEndImageContext();
            //UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
            
            
            // determine the size of the reflection to create
            NSUInteger reflectionHeight = self.videoboxImageView.bounds.size.height * kDefaultReflectionFraction;
            
            // create the reflection image and assign it to the UIImageView
            self.reflectionView.image = [DMDDelegate reflectedImage:screenshotView withHeight:reflectionHeight];
            //self.reflectionView.image = [self reflectedImage:screenshotView withHeight:reflectionHeight];
            self.reflectionView.alpha = kDefaultReflectionOpacity;
            
            screenshotView.image = [UIImage imageNamed:@"video_box_transparent.png"];
            screenshotView.hidden = TRUE;
            
            [self.carousel reloadData];
            //NSLog(@"===ID===%@",tempdetailsongObj.songClip_Part_ID);
            [self songUrlAsynchronousCall:tempdetailsongObj.songClip_Part_ID];
        }
        //NSLog(@"unlock");
    } else {
        //NSLog(@"locked");
    }
    
}

- (CGFloat)carousel:(iCarousel *)_carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionFadeMin:
        {
            return -0.2;
        }
        case iCarouselOptionFadeMax:
        {
            return 0.2;
        }
        case iCarouselOptionFadeRange:
        {
            return 3.0;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return 0.599131;
        }
        case iCarouselOptionTilt:
        {
            return 0.495158;
        }
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return NO;
        }
        default:
        {
            return value;
        }
    }
}

@end
